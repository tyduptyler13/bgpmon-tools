#Test suite for BGPmon::Fetch::Archive

use warnings;
use strict;
use Test::More;

BEGIN { use_ok('BGPmon::Fetch::Archive') }

my $ret = undef;    #Return value of calls
my $msg = undef;    #Message

#Set some common use values
use constant NETSEC_VALID_ARCHIVE_ADDR => 'archive.netsec.colostate.edu/peers';

#peers '89.149.178.10','65.49.129.101'
my @peerlist = ('89.149.178.10','65.49.129.101');

#1345852800 -1346468400
#1346457599 -1346458800
#1346449800 -1346468400
#1346449800 -1346455000
use constant NETSEC_VALID_START_TIME => 1346449800;
use constant NETSEC_VALID_END_TIME   => 1346455000;
##################### activate #########################################
##################### Fail conditions ########################################
#Test for incorrect arguments to activate

#Test for invalid start/end times
my $fetch = BGPmon::Fetch::Archive->new(
    url   => NETSEC_VALID_ARCHIVE_ADDR,
    peer  => \@peerlist,
    begin => "1338552838",
    end   => "1338550000"
);
$ret = $fetch->activate();
is( $ret,                     1,   'activate - invalid interval' );
is( $fetch->get_error_code(), 406, "activate - invalid timerange" );

$fetch = BGPmon::Fetch::Archive->new(
    url   => NETSEC_VALID_ARCHIVE_ADDR,
    peer  => \@peerlist,
    begin => "-1338552838",
    end   => "1338550000"
);
$ret = $fetch->activate();
is( $ret,                     1,   'activate: negative start time' );
is( $fetch->get_error_code(), 406, "activate - invalid timerange" );

#Test for incorrectly-formatted start/end times
$fetch = BGPmon::Fetch::Archive->new(
    url   => NETSEC_VALID_ARCHIVE_ADDR,
    peer  => \@peerlist,
    begin => "Mar 13 2011 12:34:56",
    end   => "Apr 1 2011 11:11:11"
);
$ret = $fetch->activate();
is( $ret,                     1,   'activate: string time format' );
is( $fetch->get_error_code(), 406, "activate - invalid timerange" );

#Test for illegal characters in argument
my $illegal = chr(0x90) . chr(0x90) . chr(0x90) . chr(0x90) . chr(0x90);
$fetch = BGPmon::Fetch::Archive->new(
    url   => "NETSEC_VALID_ARCHIVE_ADDR$illegal",
    peer  => \@peerlist,
    begin => "1338508800",
    end   => "1340150400"
);
$ret = $fetch->activate();
is( $ret,                     1,   "activate - invalid character in URL" );
is( $fetch->get_error_code(), 406, "activate - invalid character URL" );

# check the internals after a correct call
$fetch = BGPmon::Fetch::Archive->new(
    url   => NETSEC_VALID_ARCHIVE_ADDR,
    peer  => \@peerlist,
    begin => NETSEC_VALID_START_TIME,
    end   => NETSEC_VALID_END_TIME,
    ribs  => '0'
);
$ret = $fetch->activate();
is( $ret, 0, "valid initilization" );
is( $fetch->get_error_code(),
    0, "connect_archive - no error code set on success" );

open( TESTFILE, '>', 't/TESTCOUNTFILE' ) or die();

#read message
$msg = $fetch->read_xml_message();
ok( defined($msg), "read_xml_message - read first message" );
print TESTFILE $msg, "\n";
is( $fetch->get_error_code(),
    0, "read_xml_message - read first message error code" );

#read rest of the messages
do {
    $msg = $fetch->read_xml_message();
    if ( defined($msg) ) {
        print TESTFILE $msg, "\n";
    }
} while ( defined($msg) );

close TESTFILE;
my $num_lines = 0;
open( FILE, 't/TESTCOUNTFILE' ) or die;
$num_lines++ while (<FILE>);
close FILE;
is( $fetch->get_msgs_count(),
    $num_lines, "get_msgs_count - total number of messages read" );

unlink("t/TESTCOUNTFILE");

#print "Total number of files read: ", $fetch->{'files_read'}, "\n";

#$ret = $fetch->deactivate();
#is($ret,0,"deactivate - close valid connection");
#is($fetch->get_error_code(),0,"deactivate - close valid connection error code");

done_testing();

