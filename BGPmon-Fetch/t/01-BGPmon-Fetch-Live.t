# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl BGPmon-Fetch-Live.t'

#########################


use strict;
use warnings;

use Test::More;
BEGIN { use_ok('BGPmon::Fetch::Live') }
use_ok('BGPmon::Fetch');

isa_ok('BGPmon::Fetch::Live', 'BGPmon::Fetch');
#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

#print "Creating a Live object with correct arguments:\n";
my $live1 = BGPmon::Fetch::Live->new(server=>"123.345.67.89", port=>"50001");
isa_ok( $live1, 'BGPmon::Fetch::Live');

#check error code
my $ret = $live1->get_error_code();
is($ret, 0, "Error code with correct arguments");
is($live1->{'error_code'}, $live1->NO_ERROR_CODE, "Hash test");


#print "Creating a Live object with missing server and port arguments\n";
my $live2 = BGPmon::Fetch::Live->new();#(server=>"123.345.67.89", port=>"50001")
isa_ok( $live2, 'BGPmon::Fetch::Live');

#check error code, a Fetch.pm attribute inherited by Live.pm
$ret = $live2->get_error_code();
is($ret, 210, "Error code with incorrect arguments");

#Creating a Live object with one missing arguments\n";
my $live3 = BGPmon::Fetch::Live->new(server=>"123.345.67.89");
isa_ok( $live3, 'BGPmon::Fetch::Live');

#check error code
$ret = $live3->get_error_code();
is($ret, 210, "Error code while missing a required argument");

#Activation with missing required arguments should return 0 for failure
$ret = $live2->activate();
is($ret, 1, "Activation with missing required arguments");

#test connecting and reading xml msg
my $server = '127.0.0.1';
my $xml_msg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">129.250.0.11</ADDRESS><PORT>179</PORT><ASN4>2914</ASN4></SOURCE><DEST><ADDRESS afi="1">127.0.0.1</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.102</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1372636819</TIMESTAMP><DATETIME>2013-07-01T00:00:19Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>226102578</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>2914</bgp:ASN4><bgp:ASN4>7575</bgp:ASN4><bgp:ASN4>7575</bgp:ASN4><bgp:ASN4>7575</bgp:ASN4><bgp:ASN4>24436</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:MULTI_EXIT_DISC optional="true" transitive="false" partial="false" extended="false" attribute_type="4">184549376</bgp:MULTI_EXIT_DISC><bgp:MP_REACH_NLRI optional="true" transitive="false" partial="false" extended="true" attribute_type="14" safi="2"><bgp:MP_NEXT_HOP afi="1">129.250.0.11</bgp:MP_NEXT_HOP><bgp:MP_NLRI afi="1">192.150.139.0/24</bgp:MP_NLRI></bgp:MP_REACH_NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0073020000005C40010100400216020500000B6200001D9700001D9700001D9700005F748004040000000BC008200B62019A0B6203F00B6207D00B620BB81D9703E81D9708CB1D970BBCFF1651E5900E00110001020481FA000B0018C0968B18C09689</OCTET_MESSAGE>></BGP_MONITOR_MESSAGE>';

sub server {
	my $p = shift;
	my $socket = new IO::Socket::INET (
    	LocalHost => '127.0.0.1',
   		LocalPort => $p,
		  Proto => 'tcp',
    	Listen => 5,
    	Reuse => 1,
	) or die "Error creating socket: $!";

	my $client_socket = $socket->accept();

	# Write <xml> tag on client socket.
	$client_socket->send("<xml>");
	sleep(1);
	# Write XML message on client socket.
	for (my $i = 0; $i < 100; $i++) {
        $client_socket->send($xml_msg);
	}
	$socket->close();
}

# No server running, check if connected.
my $tempClient = BGPmon::Fetch::Live->new(server=>"127.0.0.1", port=>"64536");#fake port, should fail
$ret = $tempClient->is_activated();
ok($ret == 0, "actual: $ret expected 0");

# Read should fail.
my $msg = $tempClient->read_xml_message();
my $error_code = $tempClient->get_error_code('read_xml_message');
ok($error_code == 202, "actual: $error_code, expected: 202");

# Generate random port no.
my $port = int(rand(30000)) + 1024;

# Run stub server
my $pid = fork();
if ($pid == 0) {
	# Start server
	server($port);
} else {
	# Give some time for the server to fire up.
	sleep(1);
	# Try connecting.
        my $client = BGPmon::Fetch::Live->new(server=>"127.0.0.1", port=>$port);
        ok(defined($client), "Testing client is made properly");
	$ret = $client->activate();
	$error_code = $client->get_error_code();
	ok($ret == 0, "actual: $ret, expected: 0");
	ok($error_code == 0, "actual: $error_code, expected: 0");

        #trying to activate connection while already activated
	$ret = $client->activate();
	$error_code = $client->get_error_code();
	ok($ret == 1, "Already connected .. actual: $ret, expected: 1");
	ok($error_code == 203, "Already connected error code.. actual: $error_code, expected: 203");

	# Read an XML message.
	$msg = $client->read_xml_message();
	ok($msg eq $xml_msg, "xml msg was read properly");
	wait;
	$client->deactivate();
        $ret = $client->is_activated();
        ok($ret == 0,"client is disconnected .. actual: $ret, expected: 0");
        
        #trying to read while disconnected
	$msg = $client->read_xml_message();
	is($msg, undef, "Shouldn't be able to read with disconnected client");
}




done_testing();
