# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl BGPmon-Fetch-File.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use strict;
use warnings;

use Test::More;
BEGIN { use_ok('BGPmon::Fetch::File') }

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

my $first_msg =
"<ARCHIVER><TIME timestamp=\"1340136304\" datetime=\"2012-06-19T20:05:04Z\"/><EVENT cause=\"CREATE_NEW_FILE\">OPENED</EVENT></ARCHIVER>";

my $second_msg =
'<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">164.128.32.11</ADDRESS><PORT>179</PORT><ASN4>3303</ASN4></SOURCE><DEST><ADDRESS afi="1">127.0.0.1</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.102</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1372636819</TIMESTAMP><DATETIME>2013-07-01T00:00:19Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>226125243</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>3303</bgp:ASN4><bgp:ASN4>3491</bgp:ASN4><bgp:ASN4>18187</bgp:ASN4><bgp:ASN4>9821</bgp:ASN4><bgp:ASN4>9821</bgp:ASN4><bgp:ASN4>45600</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">164.128.32.11</bgp:NEXT_HOP><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3303</bgp:ASN2><bgp:VALUE>1004</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3303</bgp:ASN2><bgp:VALUE>1006</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3303</bgp:ASN2><bgp:VALUE>3052</bgp:VALUE></bgp:COMMUNITY><bgp:NLRI afi="3">MALLICIOUS_ADDRESS_INSERTED</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF005202000000374001010040021A020600000CE700000DA30000470B0000265D0000265D0000B220400304A480200BC0080C0CE703EC0CE703EE0CE70BEC16CA5C94</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

my $ret = undef;    #Return value of calls
my $err = undef;    #Log message

##################### connect_file ###########################################
##################### Validation conditions ##################################
my $test_xml_file = "t/bgpmon-fetch-file-valid.xml";
my @filelist = ($test_xml_file);
my $fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');

#Create a file with no archiver tags
#Write to test file

open( TESTFILE, '>', $test_xml_file ) or die();
print TESTFILE  "<xml>";
print TESTFILE $second_msg;
print TESTFILE "</xml>";
close TESTFILE;

#Connect w/ 1 correct argument
#Test should work
$ret = $fetch->activate();
is( $ret, 0, "activate with correct arguments" );
$ret = $fetch->get_error_code();
is( $ret, 0, "activate - check error code: NO_ERROR_CODE" );

print "+++++++++++++++++++++",$fetch->{'test_string'},"\n";
$fetch->deactivate();

#Compress test file
system("gzip $test_xml_file");
$test_xml_file = $test_xml_file . ".gz";


#Connect to a valid, gzipped file
@filelist = ($test_xml_file);
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');
$ret = $fetch->activate( );
is( $ret, 0, "activate with valid gzip archive" );
$ret = $fetch->get_error_code();
is( $ret, 0, "activate - check error code: NO_ERROR_CODE" );

my $msg = $fetch->read_xml_message();
ok( defined($msg), "read_xml_message -read compressed file" );
is( $fetch->get_error_code(), 0, "read_xml_message - error code" );
ok( $fetch->is_activated(), "read_xml_message - no ARCHIVER" );

$fetch->deactivate();

#Delete Test File
unlink $test_xml_file;

#Write to test file - With Archiver tags
$test_xml_file = "t/bgpmon-fetch-file-valid.xml";
my $test_xml_file2 = "t/bgpmon-fetch-file-2-valid.xml";
open( TESTFILE, '>', $test_xml_file ) or die();
print TESTFILE
  "<xml>"; 
print TESTFILE $first_msg;
print TESTFILE $second_msg;
print TESTFILE $second_msg;    #Second message is printed twice on purpose
print TESTFILE "</xml>";
close TESTFILE;

system("cat $test_xml_file > $test_xml_file2 ");

#Try to read the first message out of the file
#This should work
@filelist=($test_xml_file);
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');
$fetch->activate();
$msg = $fetch->read_xml_message();
ok( defined($msg), "read_xml_message - simple read" );

#Check we are still connected
ok( $fetch->is_activated(), "read_xml_message - is_activated" );
is( $fetch->get_error_code(), 0, "read_xml_message - error code" );

#Read the next message from the already-open connection
$msg = $fetch->read_xml_message();
ok( defined($msg), "read_xml_message - confirm message exists" );
chomp($msg);

is( $msg, $second_msg, "read_xml_message - check second message" );
is( $fetch->get_msgs_count(),
	2, "read_xml_message - read after failed connect" );

#Read the remainder of the messages
while ( defined($msg) ) {
	$msg = $fetch->read_xml_message();
}

#There are 3 messages in this file, the connection should be closed, and
#we shouldn't be able to re-close the connection
is( $fetch->get_msgs_count(), 3, "read_xml_message - read all messages" );
ok( !$fetch->is_activated(), "is_connected - not connected after EOF" );

is( $fetch->deactivate(), 1, "close_connection - close after closed" );

#Try to read from no connection
$msg = $fetch->read_xml_message();
is( $fetch->get_error_code(),
	302, "read_xml_message - read without connection" );

#Delete Test File
unlink $test_xml_file;

# read from a non-existant file
@filelist=($test_xml_file);
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');
$ret = $fetch->activate();
is( $ret, 1,   "activate - nonexistent file" );

is( $fetch->get_error_code(), 304, "activate - error code nonexistent file" );

# read from a file we don't have permissions for
@filelist=("/etc/sudoers");
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');
$ret = $fetch->activate();
is( $ret,                     1,   "activate - no permission on file" );
is( $fetch->get_error_code(), 307, "activate - bad permissions" );
system("mv $test_xml_file2 $test_xml_file2.badformat");

  # read from a wrong format file
  $ret = $fetch->activate( file => $test_xml_file2 );
is( $ret, 1, "activate - wrong format" );
ok( !$fetch->is_activated(), "activate - wrong format is_activated" );

unlink $test_xml_file2.".badformat";


open( TESTFILE, '>', $test_xml_file ) or die();
print TESTFILE
  "<xml>"; 
print TESTFILE $first_msg;
print TESTFILE $second_msg;
print TESTFILE $second_msg;    #Second message is printed twice on purpose
print TESTFILE "</xml>";
close TESTFILE;
# start reading, delete file, continue reading
@filelist=($test_xml_file);
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');
$ret = $fetch->activate();
$msg = $fetch->read_xml_message();
$msg = $fetch->read_xml_message();

unlink "/tmp/t/extract_bgp.$$.bgpmon-fetch-file-valid.xml";

$msg = $fetch->read_xml_message();
is( $msg, undef, "read_xml_message - delete a file mid-read" );

#Connect to a file, read a message, change the permissions, try to read again
#This should work because permissions only get checked once, not per-read
@filelist=($test_xml_file);
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');
$ret = $fetch->activate();
ok( $fetch->is_activated(), "confirm file connection" );
$msg = $fetch->read_xml_message();
`chmod a-r "/tmp/t/extract_bgp.$$.bgpmon-fetch-file-valid.xml"`;
$msg = $fetch->read_xml_message();
ok( defined($msg), "read_xml_message - chmod mid-read" );
#Connect to a file, read a message, move the file, try to read again
ok( $fetch->is_activated(), "confirm file connection" );
$msg = $fetch->read_xml_message();
ok( defined($msg), "read_xml_message - read before move" );
`mv "/tmp/t/extract_bgp.$$.bgpmon-fetch-file-valid.xml" "/tmp/extract_bgp.$$.bak"`;

#Now that we've moved the file that we were reading, the next read will fail
$msg = $fetch->read_xml_message();
is( $msg, undef, "read_xml_message - move mid-read" );

unlink $test_xml_file;

open( TESTFILE, '>', $test_xml_file ) or die();
print TESTFILE  "<xml>";
print TESTFILE $second_msg;
print TESTFILE '<BGP_MONITOR_MESSAGE><timestamp>1340136304</timestamp>This is test</BGP_MONITOR_MESSAGE>';
print TESTFILE "</xml>";
close TESTFILE;
open( TESTFILE, '>', "second.xml" ) or die();
print TESTFILE  "<xml>";
print TESTFILE '<BGP_MONITOR_MESSAGE><timestamp>1340136304</timestamp>This is test too</BGP_MONITOR_MESSAGE>';
print TESTFILE "</xml>";
close TESTFILE;


@filelist = ($test_xml_file,"second.xml");
$fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304');

$fetch->activate();
$msg = $fetch->read_xml_message();
while(defined($msg)){
    $msg = $fetch->read_xml_message();
}

unlink $test_xml_file,"second.xml";

done_testing();
