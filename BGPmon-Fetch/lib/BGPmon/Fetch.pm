package BGPmon::Fetch;


use strict;
use warnings;

use 5.14.2;

require Exporter;

our @ISA = qw(Exporter);

our %EXPORT_TAGS = (
	'all' => [
		qw( activate is_activated deactivate read_xml_message get_msgs_count get_error_code get_error_msg)
	]
);

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( 
UNDEFINED_ARGUMENT_CODE
);

our $VERSION = '0.9';

use constant NO_ERROR_CODE           => 0;
use constant NO_ERROR_MSG            => 'No Error';
use constant UNDEFINED_ARGUMENT_CODE => 301;
use constant UNDEFINED_ARGUMENT_MSG  => 'Undefined Argument(s)';
use constant UNCONNECTED_CODE        => 302;
use constant UNCONNECTED_MSG         => 'Not connected to a file';
use constant ALREADY_CONNECTED_CODE  => 303;
use constant ALREADY_CONNECTED_MSG   => 'Already connected to a file';
use constant NO_SUCH_FILE_CODE       => 304;
use constant NO_SUCH_FILE_MSG      => 'Specified file/directory does not exist';
use constant SYSCALL_FAIL_CODE     => 305;
use constant SYSCALL_FAIL_MSG      => 'System call failed';
use constant DECOMPRESS_FAIL_CODE  => 306;
use constant DECOMPRESS_FAIL_MSG   => 'Failed to decompress file';
use constant OPEN_FAIL_CODE        => 307;
use constant OPEN_FAIL_MSG         => 'Failed to open file';
use constant PARSER_INIT_FAIL_CODE => 308;
use constant PARSER_INIT_FAIL_MSG  => 'Failed to initialize XML Reader';
use constant PARSER_FATAL_ERROR_CODE => 309;
use constant PARSER_FATAL_ERROR_MSG  => 'XML Parser Error';
use constant FILE_FORMAT_ERROR_CODE  => 310;
use constant FILE_FORMAT_ERROR_MSG   => 'File must begin with <xml> tag';
use constant INVALID_FUNCTION_SPECIFIED_CODE => 311;
use constant INVALID_FUNCTION_SPECIFIED_MSG  => 'Invalid function specified';
use constant INIT_FAIL_CODE                  => 312;
use constant INIT_FAIL_MSG        => 'Failed to initialize file connection';
use constant INCOMPLETE_DATA_CODE => 313;
use constant INCOMPLETE_DATA_MSG =>
  'File is missing expected ARCHIVER messages';
use constant DATA_GAP_CODE     => 314;
use constant DATA_GAP_MSG      => 'File may be missing data';
use constant IGNORE_ERROR_CODE => 315;
use constant IGNORE_ERROR_MSG =>
  'Cannot have ignore_incomplete_data off with ignore_data_errors on';


use constant UNDEFINED_ARCHIVE_ARGUMENT_CODE => 401;
use constant UNDEFINED_ARCHIVE_ARGUMENT_MSG  => 'Undefined Argument(s)';
use constant UNCONNECTED_ARCHIVE_CODE        => 402;
use constant UNCONNECTED_ARCHIVE_MSG         => 'Not connected to an archive';
use constant ALREADY_CONNECTED_ARCHIVE_CODE  => 403;
use constant ALREADY_CONNECTED_ARCHIVE_MSG   => 'Already connected to an archive';
use constant NO_INDEX_PAGE_CODE      => 404;
use constant NO_INDEX_PAGE_MSG       => 'Unable to find an index page';
use constant SYSCALL_FAIL_ARCHIVE_CODE       => 405;
use constant SYSCALL_FAIL_ARCHIVE_MSG        => 'System call failed';
use constant INVALID_ARGUMENT_CODE   => 406;
use constant INVALID_ARGUMENT_MSG    => 'Invalid value given for argument';
use constant INIT_FAIL_ARCHIVE_CODE          => 407;
use constant INIT_FAIL_ARCHIVE_MSG => 'Failed to initialize connection to archive';
use constant FILE_OPERATION_FAIL_CODE        => 408;
use constant FILE_OPERATION_FAIL_MSG         => 'File operation failed';
use constant DOWNLOAD_FAIL_CODE              => 409;
use constant DOWNLOAD_FAIL_MSG               => 'Failed to download file';
use constant INVALID_MESSAGE_CODE            => 410;
use constant INVALID_MESSAGE_MSG             => 'Invalid message read';
use constant INVALID_FUNCTION_SPECIFIED_ARCHIVE_CODE => 411;
use constant INVALID_FUNCTION_SPECIFIED_ARCHIVE_MSG  => 'Invalid function specified';

=head1 NAME

BGPmon::Fetch 

=head1 SYNOPSIS

use BGPmon::Fetch;

=head1 SUBROUTINES/METHODS
=cut
  
=head2 new

Creates a new instance of BGPmon::Fetch
This will be called by BGPmon::Fetch::File, BGPmon::Fetch::Archive or BGPmon::Fetch::Live
Output: refrence to Fetch object
=cut
sub new {
	my $class = shift;
	my $self  = bless {
		msgs_read => 0,
		connected => 0,
		saw_start => 0,    #saw_start and saw_end track <ARCHIVER> tags
		saw_end   => 0,
		test_string => 'demo',		
		#Error codes and messages
		error_code => NO_ERROR_CODE,
		error_msg  => NO_ERROR_MSG,
	};

	bless $self, $class;
	return $self;
}

=head2 activate

Method is defined is BGPmon::Fetch::File or BGPmon::Fetch::Archive to either connect to local file or download all files of all peers to local directory by BGPmon::Fetch::Archive

=cut
sub activate {

}

=head2 is_activated

Test to see if we are connected / downloaded archive file and/or opened a file
=cut
sub is_activated {
	my $self = shift;
	return $self->{'connected'};
}

=head2 deactivate

disconnects from BGPmon / closes file
=cut
sub deactivate {

}

=head2 read_xml_message

Will return one full message from the source
=cut
sub read_xml_message {
	return undef;#A call should never reach here, Since File, 
		#Live and Archive will implement this function
}

=head2 get_msgs_count

Keeps track of how many messages have been read so far
=cut
sub get_msgs_count {
	my $self = shift;
	return $self->{'msgs_read'};
}

=head2 get_error_code

Gives user error code if an error occured
=cut
sub get_error_code {
	my $self = shift;
 	return $self->{'error_code'} ;
}

=head2 get_error_msg

Give user error message if an error occured
=cut
sub get_error_msg {
	my $self = shift;
	return $self->{'error_msg'} ;
}


=head1 ERROR CODES AND MESSAGES

The following error codes and messages are defined:

    0:  No Error
        'No Error'

    301:    An argument to a function was undefined
            'Undefined Argument(s)'

    302:    There is no active connection to a file
            'Not connected to a file'

    303:    There is a currently-active connection to a file
            'Already connected to a file'

    304:    The filename or directory given does not exist
            'Specified file/directory does not exist'

    305:    A system call failed
            'System call failed'

    306:    Decompressing the file failed
            'Failed to decompress file'

    307:    The file was not opened successfully
            'Failed to open file'

    308:    Initializing the XML Reader failed
            'Failed to initialize XML Reader'

    309:    The XML Reader encountered a fatal error
            'XML Parser Error'

    310:    The XML file passed in did not begin with an <xml> tag
            'File must begin with <xml> tag'

    311:    An invalid function name was passed to get_error_[code/message/msg]
            'Invalid function specified'

    312:    There was an error initializing one or more of the options to init_bgpdata
            'Failed to initialize file connection'

    313:    At least one of the beginning ARCHIVER/OPENING or ARCHIVER/CLOSE
                messages were missing from the file
                NOTE: Setting the ignore_data_errors flag will suppress this
            'File is missing expected ARCHIVER messages'

    314:    An additional ARCHIVER/OPENING message was encountered during file
                processing. This indicates a likely gap in the data.
                NOTE: Setting the ignore_incomplete_data flag will suppress this
            'File may be missing data'

    315:    User tried to ignore all data errors, but was checking for incomplete data
            'Cannot have ignore_incomplete_data off with ignore_data_errors on'
       
    401:    A subroutine was missing an expected argument
            'Undefined Argument(s)'

    402:    There is no active connection to an archive
            'Not connected to an archive'

    403:    There is a currently-active connection to an archive
            'Already connected to an archive'

    404:    The module was unable to find an HTML index page
                or any download links on the index page
            'Unable to find an index page'

    405:    A system call failed
            'System call failed'

    406:    An invalid value was passed to a subroutine as an argument
            'Invalid value given for argument'

    407:    The connection could not be initialized, either by a failure
                to set the scratch directory, ignore-error flags, or
                the first update file could not be loaded.
            'Failed to initialize connection to archive'

    408:    A filesystem 'open' command failed
            'File operation failed'

    409:    There was a failure trying to download a file
            'Failed to download file'

    410:    An invalid XML message was read, or the end of the archive was read
            'Invalid message read'

    411:    An invalid function name was passed to get_error_[code/message/msg]
            'Invalid function specified'

    412:    User tried to ignore all data errors, but was checking for
                incomplete data
            'Cannot have ignore_incomplete_data off with ignore_data_errors on'

=head1 AUTHOR

Anant Shah, C<< <akshah at rams.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to C<bgpmon at netsec.colostate.edu>
, or through the web interface at L<http://bgpmon.netsec.colostate.edu>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::Fetch

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2014 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\

    File: Fetch.pm
    Authors: Anant Shah

=cut


1;

