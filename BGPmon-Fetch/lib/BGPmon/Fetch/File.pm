package BGPmon::Fetch::File;

use strict;
use warnings;

use 5.14.2;

require BGPmon::Fetch;
require Exporter;

our @ISA = qw(Exporter BGPmon::Fetch);

use POSIX qw/strftime/;
use IO::Uncompress::AnyUncompress qw(anyuncompress $AnyUncompressError);
use File::Path qw/mkpath rmtree/;
use XML::LibXML::Reader;
use XML::LibXML::Simple qw(XMLin);
use File::Basename;
use Data::Dumper;

our %EXPORT_TAGS = (
    'all' => [
        qw(

          )
    ]
);

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(

);

our $VERSION = '0.9';

=head1 NAME

BGPmon::Fetch::File

The BGPmon::Fetch::File module connects to local XML file(s) and reads XML messages one at a time sequentially.
Multiple BGPmon::Fetch::File instances can read from multiple list of files concurrenlty.
This module uses BGPmon::Fetch module.

=head1 SYNOPSIS

The BGPmon::Fetch::File module provides functionality to connect
to an multiple XML files and read one message at a time sequentially.

    use BGPmon::Fetch::File;
    my @filelist = ('test_xml_file_1.xml','test_xml_file_2.xml');
    my $fetch = BGPmon::Fetch::File->new(file => \@filelist,begin_time=>'1340136304',end_time=>'1340137400');
    $fetch->activate();
    my $ret = $fetch->get_error_code();
    my $msg = $fetch->read_xml_message();
    $fetch->deactivate();


=head1 SUBROUTINES/METHODS
=cut

=head2 new

Creates a new instance of BGPmon::Fetch::File

Input:
    List of files
    Begin time
    End time (optional, default=current time)
    Scratch Directory (optional, default='/tmp/t/')

Usage:  $fetch->new(file => \@filelist,begin_time=>'1340136304',end_time=>'1340137400',scratch_dir=>'/tmp/scratch/');
=cut

sub new {
    my $class = shift;
    my (%args) = @_;

    # Call the constructor of the parent class
    my $self        = $class->SUPER::new(@_);
    my ($filenames) = undef;
    my $scratch_dir = "/tmp/t/";
    my $begin       = undef;
    my $end         = time;

    #Check if file is provided
    if ( exists( $args{'file'} ) ) {
        $filenames = $args{'file'};
    }
    else {
        $self->{'error_code'} = $self->UNDEFINED_ARGUMENT_CODE;
        $self->{'error_msg'}  = $self->UNDEFINED_ARGUMENT_MSG;
        return 1;
    }
    if ( exists( $args{'scratch_dir'} ) ) {
        $scratch_dir = $args{'scratch_dir'};
    }
    if ( !-d $scratch_dir ) {
        mkpath $scratch_dir;
    }

    if ( exists( $args{'begin_time'} ) ) {
        $begin = $args{'begin_time'};
    }
    else {
        $self->{'error_code'} = $self->UNDEFINED_ARGUMENT_CODE;
        $self->{'error_msg'}  = $self->UNDEFINED_ARGUMENT_MSG;
        return 1;
    }
    if ( exists( $args{'end_time'} ) ) {
        $end = $args{'end_time'};
    }

    # Add few more attributes

    $self->{'upd_fh'}       = undef;    #Filehandles
    $self->{'upd_filename'} = [];       #Filenames
    foreach my $f (@$filenames) {
        push( @{ $self->{'upd_filename'} }, $f );
    }

    $self->{'num_files'}    = scalar(@$filenames);    #Number of files
    $self->{'current_file'} = undef;
    $self->{'begin_time'}   = $begin;
    $self->{'end_time'}     = $end;
    $self->{'xml_reader'} = undef;    #Variable to use as the XML reader
    $self->{'depth'}      = -1;

    $self->{'scratch_dir'} = $scratch_dir;
    $self->{'output_file'} = "extract_bgp.$$";   #Appends process ID to filename

    bless $self, $class;
    return $self;
}

=head2 activate

Initializes the scratch directory after validating time interval.

Input:      None

Output:     1 if initialization fails
            0 if initialization succeeds

Usage:      $fetch->activate();

=cut

sub activate {

    my $self     = shift;
    my $filename = shift( @{ $self->{'upd_filename'} } );

    if ( !defined($filename) ) {
        return 1;
    }

    if ( defined( $self->{'end_time'} ) ) {
        if (   $self->{'begin_time'} =~ /[a-zA-Z]/
            || $self->{'end_time'} =~ /[a-zA-Z]/
            || $self->{'begin_time'} < 0
            || $self->{'end_time'} < 0
            || $self->{'begin_time'} > $self->{'end_time'} )
        {
            $self->{'error_code'} = $self->INVALID_ARGUMENT_CODE;
            $self->{'error_msg'}  = $self->INVALID_ARGUMENT_MSG;
            return 1;
        }
    }

    #The file must exist before we can connect to it
    if ( !$self->file_check( file => $filename ) ) {
        $self->{'error_code'} = $self->NO_SUCH_FILE_CODE;
        $self->{'error_msg'}  = $self->NO_SUCH_FILE_MSG;
        return 1;
    }

    #TODO:Put an if here to avoid trying to decompress .xml files

    #decompress_file
    my $ret = $self->decompress_file( file => $filename );
    $filename = $ret if defined $ret;    #Returns decompressed file name

    #Now open the file
    unless ( open( $self->{'upd_fh'}, "<", $filename ) ) {
        $self->{'error_code'} = $self->OPEN_FAIL_CODE;
        $self->{'error_msg'}  = $self->OPEN_FAIL_MSG . ": $@";
        return 1;
    }

    #Instantiate the XML parser
    unless ( $self->{'xml_reader'} =
        XML::LibXML::Reader->new( IO => $self->{'upd_fh'}, recover => 1 ) )
    {
        $self->{'error_code'} = $self->PARSER_INIT_FAIL_CODE;
        $self->{'error_msg'}  = $self->PARSER_INIT_FAIL_MSG;
        close( $self->{'upd_fh'} );
        return 1;
    }

    #For now need xml tag
    $self->{'depth'} = 1;

    #We require all files to start w/ an <xml> tag; fail if it is not present.
    eval {
        if ( $self->{'xml_reader'}->read() == 1 ) {
            if ( $self->{'xml_reader'}->localName() eq "xml" ) {
                $self->{'depth'} = 1;
            }

#For now we need the <xml> tag so ignore this else raise error instead
#else {
#	$self->{'depth'} = 0;
#}
        }
        else {
            $self->{'error_code'} = $self->FILE_FORMAT_ERROR_CODE;
            $self->{'error_msg'}  = $self->FILE_FORMAT_ERROR_MSG;
            close( $self->{'upd_fh'} );
            return 1;
        }
        1;
    } or do {
        $self->{'error_code'} = $self->PARSER_FATAL_ERROR_CODE;
        $self->{'error_msg'}  = $self->PARSER_FATAL_ERROR_MSG . ": $@";
        close( $self->{'upd_fh'} );
        return 1;
    };

    #Now that we have successfully connected, set remaining state variables
    #for this connection
    $self->{'connected'}    = 1;
    $self->{'current_file'} = $filename;
    $self->{'next_file'}    = 1;
    $self->{'error_code'}   = $self->NO_ERROR_CODE;
    $self->{'error_msg'}    = $self->NO_ERROR_MSG;
    return 0;
}

=head2 deactivate

Function to close and delete any files

Input: None

Output: 1 on error
        0 on success

Usage:  $file->deactivate();
=cut

sub deactivate {
    my $self = shift;

    #Can't close a connection that isn't there...
    if ( !$self->is_activated() ) {
        $self->{'error_code'} = $self->UNCONNECTED_CODE;
        $self->{'error_msg'}  = $self->UNCONNECTED_MSG;
        return 1;
    }

    #Close the XML reader
    $self->{'xml_reader'}->close();

    #Close the open file handle
    close( $self->{'upd_fh'} ) if defined( fileno $self->{'upd_fh'} );

    eval {
        my $errs;
        rmtree( $self->{'scratch_dir'},
            { keep_root => 1, safe => 1, error => \$errs } );
        1;
    } or do {
        $self->{'error_code'} = $self->SYSCALL_FAIL_CODE;
        $self->{'error_msg'}  = $self->SYSCALL_FAIL_MSG . ": $@";
    };

    $self->{'upd_fh'}     = undef;
    $self->{'error_code'} = $self->NO_ERROR_CODE;
    $self->{'error_msg'}  = $self->NO_ERROR_MSG;
    $self->{'connected'}  = 0;
    return 0;

}
my $timestamp_of_message = undef;    #This is used by extract_timestamp function

#next_message is used by read_xml_message func. Performs error checking and loops to next file in the list.
sub next_message {
    my $self = shift;

    #There must be an open file connection to read from
    if ( !$self->is_activated() ) {
        $self->{'error_code'} = $self->UNCONNECTED_CODE;
        $self->{'error_msg'}  = $self->UNCONNECTED_MSG;
        return undef;
    }

    #The file that we're supposed to be connected to had better still be there
    if ( !$self->file_check( file => $self->{'current_file'} ) ) {
        $self->{'error_code'} = $self->NO_SUCH_FILE_CODE;
        $self->{'error_msg'}  = $self->NO_SUCH_FILE_MSG;
        $self->deactivate();
        return undef;
    }

    my $complete_xml_msg = "";

    while ($self->{'xml_reader'}->depth() != $self->{'depth'}
        || $self->{'xml_reader'}->nodeType() ==
        XML_READER_TYPE_SIGNIFICANT_WHITESPACE )
    {

        if ( $self->{'xml_reader'}->read() == 0 ) {

            #This case catches any XML parser error.
            if ($?) {
                print "\nParser error\n\n";
                $self->{'error_code'} = $self->PARSER_FATAL_ERROR_CODE;
                $self->{'error_msg'}  = $self->PARSER_FATAL_ERROR_MSG . ": $@";
            }

            #Start new file or terminate.
            #

            if ( $self->activate() == 1 ) {

                #New file doesnt activate so terminate

                $self->deactivate();
                return undef;
            }

            return undef if !$self->{'xml_reader'}->read();

        }
    }

    $complete_xml_msg .= $self->{'xml_reader'}->readOuterXml();

    if ( !defined($complete_xml_msg) ) {
        $self->{'error_code'} = $self->FILE_FORMAT_ERROR_CODE;
        $self->{'error_msg'}  = $self->FILE_FORMAT_ERROR_MSG . ": $@";
        $self->deactivate();
        return undef;
    }

    $self->{'xml_reader'}->next();
    return $complete_xml_msg;

}

=head2 read_xml_message

This function reads one XML message at a time from an open file connection.

Input:  None, but assumes activate() has been called

Output: The next available XML message in given time interval from the current file, or starts reading next file in the list until all files are read.

Usage:  my $msg = $fetch->read_xml_message();
=cut

sub read_xml_message {
    my $self = shift;
    my $msg  = undef;

    while (1) {
        $msg = $self->next_message();
        if ( defined($msg) ) {
            my $xml_in = XMLin($msg);
            $timestamp_of_message = undef;
            my $msg_time = "";
            $msg_time = extract_timestamp($xml_in);

            #Evaluate its timestamp
            if ( !defined($msg_time) ) {
                $self->{'error_code'} = $self->INVALID_MESSAGE_CODE;
                $self->{'error_msg'}  = $self->INVALID_MESSAGE_MSG;
                return undef;
            }

            #if the message is too early, get the next one
            if ( $msg_time < $self->{'begin_time'} ) {
                next;
            }

            #If the message time >= start time but <= end_time, return it
            if ( $msg_time <= $self->{'end_time'} ) {
                $self->{'error_code'} = $self->NO_ERROR_CODE;
                $self->{'error_msg'}  = $self->NO_ERROR_MSG;
                $self->{'msgs_read'} += 1;
                return $msg;
            }

        }
        else {
            $self->deactivate();
            return undef;
        }
    }
}

#Helper function to extract timestamp from a message
#Returns undef if no TIMESTAMP is found
sub extract_timestamp {
    my ($hash) = @_;

    while ( my ( $key, $value ) = each %$hash ) {
        if ( 'HASH' eq ref $value ) {
            extract_timestamp($value);
        }
        else {
            if ( $key =~ /TIMESTAMP/i ) {
                $timestamp_of_message = $value;
            }
        }
    }
    return $timestamp_of_message;
}

# file_check

#This function checks whether or not the currently-open file exists
#Input:  A filename to check
#Output: 1 if the file exists, 0 otherwise

sub file_check {
    my $self   = shift;
    my $file   = undef;
    my (%args) = @_;
    if ( exists( $args{'file'} ) ) {
        $file = $args{'file'};
    }
    else {
        $self->{'error_code'} = $self->UNDEFINED_ARGUMENT_CODE;
        $self->{'error_msg'}  = $self->UNDEFINED_ARGUMENT_MSG;
        return 0;
    }

    if ( -e $file ) {
        return 1;
    }
    else {
        $self->{'error_code'} = $self->NO_SUCH_FILE_CODE;
        $self->{'error_msg'}  = $self->NO_SUCH_FILE_MSG;
        return 0;
    }
}

# decompress_file

#This function decompresses a file using Perl's IO::Uncompress library.
#Currently supports gzip, bzip2, RFC 1950/1951, zip,lzop,lzf,lzma,xz
#Input:      The filename to uncompress
#Returns:    undef on failure, the name of the uncompressed file on success

sub decompress_file {
    my $self   = shift;
    my $file   = undef;
    my (%args) = @_;

    if ( exists( $args{'file'} ) ) {
        $file = $args{'file'};
    }
    else {
        $self->{'error_code'} = $self->UNDEFINED_ARGUMENT_CODE;
        $self->{'error_msg'}  = $self->UNDEFINED_ARGUMENT_MSG;
        return undef;
    }
    if ( $self->is_activated() ) {
        $self->{'error_code'} = $self->ALREADY_CONNECTED_CODE;
        $self->{'error_msg'}  = $self->ALREADY_CONNECTED_MSG;
        return undef;
    }

    if ( !$self->file_check( file => $file ) ) {
        $self->{'error_code'} = $self->NO_SUCH_FILE_CODE;
        $self->{'error_msg'}  = $self->NO_SUCH_FILE_MSG;
        return undef;
    }
    my $name =
      $self->{'scratch_dir'} . $self->{'output_file'} . "." . basename($file);

    unless ( anyuncompress $file => $name ) {
        $self->{'error_code'} = $self->DECOMPRESS_FAIL_CODE;
        $self->{'error_msg'}  = $self->DECOMPRESS_FAIL_MSG . ": $@";
        return undef;
    }

    return $name;
}

=head1 AUTHOR(S)

    Anant Shah, C<< <akshah at rams.colostate.edu> >>
    M. Lawrence Weikum, C<< <mweikum at rams.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to C<bgpmon at netsec.colostate.edu>
, or through the web interface at L<http://bgpmon.netsec.colostate.edu>.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::Fetch::File

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2014 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\

    File: File.pm
    Authors: Anant Shah, M. Lawrence Weikum

=cut

1;
