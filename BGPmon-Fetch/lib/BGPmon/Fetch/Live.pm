package BGPmon::Fetch::Live;

our $VERSION = '0.01';

use 5.10.0;
use strict;
use warnings;
use IO::Socket;
use IO::Select;
use BGPmon::Fetch;
use POSIX qw/strftime/;

use constant FALSE => 0;
use constant TRUE => 1;


#Worked without the following 2 lines for local testing, but try to uncomment
#them if required, also try uncomment %EXPORT_TAGS, @EXPORT_OK, and @EXPORT below:
#require BGPmon::Fetch;
#require Exporter;
our @ISA = qw(Exporter BGPmon::Fetch);


# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
# This allows declaration	use BGPmon::Fetch::File ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.

# As a general rule, if the module is trying to be object oriented then export
# nothing. If it's just a collection of functions then @EXPORT_OK anything but
# use @EXPORT with caution.
# for now, keep the following untill you're sure they aren't needed!
# our %EXPORT_TAGS = ('all' => [qw( )	]);
# our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
# our @EXPORT = qw();


#Error messages
use constant NO_ERROR_CODE => 0; #moved to Fetch.pm
use constant NO_ERROR_MSG => 'No error.'; #moved to Fetch.pm
use constant CONNECT_ERROR_CODE => 201;
use constant CONNECT_ERROR_MSG => 'Could not connect to BGPmon.';
use constant NOT_CONNECTED_CODE => 202;
use constant NOT_CONNECTED_MSG => 'Not connected to a BGPmon instance.';
use constant ALREADY_CONNECTED_CODE => 203;
use constant ALREADY_CONNECTED_MSG => 'Already connected to a BGPmon instance.';
use constant XML_MSG_READ_ERROR_CODE => 204;
use constant XML_MSG_READ_ERROR_MSG => 'Error reading XML message from stream.';
use constant CONNECTION_CLOSED_CODE => 205;
use constant CONNECTION_CLOSED_MSG => 'Connection to BGPmon closed.';
use constant INVALID_NUM_BYTES_ERROR_CODE => 206;
use constant INVALID_NUM_BYTES_ERROR_MSG =>
  'Attempting to read invalid number of bytes.';
use constant READ_ERROR_CODE => 207;
use constant READ_ERROR_MSG =>
  'Could not read from file handle. Error in sysread.';
use constant SOCKET_TIMEOUT_CODE => 208;
use constant SOCKET_TIMEOUT_MSG => 'Socket timed out.';
use constant NON_PRINTABLE_CHARS_CODE => 209;
use constant NON_PRINTABLE_CHARS_MSG =>
  'Received XML messages contains non-printable characters';

#Missing server or port arguments at Live object instantiation
#will give user same error code for any missing argument
#error-msg will tell what is missing: server, port, or both
use constant UNDEFINED_ARG_CODE => 210;
use constant UNDEFINED_SERVER_ARGUMENT_MSG  => 'Missing server argument';
use constant UNDEFINED_PORT_ARGUMENT_MSG  => 'Missing port argument';

#this should be moved to Fetch.pm
use constant UNKNOWN_ERROR_CODE => 299;
use constant UNKNOWN_ERROR_MSG => 'Unknown error occurred.';




=head1 NAME

BGPmon::Fetch::Live

The Live module is a mean to connect to BGPmon and receive XML messages
one at a time.  Multiple instances of OOClient may be used within the same
script as it is Object Oriented

=cut

=head1 SYNOPSIS


The BGPmon::Fetch::Live module provides functionality to connect to a bgpmon
instance and read one XML message at a time.

    use BGPmon::Fetch::Live;

    #A new instance requires arguments for: BGPmon server, port and
    #TODO: connection timeout and Reconnect limits - TO DISCUSS LATER
    #either max reconnect attempts or max time to reconnect.


    Creating a Live object example: The PeerAddr can be a hostname or the
    IP-address on the "xx.xx.xx.xx" form. The PeerPort can be a number or a
    symbolic service name

    my $handle = BGPmon::Fetch::Live->new(server=>'bgpdata3.netsec.colostate.edu',
        port=>50001);
    OR using server IP

    my $handle = BGPmon::Fetch::Live->new(server=>"123.345.67.89", port=>50001)


    my $ret = $handle->activate();#ret could be used to check for connection status
    $handle->set_timeout($time_out_seconds);
    my $xml_msg = $handle->read_xml_message();
    my $ret = $handle->is_activated(); # was is_connected();
    my $num_read = $handle->get_msgs_count();
    my $ret = $handle->deactivate(); # was close_connection();
    my $downtime = $handle->connection_endtime();
=cut

=head1 EXPORT

new
activate
set_timeout
read_xml_essage
deactivate # was close_connection
connection_endtime
is_activated # was is_connected



=cut

=head1 SUBROUTINES/METHODS

=cut

=head2 new

Creates a new Live boject

Input: IPaddress or a hostname, port .. (Future work: add timeout and reconnect)

Output: undef on error
        class reference on success

=cut

sub new {
    my $class = shift;

    # call parent class (Fetch) constructor. This should initialize all parent
    # attributes
    my $self = $class->SUPER::new();

    # might need to move the following code to activate
    # this assumes user arguments are in the form:
    # Class-hierarchy->new(server=>"123.345.67.89", port=>50001)
    my %args = @_;


    # server
    if (exists $args{'server'}){
       $self->{'server'} = $args{'server'};
    } else {
           $self->{'error_code'} = UNDEFINED_ARG_CODE;
           $self->{'error_msg'}  = UNDEFINED_SERVER_ARGUMENT_MSG;
    }

    # port
    if (exists $args{'port'}){
       $self->{'port'} = $args{'port'};
    } else {
           if($self->{'error_code'} != 210){ #no arguments error was set previously
              $self->{'error_code'} = UNDEFINED_ARGUMENT_CODE;
              $self->{'error_msg'}  = UNDEFINED_PORT_ARGUMENT_MSG;
           } else {
              #error_code is already set
              #msg from previous error is concatenated with the new one
              $self->{'error_msg'}  .= " and " . UNDEFINED_PORT_ARGUMENT_MSG;
           }
    }

    #TODO the following two attributes are for future use
    #if used, they should be rewritten as in server and host above
    #$self->{'timeout'} = $args{'timeout'} if exists $args{'timeout'};
    #$self->{'reconnect'} = $args{'reconnect'} if exists $args{'reconnect'};

    #other attributes:
    #connection status
    #$self->{'msgs_read'} and $self->{'connected'} are are defined in Fetch.pm
    $self->{'activation_time'} = undef; #would be set at the start of connection
    $self->{'activation_end_time'} = undef; #would be set at the end of connection


    # socket and persistent buffer for storing partial messages
    $self->{'sock'} = undef;
    $self->{'sel'} = undef;
    $self->{'read_buffer'} = "";
    $self->{'chunksize'} = 1024;
    $self->{'socket_timeout'} = 600, # Socket times out after 10 min.
    $self->{'socket_buffer'} = [];

    bless $self, $class;
    return $self;
}


=head2 set_timeout

Sets the socket timeout value in seconds. Takes one argument -
the timeout value in seconds.

Input: Timeout in seconds

Output: None
=cut

#This timeout will be used to specify how long to wait for handles that are
#ready for reading. Might get red of this and use timeout value provided by user
#when a new Fetch::Live instance is created
sub set_timeout {
    my $self = shift;
    $self->{'socket_timeout'} = shift;
    $self->set_no_error();
}

=head2 activate

This function connects to the BGPmon server. If the connection succeeds, the
function attempts to read the starting <xml> tag from the BGPmon server. If
that succeeds, the created socket is returned. If not, the function returns
undef.

Input: None

Output: undef on failure
        Socket on success
=cut

sub activate {

    my $self = shift;

    #check for server and port error codes set in 'new' due to missing server or
    #port arguments for the oject
    if($self->{'error_code'} != NO_ERROR_CODE){
       return 1; #failed to connect
    }

    #check if oject already connected, cannot have more than one connection!
    if ($self->{'connected'} == TRUE) { # or use $self->is_activated() from Fetch
        $self->{'error_code'} = ALREADY_CONNECTED_CODE;
        $self->{'error_msg'} = ALREADY_CONNECTED_MSG;
        return 1; #failed to connect
    }

    my $server = $self->{'server'};
    my $port = $self->{'port'};

    # Connect to the BGPmon instance to receive XML xml_stream
    $self->{'sock'} = new IO::Socket::INET(
      PeerAddr => $server, PeerPort => $port, Proto => 'tcp');
    if(!defined $self->{'sock'}) {
        $self->{'error_code'} = CONNECT_ERROR_CODE;
        $self->{'error_msg'} = CONNECT_ERROR_MSG;
        return 1; #failed to connect
    }

    # Create select object, It allows the user to see what IO handles, are ready
    # for reading, writing or have an exception pending.
    $self->{'sel'} = new IO::Select->new();

    #add the already created socket to the list of handles that select has
    $self->{'sel'}->add($self->{'sock'});

    my $data;
    # can_read returns an array of handles that are ready for reading.
    my @ready = $self->{'sel'}->can_read($self->{'socket_timeout'});
    if (scalar(@ready) > 0) { #We have data on socket.
        $data = $self->read_n_bytes(5);
        if (!defined $data) {
            $self->{'error_code'} = XML_MSG_READ_ERROR_CODE;
            $self->{'error_msg'} = XML_MSG_READ_ERROR_MSG;
            $self->{'sel'}->remove($self->{'sock'});
            close($self->{'sock'});
            return 1; #failed to connect
        }
        if ($data ne "<xml>") {
            $self->{'error_code'} = XML_MSG_READ_ERROR_CODE;
            $self->{'error_msg'} = XML_MSG_READ_ERROR_MSG;
            $self->{'sel'}->remove($self->{'sock'});
            close($self->{'sock'});
            return 1; #failed to connect
        }
        $self->{'connected'} = TRUE;
        $self->{'activation_time'} = time;
        $self->{'msgs_read'} = 0;

        $self->set_no_error();
        return 0; #connected successfuly
    }

    # Socket timed out.
    $self->{'error_code'} = SOCKET_TIMEOUT_CODE;
    $self->{'error_msg'} = SOCKET_TIMEOUT_MSG;
    $self->{'sel'}->remove($self->{'sock'});
    close($self->{'sock'});
    return 1; #failed to connect
}

=head2 read_xml_message

This function reads one xml message at a time from the BGPmon XML stream.

Input: None

Output: undef on falure
        String on success

=cut

sub read_xml_message {
    my $self = shift;
    my $complete_xml_msg = undef;
    my $fname = 'read_xml_message';

    # Check if we are connected to a server
    unless ($self->{'connected'} == 1) {
        $self->{'error_code'} = NOT_CONNECTED_CODE;
        $self->{'error_msg'} = NOT_CONNECTED_MSG;
        return undef;
    }

    my $tempbuf;
    my $bytesread = 0;
    my @ready = $self->{'sel'}->can_read($self->{'socket_timeout'});

    # If there is a socket with data, read data from the socket.
    if (scalar(@ready) > 0) {
        while ($self->{'read_buffer'} !~ /<BGP_MONITOR_MESSAGE.*?BGP_MONITOR_MESSAGE>/) {
            #sysread Returns the number of bytes actually read,
            #0 at end of file, or undef if there was an error
            $bytesread = sysread($self->{'sock'}, $tempbuf, $self->{'chunksize'});
            if (!defined $bytesread) {
                $self->{'error_code'} = XML_MSG_READ_ERROR_CODE;
                $self->{'error_msg'} = XML_MSG_READ_ERROR_MSG;
                $self->deactivate(); #close_connection();
                return undef;
            }
            if ($bytesread == 0) {
                $self->{'error_code'} = CONNECTION_CLOSED_CODE;
                $self->{'error_msg'} = CONNECTION_CLOSED_MSG;
                $self->deactivate(); #close_connection();
                return undef;
            }
            $self->{'read_buffer'} .= $tempbuf;
        }

        # check if message contains only ASCII printable characters
        unless ($self->{'read_buffer'} =~ /[[:print:]]/) {
            $self->{'error_code'} = NON_PRINTABLE_CHARS_CODE;
            $self->{'error_msg'} = NON_PRINTABLE_CHARS_MSG;
            return undef;
        }

        # at this point I have a complete message OR my socket closed
        if ($self->{'read_buffer'} =~ /^(<BGP_MONITOR_MESSAGE.*?BGP_MONITOR_MESSAGE>)/) {
            $complete_xml_msg = $1;
            $self->{'msgs_read'} += 1;
            $self->{'read_buffer'} =~ s/^<BGP_MONITOR_MESSAGE.*?BGP_MONITOR_MESSAGE>//;
        } else {
            $self->{'error_code'} = UNKNOWN_ERROR_CODE;
            $self->{'error_msg'} = UNKNOWN_ERROR_MSG;
        }
        $self->set_no_error();
        return $complete_xml_msg;
    }
    # Timeout.
    $self->{'error_code'} = SOCKET_TIMEOUT_CODE;
    $self->{'error_msg'} = SOCKET_TIMEOUT_MSG;
    return undef;
}

#close_connection
=head2 deactivate

Function to close open files and sockets.

Input: None

Output: 0 on success

=cut

sub deactivate { # was close_connection {
    my $self = shift;
    $self->{'connected'} = 0;
    $self->{'activation_end_time'} = time;
    if ($self->{'sock'}) {
        $self->{'sel'}->remove($self->{'sock'});
        $self->{'sock'}->close();
    }
    $self->set_no_error();
    return 1;
}

=head2 is_activated

Function to report whether currently connected to BGPmon.

Input: None

Output: 1 if connected
        0 if not connected


=cut

sub is_activated { #is_connected {
    my $self = shift;
    return $self->{'connected'};
}



=head2 connection_endtime

Returns the time the connection ended .
If the connection is up, return 0.

=cut

sub connection_endtime {
  my $self = shift;
    if ($self->{'connected'}) {
        return 0;
    }
    $self->set_no_error();
    return $self->{'activation_end_time'};

}

=head2 set_no_error

set error code and msg to indicate no errors are there.

=cut

sub set_no_error{
   my $self = shift;
   $self->{'error_code'} = NO_ERROR_CODE;
   $self->{'error_msg'} = NO_ERROR_MSG ;
}


=head2 read_n_bytes

This function reads exactly n bytes from a connection.
returns undef on any error or connection close

=cut

sub read_n_bytes {
    my $self = shift;
    my ($bytesneeded) = shift;
    my $fname = 'read_n_bytes';
    if ($bytesneeded < 0) {
        $self->{'error_code'} = INVALID_NUM_BYTES_ERROR_CODE;
        $self->{'error_msg'} = INVALID_NUM_BYTES_ERROR_MSG;
        return undef;
    }
    my $data = "";
    my $buf = "";
    my $bytesread = 0;
    while($bytesneeded > 0) {
        $bytesread = sysread($self->{'sock'}, $buf, $bytesneeded);
        if (!defined $bytesread) {
            $self->{'error_code'} = READ_ERROR_CODE;
            $self->{'error_msg'} = READ_ERROR_MSG . ": $!";
            return undef;
        }
        if ($bytesread == 0) {
            $self->{'error_code'} = CONNECTION_CLOSED_CODE;
            $self->{'error_msg'} = CONNECTION_CLOSED_MSG;
            return undef;
        }
        $data .= $buf;
        $bytesneeded = $bytesneeded - $bytesread;
    }
    $self->set_no_error();
    return $data;
}




=head1 AUTHOR

Manaf Gharaibeh, C<< <gharaibe at cs.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bgpmon at netsec.colostate.edu>, or through the web interface at
L<http://bgpmon.netsec.colostate.edu>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::Fetch::Live

=cut

=head1 LICENSE AND COPYRIGHT
Copyright (c) 2014 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\


  File: Live.pm
  Authors: Manaf Gharaibeh, (originally M. Lawrnece Weikum)
  Date: 4 April 2014

=cut
1; # End of BGPmon::Fetch::Live
