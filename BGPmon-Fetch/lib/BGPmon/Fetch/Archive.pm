package BGPmon::Fetch::Archive;

use strict;
use warnings;

use 5.14.2;

use POSIX qw/strftime/;
use File::Path qw/mkpath rmtree/;

use LWP::Simple;
use Data::Dumper;

use BGPmon::Fetch::File;
require BGPmon::Fetch;
require Exporter;

our @ISA = qw(Exporter BGPmon::Fetch);

# By default, connect to our archive.
use constant DEFAULT_BGPMON_ARCHIVE =>
'http://archive-test.netsec.colostate.edu/peers/';

our %EXPORT_TAGS = (
	'all' => [
	qw(

	)
	]
);

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(

);

our $VERSION = '0.9';

=head1 NAME

BGPmon::Fetch::Archive 

The BGPmon::Fetch::Archive connects to an online archive of BGPmon files, download XML files and read XML messages one at a time. 

This module uses BGPmon::Fetch module.

=head1 SYNOPSIS

BGPmon::Fetch::Archive module provides functionality to connect to multiple peers and download XML files falling in given time range and read the messages in sequence starting with first peer in the list. 
	use BGPmon::Fetch::Archive;
	my @peerlist=('89.149.178.10','65.49.129.101');
	my $fetch = BGPmon::Fetch::Archive->new(url=>'archive.netsec.colostate.edu/peers',peer => \@peerlist,begin_time=>'1346449800',end_time=>'1346455000',ribs=>'0');
	$fetch->activate();
	my $ret = $fetch->get_error_code();
	my $msg = $fetch->read_xml_message();
	$fetch->deactivate();


=head1 SUBROUTINES/METHODS
=cut

=head2 new

Creates a new instance of BGPmon::Fetch::Archive

Input: 
	Archive URL
	List of Peers
	Begin time
	End time
	RIBS (optional, default=UPDATES)
	Scratch Directory (optional, default='/tmp/')

Usage:  my $fetch = BGPmon::Fetch::Archive->new(url=>'archive.netsec.colostate.edu/peers',
peer =>\@peerlist,begin_time=>'1346449800',end_time=>'1346455000',ribs=>'0',scratch_dir=>'/tmp/scratch/');
=cut
sub new {
	my $class = shift;
	my (%args) = @_;

	# Call the constructor of the parent class
	my $self            = $class->SUPER::new(@_);
	my ($peers)         = undef;
	my ($list_of_files) = undef;

	my $scratch_dir = "/tmp/";
	if ( exists( $args{'scratch_dir'} ) ) {
		$scratch_dir = $args{'scratch_dir'};
		if ( !-d $scratch_dir ) {
			mkpath $scratch_dir;
		}
	}

	#Check if peer is provided
	if ( exists( $args{'peer'} ) ) {
		$peers = $args{'peer'};
	}
	else {
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return 1;
	}

	#state variables to maintain state between calls to read_xml_message
	$self->{'url'}        = undef;    #Root URL to retrieve  files from
	$self->{'begin_time'} = undef;    #interval start time
	$self->{'end_time'}   = undef;    #interval end time
	$self->{'year'}       = undef;    #year/month used for URL construction
	$self->{'month'}      = undef;
	$self->{'day'}        = undef;
	$self->{'hour'}       = undef;
	$self->{'minute'}     = undef;
	$self->{'ribs'} = 0;    # determine if we are retrieving RIBs or UPDATES
	$self->{'append'} =
	undef;    #variable detects /UPDATES/ dirs below the normal depth
	$self->{'index_page'}           = [];           #list of HTML download links
	$self->{'scratch_dir'}          = $scratch_dir;
	$self->{'files_read'}           = 0;
	$self->{'file_obj'}             = undef;
	$self->{'current_peer'}         = undef;
	$self->{'need_next_index_file'} = undef;
	$self->{'peers'}                = [];           #Filenames

	foreach my $f (@$peers) {
		push( @{ $self->{'peers'} }, $f );
	}

	$self->{'num_peers'} = scalar(@$peers);         #Number of Peers

	# Check whether we are retrieving RIBs or UPDATEs
	if ( exists( $args{'ribs'} ) ) {
		$self->{'ribs'} = $args{'ribs'};
	}

	#Store arguments
	if ( exists( $args{'url'} ) ) {
		$self->{'url'} = $args{'url'};
	}
	else {
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return 1;
	}
	if ( exists( $args{'begin'} ) ) {
		$self->{'begin_time'} = $args{'begin'};
	}
	else {
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return 1;
	}
	if ( exists( $args{'end'} ) ) {
		$self->{'end_time'} = $args{'end'};
	}

	bless $self, $class;

	return $self;

}

=head2 activate

Initializes the scratch directory after validating time interval. Downloads all required files for all peers in the list. 
Uses BGPmon::Fetch::File instances to read these files sequentially. Since this function downloads all the files at once, it might take some time. 

Input:      None

Output:     1 if initialization fails
			0 if initialization succeeds

Usage:      $fetch->activate();

=cut
sub activate {
	my $self = shift;

	my $url   = $self->{'url'};
	my $begin = $self->{'begin_time'};
	my $end   = $self->{'end_time'};

	#check to make sure start and end time are proper UNIX timestamps
	#and that the start time <= end time and the URL has no garbage characters
	if ( defined( $self->{'end_time'} ) ) {
		if (   $self->{'begin_time'} =~ /[a-zA-Z]/
			|| $self->{'end_time'} =~ /[a-zA-Z]/
			|| $self->{'begin_time'} < 0
			|| $self->{'end_time'} < 0
			|| $self->{'begin_time'} > $self->{'end_time'}
			|| $self->{'url'} =~ m/[^[:graph:]]/ )
		{
			$self->{'error_code'} = $self->INVALID_ARGUMENT_CODE;
			$self->{'error_msg'}  = $self->INVALID_ARGUMENT_MSG;
			return 1;
		}
	}

	#Check to make sure we aren't already connected to an archive
	if ( $self->is_activated() ) {
		$self->{'error_code'} = $self->ALREADY_CONNECTED_ARCHIVE_CODE;
		$self->{'error_msg'}  = $self->ALREADY_CONNECTED_ARCHIVE_MSG;
		return 1;
	}

	#Select first peer
	$self->{'current_peer'} = shift( @{ $self->{'peers'} } );

	#Need to have at least one peer
	if ( !defined( $self->{'current_peer'} ) ) {
		return 1;
	}

	do {

		#Checking complete now read time range required
		#Set year/month variables with the start time
		my @c_time = gmtime($begin);
		$self->{'year'}   = scalar strftime( "%Y", @c_time );
		$self->{'month'}  = scalar strftime( "%m", @c_time );
		$self->{'day'}    = scalar strftime( "%d", @c_time );
		$self->{'hour'}   = scalar strftime( "%H", @c_time );
		$self->{'minute'} = scalar strftime( "%M", @c_time );

		my @end_time = gmtime($end);
		$self->{'end_year'}   = scalar strftime( "%Y", @end_time );
		$self->{'end_month'}  = scalar strftime( "%m", @end_time );
		$self->{'end_day'}    = scalar strftime( "%d", @end_time );
		$self->{'end_hour'}   = scalar strftime( "%H", @end_time );
		$self->{'end_minute'} = scalar strftime( "%M", @end_time );

		#This will be used to choose which files to download
		$self->{'begin_ts'} =
		$self->{'year'}
		. $self->{'month'}
		. $self->{'day'}
		. $self->{'hour'}
		. $self->{'minute'};
		$self->{'end_ts'} =
		$self->{'end_year'}
		. $self->{'end_month'}
		. $self->{'end_day'}
		. $self->{'end_hour'}
		. $self->{'end_minute'};

		$self->set_append();

		#Fetch the first update file (which will in turn get the first index file)
		if ( !defined( $self->get_all_files() ) ) {
			$self->{'error_code'} = $self->INIT_FAIL_ARCHIVE_CODE;
			$self->{'error_msg'}  = $self->INIT_FAIL_ARCHIVE_MSG;
			$self->deactivate();
			return 1;
		}

		#read next peer
		$self->{'current_peer'} = shift( @{ $self->{'peers'} } );
	} while ( defined( $self->{'current_peer'} ) );

	$self->{'file_obj'} = BGPmon::Fetch::File->new(
		file       => \@{ $self->{'list_of_files'} },
		begin_time => $self->{'begin_time'},
		end_time   => $self->{'end_time'}
	);

	if ( $self->{'file_obj'}->activate() ) {

		$self->{'error_code'} = $self->FILE_OPERATION_FAIL_CODE;
		$self->{'error_msg'}  = $self->FILE_OPERATION_FAIL_MSG;
		return undef;
	}

	$self->{'is_activated'} = 1;
	$self->{'error_code'}   = $self->NO_ERROR_CODE;
	$self->{'error_msg'}    = $self->NO_ERROR_MSG;
	return 0;
}

#set_append

#This function tests different variants of the final possible subdirectory
#under an archive page.
#Input:      None
#Output:     0

sub set_append {
	my $self = shift;

	my $url =
	$self->{'url'} . "/"
	. $self->{'current_peer'} . "/"
	. $self->{'year'} . "."
	. $self->{'month'} . "/";

	my $output = $self->{'scratch_dir'} . "/index-test.html";

	if ( $self->{'ribs'} == 0 ) {
		if (   $self->download_URL( $url . "UPDATES/", $output ) == 0
			&& $self->validateIndex($output) )
		{
			$self->{'append'} = "UPDATES/";
		}
		elsif ($self->download_URL( $url . "updates/", $output ) == 0
			&& $self->validateIndex($output) )
		{
			$self->{'append'} = "updates/";
		}

		#TODO: Evaluate efficacy of this elsif
		elsif ($self->download_URL( $url, $output ) == 0
			&& $self->validateIndex($output) )
		{
			$self->{'append'} = "";
		}
		else {
			$self->{'append'} = undef;
		}
		eval {
			unlink($output);
			1;
		} or do {
			$self->{'error_code'} = $self->SYSCALL_FAIL_ARCHIVE_CODE;
			$self->{'error_msg'}  = $self->SYSCALL_FAIL_ARCHIVE_MSG . ": $@";
		};
	}
	else {
		if (   $self->download_URL( $url . "RIBS/", $output ) == 0
			&& $self->validateIndex($output) )
		{
			$self->{'append'} = "RIBS/";
		}
		elsif ($self->download_URL( $url . "ribs/", $output ) == 0
			&& $self->validateIndex($output) )
		{
			$self->{'append'} = "ribs/";
		}
		elsif ($self->download_URL( $url, $output ) == 0
			&& $self->validateIndex($output) )
		{
			$self->{'append'} = "";
		}
		else {
			$self->{'append'} = undef;
		}
		eval {
			unlink($output);
			1;
		} or do {
			$self->{'error_code'} = $self->SYSCALL_FAIL_ARCHIVE_CODE;
			$self->{'error_msg'}  = $self->SYSCALL_FAIL_ARCHIVE_MSG . ": $@";
		};

		return 0;
	}
}

# get_filename_from_line

#This helper function extracts an update filename from a line of HTML.
#Input:      A single line of HTML code
#Output:     The filename extracted from the line (of the form
#                updates.YYYYMMDD.HHMM.*.xml.[compression type] )
#                or the empty string if none is found.

sub get_filename_from_line {
	my $self = shift;
	my $line = shift;
	if ( !defined($line) ) {
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return undef;
	}

	#Filenames look like this: updates.YYYYMMDD.HHMM.*.xml
	#Filenames may also have an up-to-4-character extension for compressed
	if ( $line =~
		m/(\"updates\.[0-9][0-9][0-9][0-9][0-1][0-9][0-3][0-9]\.[0-2][0-9][0-5][0-9].*\.xml(\.\w{0,4})?\")/
		|| $line =~
		m/(\"ribs\.[0-9][0-9][0-9][0-9][0-1][0-9][0-3][0-9]\.[0-2][0-9][0-5][0-9].*\.xml(\.\w{0,4})?\")/
	)
	{
		my $filename = $1;

		#Hack off the double-quotes on either side of the filename
		$filename =~ s/\"//g;
		$self->set_no_error();
		return $filename;
	}
	return "";
}

# validateIndex
#A helper function to scan an HTML page to determine whether the page is
#a valid index page or something else.  It looks for the key phrase
#"Index of" with the current year.month subdirectory.
#Input:      The filename to check
#Output:     1 if the file matches the search, 0 otherwise

sub validateIndex {
	my $self     = shift;
	my $index_fn = shift;

	if ( !defined($index_fn) ) {
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return 0;
	}
	my $index_fh;
	unless ( open( $index_fh, "<", $index_fn ) ) {
		$self->{'error_code'} = $self->FILE_OPERATION_FAIL_CODE;
		$self->{'error_msg'}  = $self->FILE_OPERATION_FAIL_MSG;
		return 0;
	}

	$self->set_no_error();
	while (<$index_fh>) {
		my $line = $_;
		if ( $line =~ m/Index of .*\/$self->{'year'}.$self->{'month'}\// ) {
			close($index_fh);
			return 1;
		}
	}
	close($index_fh);
	return 0;
}

# get_all_files
#
#Iterates through a list of files and downloads and opens the next one
#Input:      None
#Output:     0 on success, undef on failure
#            Also, the module-level filehandle upd_fh is open on success

sub get_all_files {
	my $self = shift;

	my $next_url = "";
	$self->{'need_next_index_file'} = -1;

	if ( $self->is_activated() && $self->{'file_obj'}->is_activated() ) {
		$self->{'error_code'} = $self->FILE_OPERATION_FAIL_CODE;
		$self->{'error_msg'}  = $self->FILE_OPERATION_FAIL_MSG;
		return undef;
	}

    #If there is no index page loaded for an otherwise connected archive,
    #then return an error
    if ( !(scalar(@{ $self->{'index_page'} })>0) && $self->is_activated() ) {
        $self->{'error_code'} = $self->NO_INDEX_PAGE_CODE;
        $self->{'error_msg'}  = $self->NO_INDEX_PAGE_MSG;
        return undef;
    }

	#Grab the URL of the next file to download
	#print("[BGPmon::Fetch::Archive] Downloading files\n");
	while (1) {
		$next_url = shift @{ $self->{'index_page'} };
		if ( !defined($next_url) || $next_url eq "" ) {

			#If the current index page is exhausted, increment the month/year
			#and download the next index page

			if ( $self->{'need_next_index_file'} == 1 ) {

				$self->advanceIndex();
				if ( $self->{'year'} > $self->{'end_year'} ) {
					last;
				}
				elsif ($self->{'year'} == $self->{'end_year'}
					&& $self->{'month'} > $self->{'end_month'} )
				{
					last;
				}

                $self->set_append(); 
		    $self->{'index_page'} = $self->get_next_index();
			}
			if ( $self->{'need_next_index_file'} == -1 ) {
				$self->{'need_next_index_file'} = 1;
 		    $self->{'index_page'} = $self->get_next_index();              
            }
			


            if ( !(scalar(@{ $self->{'index_page'} })>0)) {
                $self->{'error_code'} = $self->DOWNLOAD_FAIL_CODE;
                $self->{'error_msg'}  = $self->DOWNLOAD_FAIL_MSG;
                return undef;
            }
            $next_url = shift @{ $self->{'index_page'} };


		}

		#Extract the filename from the next URL to download
		my @url_split = split( "/", $next_url );
		my $upd_fn = $url_split[-1];
		my $ret =
		$self->download_URL( $next_url, "$self->{'scratch_dir'}/" . $upd_fn );
		if ( $ret == 1 ) {
			$self->{'error_code'} = $self->DOWNLOAD_FAIL_CODE;
			$self->{'error_msg'}  = $self->DOWNLOAD_FAIL_MSG . " $upd_fn";
			return undef;
		}

		#Populate list of files sequentially for all peers
		push(
			@{ $self->{'list_of_files'} },
			"$self->{'scratch_dir'}/" . $upd_fn
		);
		$self->{'files_read'} += 1;

	}

	$self->set_no_error();
	return 0;
}

#advanceIndex
#A helper function to advance the month
#and possibly year module state variables.
#Input:      None
#Output:     None (returns 0)

sub advanceIndex {

	my $self = shift;
	$self->{'month'} = sprintf( "%02u", ( $self->{'month'} + 1 ) % 13 );
	$self->{'month'} = "01" if $self->{'month'} == 0;
	$self->{'year'} = sprintf( "%04u", $self->{'year'} + 1 )
	if $self->{'month'} == 1;
	return 0;
}

# download_URL
#
#Downloads a target file and saves it to a user-specified file
#This function is primarily a wrapper around several functions of
# the LWP::Simple module.
#Input:        a target URL, either an HTML index or another file
#              an output file name
#Output:       0 on success, 1 on failure

sub download_URL {
	my $self = shift;

	#Get argument(s) and check that they exist
	my $target_url = shift;
	my $output     = shift;

	#If the target is not defined, obviously that is a problem
	if (   !defined($target_url)
		|| $target_url eq ""
		|| !defined($output)
		|| $output eq "" )
	{
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return 1;
	}

	if ( $target_url !~ m/http/ ) {
		$target_url = "http://" . $target_url;
	}

	## Download the specified file into the given output file
	my $ret = LWP::Simple::getstore( $target_url, $output );

	#If grabbing the url fails, log the reason why
	if ( LWP::Simple::is_error($ret) ) {
		$self->{'error_code'} = $self->DOWNLOAD_FAIL_CODE;
		$self->{'error_msg'}  = $self->DOWNLOAD_FAIL_MSG . $ret . " ";
		return 1;
	}
	$self->set_no_error();
	return 0;
}

#get_next_index
#Fetches an index HTML page and returns the contents as an array
#Input:         None
#Output: An array with the lines of the HTML, or undef on failure
sub get_next_index {

    my $index_url  = "";
    my @html_index = ();
	my @empty_array=();
    my $index_fh   = undef;
    my $index_fn   = $self->{'scratch_dir'} . "/index.html";

    if (   !defined( $self->{'url'} )
        || !defined( $self->{'current_peer'} )
        || !defined( $self->{'month'} )
        || !defined( $self->{'year'} )
        || !defined( $self->{'append'} ) )
    {
        $self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
        $self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
        return \@empty_array;
    }

	if (   !defined( $self->{'url'} )
		|| !defined( $self->{'current_peer'} )
		|| !defined( $self->{'month'} )
		|| !defined( $self->{'year'} )
		|| !defined( $self->{'append'} ) )
	{
		$self->{'error_code'} = $self->UNDEFINED_ARCHIVE_ARGUMENT_CODE;
		$self->{'error_msg'}  = $self->UNDEFINED_ARCHIVE_ARGUMENT_MSG;
		return undef;
	}

	#Construct the current index URL from the current month and year
	$index_url =
	$self->{'url'} . "/"
	. $self->{'current_peer'} . "/"
	. $self->{'year'} . "."
	. $self->{'month'} . "/"
	. $self->{'append'};

    
    my $ret = $self->download_URL( $index_url, $index_fn );
    if ( !defined($ret) ) {
        
        $self->{'error_code'} = $self->DOWNLOAD_FAIL_CODE;
        $self->{'error_msg'}  = $self->DOWNLOAD_FAIL_MSG;
        return \@empty_array;
    }

    #Open the saved index HTML file and load it into an array
    unless ( open( $index_fh, "<", "$index_fn" ) ) {
        $self->{'error_code'} = $self->FILE_OPERATION_FAIL_CODE;
        $self->{'error_msg'}  = $self->FILE_OPERATION_FAIL_MSG . ": $@";
        return \@empty_array;
    }

	my $ret = $self->download_URL( $index_url, $index_fn );
	if ( !defined($ret) ) {

		$self->{'error_code'} = $self->DOWNLOAD_FAIL_CODE;
		$self->{'error_msg'}  = $self->DOWNLOAD_FAIL_MSG;
		return undef;
	}

	#Open the saved index HTML file and load it into an array
	unless ( open( $index_fh, "<", "$index_fn" ) ) {
		$self->{'error_code'} = $self->FILE_OPERATION_FAIL_CODE;
		$self->{'error_msg'}  = $self->FILE_OPERATION_FAIL_MSG . ": $@";
		return undef;
	}

	while (<$index_fh>) {
		my $line = $_;
		chomp($line);

		my $file_url = $index_url . $self->get_filename_from_line($line);

    }
    
    if (!(scalar(@html_index)>0)) {
        $self->{'error_code'} = $self->NO_INDEX_PAGE_CODE;
        $self->{'error_msg'}  = $self->NO_INDEX_PAGE_MSG;
        return undef;
    }

			#Now check file timestamp
			my @values = split( '\.', $file_url );
			my $num_fields = @values;
			my $file_ts =
			$values[ $num_fields - 7 ]
			. $values[ $num_fields - 6 ]
			;    #Terrible hack to extract timestamp from filename
			if ( $file_ts <= $self->{'end_ts'} )
			{ 
				push( @html_index, $file_url );
			}
		}
	}
	shift(@html_index);    #Removed undef entry

	my $to_be_removed_counter = 0;
	for ( my $i = 0 ; $i < @html_index - 1 ; $i++ ) {

		my $line_url  = $html_index[ $i + 1 ];
		my @vals      = split( '\.', $line_url );
		my $num_fs    = @vals;
		my $f_ts_next = $vals[ $num_fs - 7 ] . $vals[ $num_fs - 6 ];

		if ( $f_ts_next <= $self->{'begin_ts'} ) {
			shift(@html_index);
			$i = 0;        #Reset
			my $si = scalar(@html_index);
			$to_be_removed_counter += 1;
		}
		else {
			last;          #Done with removing unwanted files
		}

	}
	if ( !@html_index ) {
		$self->{'error_code'} = $self->NO_INDEX_PAGE_CODE;
		$self->{'error_msg'}  = $self->NO_INDEX_PAGE_MSG;
		return undef;
	}

	close($index_fh);
	eval {
		unlink($index_fn);
		1;
	} or do {
		$self->{'error_code'} = $self->SYSCALL_FAIL_ARCHIVE_CODE;
		$self->{'error_msg'}  = $self->SYSCALL_FAIL_ARCHIVE_MSG;
	};

	$self->set_no_error();

	return \@html_index;
}

sub set_no_error {
	my $self = shift;
	$self->{'error_code'} = $self->NO_ERROR_CODE;
	$self->{'error_msg'}  = $self->NO_ERROR_MSG;
}

=head2 read_xml_message

Reads the next XML message from the data source that is in the interval.

Input:  None, but assumes activate() has been called

Output: The next XML message from the archive or undef

Usage:  $fetch->read_xml_message();

=cut
sub read_xml_message {
	my $self = shift;

	if ( !$self->{'file_obj'}->is_activated() ) {
		$self->{'error_code'} = $self->UNCONNECTED_ARCHIVE_CODE;
		$self->{'error_msg'}  = $self->UNCONNECTED_ARCHIVE_MSG;
		return undef;
	}

	#Try to get the next message
	my $msg = $self->{'file_obj'}->read_xml_message();

	if ( defined($msg) ) {

#For every valid message make sure Archive object knows the number of messages read by File object
		$self->{'msgs_read'} += 1;
	}
	$self->set_no_error();
	return $msg;
}

=head2 deactivate

Function to close and delete any files 

Input: None

Output: 1 on error
		0 on success

Usage:  $fetch->deactivate();

=cut
sub deactivate {
	my $self = shift;
	if ( !$self->{'file_obj'}->is_activated() ) {
		$self->{'error_code'} = $self->UNCONNECTED_ARCHIVE_CODE;
		$self->{'error_msg'}  = $self->UNCONNECTED_ARCHIVE_MSG;
		return 1;
	}
	eval {
		my $errs;
		rmtree( $self->{'scratch_dir'},
			{ keep_root => 1, safe => 1, error => \$errs } );
		1;
	} or do {
		$self->{'error_code'} = $self->SYSCALL_FAIL_ARCHIVE_CODE;
		$self->{'error_msg'}  = $self->SYSCALL_FAIL_ARCHIVE_MSG . ": $@";
	};
	(
		$self->{'url'},        $self->{'year'}, $self->{'month'},
		$self->{'begin_time'}, $self->{'end_time'}
	) = undef;
	$self->{'index_page'} = [];
	$self->{'file_obj'}->{'connected'} = 0;
	return 0;
}

=head1 AUTHOR(S)

	Anant Shah, C<< <akshah at rams.colostate.edu> >>
	M. Lawrence Weikum, C<< <mweikum at rams.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to C<bgpmon at netsec.colostate.edu>
, or through the web interface at L<http://bgpmon.netsec.colostate.edu>.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

	perldoc BGPmon::Fetch::Archive

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2014 Colorado State University

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom
	the Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.\

	File: Archive.pm
	Authors: Anant Shah, M. Lawrence Weikum 

=cut

1;
