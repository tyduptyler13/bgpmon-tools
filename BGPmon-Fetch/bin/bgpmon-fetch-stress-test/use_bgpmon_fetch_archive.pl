#!/usr/bin/perl
#
#This script is written to stress test BGPmon::Fetch::Archive module
#Goal: Use time range spanning more than a month for one peer for mulitple peers
my $start_run = time();

use BGPmon::Fetch::Archive;
my @peerlist = ('129.82.2.2','164.128.32.11');

use constant NETSEC_VALID_ARCHIVE_ADDR => 'archive.netsec.colostate.edu/peers';

my $fetch = BGPmon::Fetch::Archive->new(
	url   => NETSEC_VALID_ARCHIVE_ADDR,
	peer  => \@peerlist,
	begin => "1393632051",
	end   => "1398880808",
	scratch_dir => "/tmp/"
);

if($fetch->get_error_code()!=0){
	print "ERROR: Object Not Initialized\n";
}

my $ret = $fetch->activate();
if($fetch->get_error_code()!=0){
	print "ERROR: Activate Failed\n";
}

my $msg=$fetch->read_xml_message();

if(!defined($msg)){
	print "ERROR: First Read Failed\n";
}
while(defined($msg)){
	$msg = $fetch->read_xml_message();
}
$fetch->deactivate();
my $end_run = time();
my $run_time = $end_run - $start_run;
print "Stress test took $run_time seconds\n";
