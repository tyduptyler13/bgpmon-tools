use 5.14.0;
use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
#

my @exec_files = qw(bin/routeviews2xml.pl);

WriteMakefile(
    NAME              => 'routeviews2xml',
    VERSION_FROM      => 'bin/routeviews2xml.pl',
    PREREQ_PM         => {'BGPmon::Message' => 0,
			  'LWP::Simple' => 0,
			  'File::Path' => 0,
			  'Getopt::Long' => 0,
			  'File::Slurp' => 0,
			  'File::Basename' => 0,
			  'File::Copy' => 0,
			  'DateTime' => 0,
			  'IO::Uncompress::Bunzip2' => 0,
			 }, # e.g., Module::Name => 1.1
    EXE_FILES		=> \@exec_files,
    ($] >= 5.005 ?     ## Add these new keywords supported since 5.005
      (#ABSTRACT_FROM  => 'bin/bgpmon-archiver.pl', # retrieve abstract from module
       AUTHOR         => 'Kaustubh Gadkari <kaustubh@cs.colostate.edu>') : ()),
);
