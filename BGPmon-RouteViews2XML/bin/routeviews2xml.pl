#!/usr/bin/perl
# *
# *
# *      Copyright (c) 2012 Colorado State University
# *
# *      Permission is hereby granted, free of charge, to any person
# *      obtaining a copy of this software and associated documentation
# *      files (the "Software"), to deal in the Software without
# *      restriction, including without limitation the rights to use,
# *      copy, modify, merge, publish, distribute, sublicense, and/or
# *      sell copies of the Software, and to permit persons to whom
# *      the Software is furnished to do so, subject to the following
# *      conditions:
# *
# *      The above copyright notice and this permission notice shall be
# *      included in all copies or substantial portions of the Software.
# *
# *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# *      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# *      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# *      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# *      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# *      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# *      FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# *      OTHER DEALINGS IN THE SOFTWARE.
# *
# *
# *  File: routeviews2xml.pl
# *  Authors: Kaustubh Gadkari
# *  Date: April 2014
# *

our $VERSION = '1.0';
use v5.12.0;

use strict;
use warnings;
use LWP::Simple;
use IO::Uncompress::Bunzip2 qw(bunzip2 $Bunzip2Error);
use IO::Compress::Bzip2 qw(bzip2 $Bzip2Error);
use Getopt::Long;
use File::Slurp qw/read_file/;
use File::Basename;
use File::Copy qw/move/;
use File::Path qw/make_path/;
use DateTime;
use POSIX qw/strftime/;
use Parallel::ForkManager;

use constant RV_RIB_URL =>
    "http://archive.routeviews.org/COLLECTOR/bgpdata/YEAR.MONTH/RIBS";
use constant RV_UPDATE_URL =>
    "http://archive.routeviews.org/COLLECTOR/bgpdata/YEAR.MONTH/UPDATES";
use constant XML_SAFI => -1;
use constant FALSE => 0;
use constant TRUE => 1;
# Global config variables.
my $trans_log_loc = "/raid1/routeviews_archives/logs";
my $out_dir = "/tmp/routeviews2xml/date";
my $translator = "/usr/local/bin/bgpmon_mrt2xml";
my $collector = "route-views2";
my $year = "2014";
my $month = "08";
my $r_url = RV_RIB_URL;
my $u_url = RV_UPDATE_URL;
my $verbose;
my $curr_dir_path = "";
my $ribs = 0;
my $ipv = 4;
my $collector_ip = "";
my $debug = 0;
my $logfile = "/raid1/routeviews_archives/routeviews2xml.log";

make_dir($trans_log_loc);

# List of all routeviews collectors.
my @collectors = qw(route-views2 route-views3 route-views4 route-views6 route-views.eqix
                    route-views.isc route-views.kixp route-views.jinx
                    route-views.linx route-views.nwax route-views.telxatl
                    route-views.wide route-views.sydney route-views.saopaulo);

my %collectors_ip4 = (
    'route-views2' => '128.223.51.102',
    'route-views3' => '128.223.51.108',
    'route-views4' => '128.223.51.15',
    'route-views6' => '128.223.51.112',
    'route-views.eqix' => '206.223.132.88',
    'route-views.isc' => '149.20.36.133',
    'route-views.kixp' => '196.6.220.26',
    'route-views.jinx' => '196.23.136.252',
    'route-views.linx' => '195.66.241.146',
    'route-views.nwax' => '207.162.219.54',
    'route-views.telxatl' => '67.23.60.46',
    'route-views.wide' => '203.178.141.138',
    'route-views.sydney' => '114.31.193.202',
    'route-views.saopaulo' => '200.160.6.217',
);

my %collectors_ip6 = (
    'route-views3' => '2001:468:d01:33::80df:336c',
    'route-views4' => '2001:468:d01:33::80df:330f',
    'route-views6' => '2001:468:d01:33::80df:3370',
    'route-views.saopaulo' => '2001:12ff:0:6192::217',
);

#---- MAIN ----

parse_command_line();

unless(-e $translator and -x $translator) {
    print STDERR "No suitable translator found.\n";
    usage();
}

if ($ribs != 0 and $ribs != 1) {
    print STDERR "Command line option ribs can have value 0 or 1.\n";
    exit(0);
}

if ($ribs == 1) {
    $logfile = "/raid1/routeviews_archives/routeviews2xml_ribs.log";
    if ($ipv != '4' and $ipv != '6') {
        print STDERR "IP version can only be 4 or 6.\n";
        exit(0);
    }
}

if ($ipv == 4) {
    if (exists($collectors_ip4{$collector})) {
        $collector_ip = $collectors_ip4{$collector};
    } else {
        print STDERR "Collector $collector does not have an IPv4 address.\n";
        exit(0);
    }
} else {
    if (exists($collectors_ip6{$collector})) {
        $collector_ip = $collectors_ip6{$collector};
    } else {
        print STDERR "Collector $collector does not have an IPv6 address.\n";
        exit(0);
    }
}

# Open the log file.
my $log_fh;
open ($log_fh, ">>$logfile") or die "Could not open log file: $!";

# The URL for route-views2 does not have the collector name in the URL.
# For every other collector, set the right name in the URL.
if ($collector eq 'route-views2') {
    $r_url =~ s/COLLECTOR//;
    $u_url =~ s/COLLECTOR//;
} else {
    $r_url =~ s/COLLECTOR/$collector/g;
    $u_url =~ s/COLLECTOR/$collector/g;
}

$month = sprintf("%02d", $month);
$r_url =~ s/YEAR/$year/;
$r_url =~ s/MONTH/$month/;
$u_url =~ s/YEAR/$year/;
$u_url =~ s/MONTH/$month/;

# Set download url.
my $url = $ribs == 1 ? $r_url : $u_url;

# Download the index file for the updates.
my $index_file = $ribs == 1 ? "/tmp/index-ribs.html" : "/tmp/index-upds.html";
if (download_file($url, $index_file) == FALSE) {
    print $log_fh "Error downloading index file from $url.\n";
    exit(0);
}
my @index_lines = read_file($index_file);

my @all_files;

# Process each update file and run it through the translator.
foreach my $line (reverse @index_lines) {
    if ($line =~ /a href="(.*?bz2)"/) {
        push(@all_files, $1);
    }
}

# Start parallel fork manager
my $manager = new Parallel::ForkManager(10);
foreach my $file (@all_files) {
        $manager->start() and next;
        if (download_file("$url/$file", "/tmp/$file") == FALSE) {
            print $log_fh "Error downloading file $url/$file.\n";
            next;
        }
        process_file($collector, "/tmp/$file", $ribs);
        # Remove input file.
        unlink("/tmp/$file") if (-e "/tmp/$file");
        $manager->finish();
}
unlink($index_file);
close($log_fh);

#---- END MAIN ----

#---- SUBROUTINES ----

# Print a usage message.
# Input: None
# Output: Usage message
# Kaustubh Gadkari Apr 2014
sub usage {
    print "This script converts a routeviews archive for a given YYYY.MM.\n";
    print "Usage: $0 [-year YYYY] [-month MM]
        [-out_dir /path/to/date/files]
        [-translator /path/to/translator]
        [-ribs] [-ipv 4|6 default = 4]
        [-help] [-v] [-debug]\n";
    exit(0);
}

# Parse the command line and set the appropriate options.
# Input: None
# Output: None
# Kaustubh Gadkari Apr 2014
sub parse_command_line {
    my $result = GetOptions("collector=s" => \$collector,
                            "ribs=i" => \$ribs,
                            "year=s" => \$year,
                            "month=s" => \$month,
                            "help" => \&usage,
                            "out_dir=s" => \$out_dir,
                            "translator=s" => \$translator,
                            "ipv=i" => \$ipv,
                            "debug=i" => \$debug,
                            "v" => \$verbose);
    unless ($result) {
        usage();
    }

    unless (grep($collector, @collectors)) {
        print STDERR "Unknown collector $collector\n";
        usage();
    }
}

# Process a given MRT file.
# Input: MRT file, switch to indicate whether it is a rib file
# Output: -1 on failure, 0 on success
# Kaustubh Gadkari Apr 2014
sub process_file {
    my ($collector, $file, $is_rib) = @_;

    # Unzip the input file.
    my ($name, $path, $suffix) = fileparse($file, ".bz2");
    $path =~ s/\/$//;
    print "Unzipping $file to $path/$name\n" if $verbose;
    bunzip2($file, "$path/$name") or die "Error in bunzip2: $Bunzip2Error";

    # Extract the file creation time from the file name.
    # This will be used as is in the xml file creation.
    my @file_name_els = split(/\./, $name);
    my $creation_time = join(".", @file_name_els[1..2]);

    # Get the full path of output file
    my $out_file_path = get_file_name($creation_time, $is_rib, $collector);
    my $out_fh = new IO::Compress::Bzip2($out_file_path, AutoClose => 1) or
        die "Could not open outfile for writing: $Bzip2Error\n";
    my $bytes_out = $out_fh->write("<xml>\n");
    unless (defined($bytes_out)) {
        print $log_fh "Could not write <xml> tag to file $out_file_path.\n";
        return;
    }
    # Run the input file through the translator.
    my $exec_string = "$translator -f $path/$name -i $collector_ip -v $ipv -l $trans_log_loc/$name |";
    if ($is_rib == 0) {
        $exec_string = "$translator -f $path/$name -t -l $trans_log_loc/$name |";
    }
    open(my $fh, $exec_string) or die "Could not execute translator: $!";
    while (my $line = <$fh>) {
        $bytes_out = $out_fh->write($line);
        unless (defined($bytes_out)) {
            print $log_fh "Could not write line to file $out_file_path.\n";
        }
    }
    close($fh);
    $bytes_out = $out_fh->write("</xml>\n");
    unless (defined($bytes_out)) {
        print $log_fh "Could not </xml> tag to file $out_file_path.\n";
    }
    $out_fh->close();
    unlink("$path/$name") if (-e "$path/$name");
}

#---- Generate a new file name. Create appropriate paths if necessary ----
sub get_file_name {
    my ($time_stamp, $is_rib, $collector) = @_;

    # Set the base directory name to YYYY.MM
    my @ts_els = split(/\./, $time_stamp);
    $ts_els[0] =~ /(\d\d\d\d)(\d\d)(\d\d)/;
    my ($yyyy, $mm, $dd) = ($1, $2, $3);
    $ts_els[1] =~ /(\d\d)(\d\d)/;
    my ($hr, $min) = ($1, $2);
    my $dir_name = "$collector/$yyyy.$mm/$dd";

    # Make the directory.
    my $type = $is_rib == 1 ? "RIBS" : "UPDATES";
    my $type_lc = lc($type);
    my $dir_path = join("/", $out_dir, $dir_name);
    $dir_path = join("/", $dir_path, $type);

    make_dir($dir_path);

    # Set the current working directory to the newly created directory.
    $curr_dir_path = $dir_path;

    # Generate new XML file name.
    my $ts = $time_stamp;
    my $file_name = "$type_lc.$ts.xml.bz2";
    # Return the new file name.
    return "$curr_dir_path/$file_name";
}

#---- Given a directory path, create a new directory. ----
sub make_dir {
    my $dir_path = shift;

# Create output directory if required.
    unless (-d $dir_path) {
        make_path($dir_path, {error => \my $err, mode => 0755, });
        if (@$err) {
            for my $diag (@$err) {
                my ($file, $message) = %$diag;
                if ($file eq '') {
                    die "Error creating directory path $dir_path: $message\n";
                } else {
                    die
                        "Error creating output directory $dir_path: $message\n";
                }
            }
            return -1;
        }
    }
    return 0;
}

sub download_file {
    my ($url, $loc) = @_;
    my $retval = LWP::Simple::getstore($url, $loc);
    if (LWP::Simple::is_error($retval)) {
        return FALSE;
    }
    return TRUE;
}
