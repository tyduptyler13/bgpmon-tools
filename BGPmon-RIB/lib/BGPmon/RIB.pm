package BGPmon::RIB;
use 5.14.0;
use strict;
use warnings;
use constant FALSE => 0;
use constant TRUE => 1;
use BGPmon::Message;
use BGPmon::RIB::Entry;
require Exporter;

BEGIN{
  our $VERSION = '0.01';
  our @ISA = qw /Exporter/;
  our @EXPORT_OK = qw/ new get_peer_address get_monitor_address
    ingest_message get_all_prefixes flag_finished is_finished
    get_error_msg get_error_code /;
  our @EXPORT = qw();
}



## Variables to hold error codes and messages
# No errors
use constant NO_ERROR_CODE => 0;
use constant NO_ERROR_MSG => 'No Error. Relax with some tea.';

#Error codes for making a new RIB
use constant TOO_FEW_ARGUMENTS_CODE => 1005;
use constant TOO_FEW_ARGUMENTS_MSG => 'Too few arguments were given to new().';

#Error codes for adding a prefix
#TODO

#Error codes for getting a list of all prefixes
use constant EMPTY_RIB_CODE => 1000;
use constant EMPTY_RIB_MSG => "The RIB is currently empty.";

#Error codes for getting info about a prefix
use constant PREFIX_NOT_FOUND_CODE => 1010;
use constant PREFIX_NOT_FOUND_MSG => 'Prefix was not found in the RIB.';



=head1 NAME

BGPmon::RIB.pm

The BGPmon RIB module, blah blah blah TODO

=head1 SYNOPSIS

The BGPmon::RIB module provides blah blah blah TODO functionality to connect to one of the

    use BGPmon::Fetch;
    my $ret = init_bgpdata();
    my $ret = connect_bgpdata();
    my $xml_msg = read_xml_message();
    if ( !defined($xml_msg) ){
        print get_error_code() . ": " . get_error_msg();
    }
    my $ret = is_connected();
    my $num_read = messages_read();
    my $uptime = uptime();
    my $ret = close_connection();
    my $downtime = connection_endtime();

=head1 EXPORT


TODO

=cut

=head1 SUBROUTINES/METHODS


=head2 new

TODO Initializes a new RIB given the peer's IP.

Input: Peer's IP Address, BGPmonitor's IP Address

Output: Undef on error
        RIB class on success
=cut
sub new{
  my $class = shift;
  my $xml_message = shift;

  if((!defined($xml_message))){
    #TODO - find a way to relay this back
    #TOO_FEW_ARGUMENTS_CODE;
    #TOO_FEW_ARGUMENTS_MSG;
    return undef;
  }

  #Makng sure we have a table start message
  if($xml_message !~ /TABLE_START/){
    #TODO make error code and return failure
    return undef;
  }

  #Starting the parsing
  my $msg = BGPmon::Message->new(string=>$xml_message);
  if(!defined($msg)){
  #TODO make error code and return failure
    return undef;
  }

  #Getting the sender and monitor information
  my $sender_addr = $msg->get_source_address();
  #my $monitor_addr = $msg->get_node('/BGP_MONITOR_MESSAGE/MONITOR/ADDRESS/_');
  my $monitor_addr = $msg->get_monitor_address();

  ##Grabbing the header information to store
  my $source =  $msg->get_node('/BGP_MONITOR_MESSAGE/SOURCE/');
  my $dest = $msg->get_node('/BGP_MONITOR_MESSAGE/DEST/');
  my $monitor =  $msg->get_node('/BGP_MONITOR_MESSAGE/MONITOR/');
  my $ob_time =  $msg->get_node('/BGP_MONITOR_MESSAGE/OBSERVED_TIME/');

  #Getting the timestamp and updating the object's most recent time
  #my $timestamp = $msg->get_node(
  #                  '/BGP_MONITOR_MESSAGE/OBSERVED_TIME/TIMESTAMP/_');
  my $timestamp = $msg->get_timestamp();
  #Making the timestamp an integer
  $timestamp += 0.0;
  #Making sure we have what we need
  if(!defined($monitor_addr) or !defined($sender_addr) or
      !defined($monitor) or !defined($ob_time) or !defined($timestamp)){
    #TODO AMKE ERROR CODE AND RETURN FAILURE
    return undef;
  }

  #Initializing the RIB
  my $self = {
      ##private variables
      #Peer address to keep track of what rib this is
      _peer_address => $sender_addr,
      #BGPmonitor that this is coming from
      _bgp_monitor => $monitor_addr,
      #Holds the pointers to xml nodes making up the header
      _source => $source,
      _dest  => $dest,
      _monitor => $monitor,
      _ob_time => $ob_time,
      #Hash for rib, prefix -> some data about it.
      _rib_entries => {}, # ribref->rib_entry
      _prefixes => {}, # prefix =>{timestamp,ribentry} =>{time, rib entry}
      _as_path => {}, #as path -> [rib entries]
      _latest_timestamp => $timestamp, #to be updated with each ingest
      _locker => 1, #used for locking

      ##public variables
      finished => FALSE,
      error_code => NO_ERROR_CODE,
      error_msg => NO_ERROR_MSG,
  };
  bless $self, $class;
  return $self;
}



sub flag_finished{
  my $self = shift;
  $self->{'finished'} = TRUE;
  return 0;
}

sub is_finished{
  my $self = shift;
  return $self->{'finished'};
}

sub print_table{
  my $self = shift;
  my $fh = shift;
  my %opts = @_;
  my $format = $opts{'format'};
  if(!defined($format)){
    $format = 'xml';
  }

  my $pretty_print = $opts{'pretty_print'};
  if (!defined($pretty_print)) {
      $pretty_print = 0;
  }

  use Data::Dumper;
  # create the table start message
  my $ts_msg = BGPmon::Message->new();
  my $number = 0;
  $ts_msg->add_node('SOURCE',$self->get_source_info());
  $ts_msg->add_node('DEST',$self->get_dest_info());
  $ts_msg->add_node('MONITOR',$self->get_monitor_info());
  $ts_msg->add_node('OBSERVED_TIME',$self->get_ob_time_info());
  $ts_msg->add_node('SEQUENCE_NUMBER',$number);
  $ts_msg->add_node('STATUS',{Author_Xpath=>"/BGP_MONITOR_MESSAGE/MONITOR",
                             TYPE=>"TABLE_START"});
  $number++;
  print $fh $ts_msg->to_string(format=>$format, pretty_print => $pretty_print);


  foreach my $ref (keys %{$self->{'_as_path'}}){
    my $tempRibEntries = $self->{'_as_path'}->{$ref};
    my @entries = @$tempRibEntries;
    foreach my $e(@entries){
      my $msg = BGPmon::Message->new();
      $msg->add_node('SOURCE',$self->get_source_info());
      $msg->add_node('DEST',$self->get_dest_info());
      $msg->add_node('MONITOR',$self->get_monitor_info());
      $msg->add_node('OBSERVED_TIME',$self->get_ob_time_info());
      $msg->add_node('SEQUENCE_NUMBER',$number);
      $number++;
      $msg->add_node('UPDATE',$e->{'xml_nodes'});
      foreach my $nlri(keys %{$e->{'nlri'}}){
        $msg->add_nlri_node($e->{'nlri'}->{$nlri});
      }
      foreach my $mp_nlri(keys %{$e->{'mp_nlri'}}){
        $msg->add_mp_nlri_nodes($e->{'mp_nlri'}->{$mp_nlri});
      }
      ## delete the encoding line -- unless text
      my @message =  split "\n",$msg->to_string(format=>$format);
      my $message_str = join "\n",@message;
      if($format eq 'xml'){
        shift @message;
        $message_str = join "\n",@message;
      }
      ## and print
      print $fh $message_str;
      print $fh "\n";
      # once the object has been printed we have to put it back
      # to the way it was -- the only part we have messed with
      # is within the update
      # so - remove the nlris and the mp_nlis
      $msg->remove_node('/BGP_MONITOR_MESSAGE/UPDATE/NLRI');
      $msg->get_mp_nlri_nodes(remove=>1);
    }
  }
  # create the table start message
  my $te_msg = BGPmon::Message->new();
  $number = 0;
  $te_msg->add_node('SOURCE',$self->get_source_info());
  $te_msg->add_node('DEST',$self->get_dest_info());
  $te_msg->add_node('MONITOR',$self->get_monitor_info());
  $te_msg->add_node('OBSERVED_TIME',$self->get_ob_time_info());
  $te_msg->add_node('SEQUENCE_NUMBER',$number);
  $te_msg->add_node('STATUS',{Author_Xpath=>"/BGP_MONITOR_MESSAGE/MONITOR",
                             TYPE=>"TABLE_STOP"});
  $number++;
  ## delete the encoding line
  my @te_message =  split "\n",$te_msg->to_string
    (format=>$format, pretty_print=>$pretty_print);
  shift @te_message;
  my $te_message_str = join "\n",@te_message;
  ## and print
  print $fh $te_message_str;
  print $fh "\n";
  return 0;
}


=head2 get_peer_address

Returns the IP address of the peer asssociated with this RIB

Input: None

Output: IPv4/6 Adddress
=cut
sub get_peer_address{
  my $self = shift;
  $self->{'error_code'} = NO_ERROR_CODE;
  $self->{'error_msg'} = NO_ERROR_MSG;
  return $self->{'_peer_address'};
}

=head2 get_monitor_address

Returns the IP address of the BGPmon asssociated with this RIB

Input: None

Output: IPv4/6 address
=cut
sub get_monitor_address{
  my $self = shift;
  $self->{'error_code'} = NO_ERROR_CODE;
  $self->{'error_msg'} = NO_ERROR_MSG;
  return $self->{'_bgp_monitor'};
}

sub get_source_info{
  my $self = shift;
  return $self->{'_source'};
}

sub get_dest_info{
  my $self = shift;
  return $self->{'_dest'};

}
sub get_monitor_info{
  my $self = shift;
  return $self->{'_monitor'};

}
sub get_ob_time_info{
  my $self = shift;
  return $self->{'_ob_time'};
}

sub get_latest_timestamp{
  my $self = shift;
  return $self->{'_latest_timestamp'};

}




sub getWithdraws{
  my $toReturn = [];
  #TODO TEST BOTH PARSING

  my $withs =  BGPmon::Translator::XFB2PerlHash::get_content(
    '/BGP_MONITOR_MESSAGE/bgp:UPDATE/bgp:WITHDRAW/');
  if(ref($withs) eq "ARRAY"){
    foreach my $res (@$withs){
      push(@$toReturn, $res->{'_'});
    }
  }
  elsif(ref($withs) eq "HASH"){
    push(@$toReturn,$withs->{'_'});
  }

  my $mpwiths =  BGPmon::Translator::XFB2PerlHash::get_content(
  '/BGP_MONITOR_MESSAGE/bgp:UPDATE/bgp:MP_UNREACH_NLRI/bgp:MP_NLRI/');
  if(ref($mpwiths) eq "ARRAY"){
    foreach my $res (@$mpwiths){
      push(@$toReturn, $mpwiths->{'_'});
    }
  }
  elsif(ref($mpwiths) eq "HASH"){
    push(@$toReturn,$mpwiths->{'_'});
  }


  return $toReturn;
}

sub should_handle{
  my $self = shift;
  my $prefix = shift;
  my $timestamp = shift;

  if(exists($self->{'_prefixes'}->{$prefix}) and
     exists($self->{'_prefixes'}->{$prefix}->{'timestamp'})){
    if($self->{'_prefixes'}->{$prefix}->{'timestamp'} > $timestamp){
      return FALSE;
    }
  }
  return TRUE;
}



sub find_similar_ribentry{
  my $self = shift;
  my $prefix = shift;
  my $update  = shift;
  my $aspath = shift;

  if(exists($self->{'_as_path'}->{$aspath})){
    my $tempRibEntries = $self->{'_as_path'}->{$aspath};
    foreach my $re (@$tempRibEntries){
      if($re->is_similar($update)){
        return $re;
      }
    }
  }
  return undef;
}


=head2 ingest_message

TODO this code is to add prefix

Input:

Output:
=cut
sub ingest_message{
  my $self = shift;
  my $xml_msg = shift;

  if(!defined($xml_msg)){
    #TODO MAKE ERROR CODE AND RETURN FAILURE
    return 1;
  }

  my $msg = BGPmon::Message->new(string=>$xml_msg);
  return 1 if !defined($msg);

  ## TODO: the timestamp needs to be checked for each prefix
  # not for the table
  #Getting the timestamp and updating the object's most recent time
  my $timestamp=$msg->get_timestamp();
  if(!defined($timestamp)){
    #TODO AMKE ERROR CODE AND RETURN FAILURE
    return 1;
  }
  #make timestamp an integer
  $timestamp += 0.0;
  ## Updating timestamp
  if($timestamp > $self->{'_latest_timestamp'}){
    #updating timestamp to the current time
    $self->{'_latest_timestamp'} = $timestamp;
  }


  ## get all of the prefixes that we need to make changes to
  # these are going to be the nodes that can be used again
  # later when creating the messages
  my $nlri = $msg->get_nlri_nodes(remove=>1);
  my $mp_nlri = $msg->get_mp_nlri_nodes(remove=>1);
  my $with = $msg->remove_node('/BGP_MONITOR_MESSAGE/UPDATE/WITHDRAW');
  my $mp_with = $msg->remove_node(
              '/BGP_MONITOR_MESSAGE/UPDATE/MP_UNREACH_NLRI/MP_NLRI');
  my $update = $msg->get_node('/BGP_MONITOR_MESSAGE/UPDATE');
  my $aspath = $msg->get_aspath();

  ## Handle withdrawls
  foreach my $p (@$with){
    #making sure this is at a new time that we need to cover
    if($self->should_handle($p, $timestamp)){
      $self->remove($p);
    }
  }

  ##Continuing parsing - handling announcements
  foreach(@$nlri){
    my $prefix = $_->{'_'};
    $self->handleNLRI($_,$prefix,$timestamp,$update,$aspath,mp=>0);
  }
  foreach(@$mp_nlri){
    my $prefix = $_->{'_'};
    $self->handleNLRI($_,$prefix,$timestamp,$update,$aspath,mp=>1);
  }

  return 0;
}

#Strategy:
#1)See if we need to change the rib entry
#2) If we do, cleanup from current rib entry (call remove)
#3)Search for matching rib entry.  If found, add to group, update hash
#4) if matching entry doesn't exist, make a new one, as to aspath{rib}
sub handleNLRI{
  my $self = shift;
  my $nlri = shift;
  my $prefix = shift;
  my $timestamp = shift;
  my $update = shift;
  my $as_path = shift;
  my %opts = @_;

  if($self->should_handle($prefix, $timestamp)){
    #stage 0
    #remove prefix from the current rib entry and clean up rib entries
    $self->remove($prefix);

    #stage 1
    #Making sure the prefix is in the rib
    if(!exists($self->{'_prefixes'}->{$prefix})){
      $self->{'_prefixes'}->{$prefix} = {
                                        'timestamp' => 0,
                                        'ribentry' => undef,
                                        'as_path' => undef,
                                      };
    }

    #stage 2
    #Updating timestamp
    $self->{'_prefixes'}->{$prefix}->{'timestamp'} = $timestamp;


    #stage 3
    #searching for a maching rib entry
    my $finderRes = $self->find_similar_ribentry($prefix, $update, $as_path);
    if(defined($finderRes)){
      if($opts{'mp'}){
        $finderRes->add_mp_nlri($prefix,$nlri);
      }else{
        $finderRes->add_nlri($prefix,$nlri);
      }
      $self->{'_prefixes'}->{$prefix}->{'ribentry'} = $finderRes;
      $self->{'_prefixes'}->{$prefix}->{'as_path'} = $as_path;
    }

    #Stage 4
    #make a new rib entry and add to hashes if one wasn't found
    else{
      my $newEntry = BGPmon::RIB::Entry->new($update);
      if($opts{'mp'}){
        $newEntry->add_mp_nlri($prefix,$nlri,$as_path);
      }else{
        $newEntry->add_nlri($prefix,$nlri,$as_path);
      }
      $self->insert($prefix,$newEntry,$as_path);
    }
  }
  return 0;
}


sub insert{
  my $self = shift;
  my $prefix = shift;
  my $newEntry = shift;
  my $aspath = shift;

  if(!exists($self->{'_as_path'}->{$aspath})){
    $self->{'_as_path'}->{$aspath} = [];
  }

  #Adding the rib entry to the aspaths groups
  push(@{$self->{'_as_path'}->{$aspath}}, $newEntry);

  #Adding to the prefix's data structure
  $self->{'_prefixes'}->{$prefix}->{'ribentry'} = $newEntry;
  $self->{'_prefixes'}->{$prefix}->{'as_path'} = $aspath;
}



=head2 remove
TODO
Will remove a prefix from the rib if it exists.

Input:  IPv4/6 prefix to remove

Output: 3 if prefix not in RIB
        2 aspath undefined for prefix
        1 on error
        0 on success
=cut
sub remove{
  my $self = shift;
  my $prefix = shift;

  #print Dumper $self->{'_prefixes'};

  #check to make sure prefix exists
  if(!exists($self->{'_prefixes'}->{$prefix})){
    #TODO make error code and return failure
    return 3;
  }
  #print Dumper $self->{'_prefixes'};

  #ensuring the as path is defined
  if(!defined($self->{'_prefixes'}->{$prefix}->{'as_path'})){
    #TODO return error code and failure
    return 2;
  }

  #Getting rib entry and aspath for that prefix
  my $re = $self->{'_prefixes'}->{$prefix}->{'ribentry'};
  my $aspath = $self->{'_prefixes'}->{$prefix}->{'as_path'};

  #removing prefix from the rib entry
  if($re->remove($prefix)){
    #TODO make error code for this
    return 1;
  }

  #checking to see if rib entry is empty; if so, remove it from as path group
  if($re->can_delete()){
    #getting the list of current re's for that path
    my @currREs = @{ $self->{'_as_path'}->{$aspath} };
    #taking the path out
    my $finalIndex = -1;
    for(my $i = 0; $i < scalar(@currREs); $i += 1){
      if($currREs[$i] eq $re){
        $finalIndex = $i;
        last;
      }
    }
    if($finalIndex == -1){
      #TODO create erorr code and return failure
      return 1;
    }
    splice(@currREs, $finalIndex, 1);

    #checking to see if aspath entry is empty, if so, remove it.
    if(scalar(@currREs) == 0){
      delete $self->{'_as_path'}->{$aspath};
    }
    #Give it the new array if everything is fine
    else{
      $self->{'_as_path'}->{$aspath} = \@currREs;
    }
  }


  #removing prefix from RIB
  delete $self->{'_prefixes'}->{$prefix};


  $self->{'error_code'} = NO_ERROR_CODE;
  $self->{'error_msg'} = NO_ERROR_MSG;
  return 0;
}





=head2 get_error_msg

Will return the error message of the given function name.

Input:  A string that contains the function name where an error occured.

Output: The message which represents the error stored from that function.

=cut
sub get_error_msg{
  my $self = shift;
  return $self->{'error_msg'};
}

=head2 get_error_code

Will return the error code of the given function name.

Input:  A string that represents the function name where an error occured.

Output: The code which represents the error stored from that function.

=cut
sub get_error_code{
  my $self = shift;
  return $self->{'error_code'};
}


=head1 ERROR CODES AND MESSAGES

The following error codes and messages are defined for the RIB module.

BLAH BLAH TODO

File uses Error codes 300-399
Archive uses Error codes 400-499
Client uses Error codes 500-599

    0:      No Error
            'No Error. Life is good.'

    101:    Too many or too few arguments were passed to connect_bgpdata or
                get_error_[code/message/msg]
            'Invalid number of arguments'

    102:    There is no connection (via connect_bgpdata) to a data source.
            'Not connected to a data source'

    103:    An invalid function name was passed to get_error_[code/message/msg]
            'Invalid function name specified'

=head1 AUTHOR

M. Lawrence Weikum, C<< <mweikum at rams.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bgpmon at netsec.colostate.edu>, or through
the web interface at L<http://bgpmon.netsec.colostate.edu>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::RIB

=cut

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2012 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\

    File: RIB.pm

    Authors: M. Lawrence Weikum
    Date: 15 January 2014

=cut




1;
__END__
