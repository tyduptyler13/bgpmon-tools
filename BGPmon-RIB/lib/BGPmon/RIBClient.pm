package BGPmon::RIBClient;
use 5.014002;
use strict;
use warnings;
use constant FALSE => 0;
use constant TRUE => 1;
use DateTime;
use File::Slurp qw /read_file/;
use POSIX qw/strftime/;
use LWP::Simple;

BEGIN {
    our $VERSION = '1.0';
    our @ISA = qw /Exporter/;
    our @EXPORT_OK = qw /fetch_rib_file/;
    our @EXPORT = qw();
}


sub fetch_rib_file {
    if (scalar(@_) != 2) {
        print STDERR "Incorrect number of arguments specified.\n";
        return undef, undef;
    }

    my ($archive_url, $e_time) = @_;

    #Append / at the end of the url if required.
    $archive_url .= '/' unless $archive_url =~ /\/$/;

    unless (defined($archive_url)) {
        print STDERR "Archive URL not specified.\n";
        return undef, undef;
    }

    unless (defined($e_time)) {
        print STDERR "Time at which RIB required is not specified.\n";
        return undef, undef;
    }

    if ($e_time < 0 || $e_time =~ /\D/) {
        print STDERR "Invalid timestamp specified.\n";
        return undef, undef;
    }

    my $tmp_e_time = $e_time;
    my ($loc, $s_time) = get_valid_rib_file($archive_url, $tmp_e_time);

    while (!defined($loc) && defined($s_time) && $s_time == -1) {
        my @time_els = gmtime($e_time);
        my $month = $time_els[4];
        my $year = $time_els[5];
        $year += 1900;

        # Roll back one month at a time.
        $month--;
        if ($month == -1) {
            $month = 11;
            $year--;
        }

        my $dt = DateTime->new(
            year => $year,
            month => $month,
            day => 1,
            hour => 0,
            minute => 0,
            time_zone => "GMT",
        );
        $tmp_e_time = $dt->epoch;
        ($loc, $s_time) = get_valid_rib_file($archive_url, $tmp_e_time);
    }

    # Return location of downloaded file and start time
    return ($loc, $s_time);
}

sub get_valid_rib_file {
    my ($archive_url, $e_time) = @_;

    # Convert unix timestamp to the format used in the archives.
    my @time_array = gmtime($e_time);
    my $year = strftime("%Y", @time_array);
    my $month = strftime("%m", @time_array);
    my $day = strftime("%d", @time_array);
    my $hour_min = strftime("%H%M", @time_array);
    my $ts_string = $year.$month.$day;

    $archive_url = $archive_url . "$year.$month/RIBS/";

    # Now, get the index file.
    my $index_file_loc = "./index.html";
    if (download_file($archive_url, $index_file_loc) == FALSE) {
        print STDERR "Error retrieving $archive_url\n";
        return undef, undef;
    }

    # Read the file into an array
    my @lines = read_file($index_file_loc);

    # First check that we have a valid index file.
    # Search lines for the string "Index of". TERRIBLE HACK!
    my $valid = FALSE;
    foreach my $line (@lines) {
        if ($line =~ /Index of/) {
            $valid = TRUE;
            last;
        }
    }
    if ($valid == FALSE) {
        print STDERR "The index file at the specified location is not valid\n";
        return undef, undef;
    }

    # We have a valid index file.
    my $fname;
    my $prev_file_name;
    my $curr_file_name;
    my @curr_file_name_els;
    my $s_time;
    foreach my $line (@lines) {
        if ($line =~ /(ribs.+\.xml)\"/) {
            $fname = $1;
        } else {
            next;
        }

        $prev_file_name = $curr_file_name;
        $curr_file_name = $fname;
        @curr_file_name_els = split(/\./, $curr_file_name);
        my $file_ts = $curr_file_name_els[1];
        my $file_hhmm = $curr_file_name_els[2];
        if ($file_ts != $ts_string) {
            next;
        }

        if ($file_hhmm >= $hour_min) {
            last;
        }
    }

    unless (defined($prev_file_name)) {
        return undef, -1;
    }

    # We have the required file name.
    # Now, convert file timestamp to unix epoch.
    my @prev_file_name_els = split(/\./, $prev_file_name);
    $prev_file_name_els[1] =~ /(\d\d\d\d)(\d\d)(\d\d)/;
    my ($yr, $mon, $d) = ($1, $2, $3);
    $prev_file_name_els[2] =~ /(\d\d)(\d\d)/;
    my ($hr, $m) = ($1, $2);
    my $dt = DateTime->new(
            year => $yr,
            month => $mon,
            day => $d,
            hour => $hr,
            minute => $m,
            time_zone => "GMT",
    );
    $s_time = $dt->epoch;

    # Now, download the correct RIB file.
    $archive_url .= $prev_file_name;
    my $rib_file_loc = "./$prev_file_name";
    if (download_file($archive_url, $rib_file_loc) == FALSE) {
        print STDERR "Error retrieving $archive_url\n";
        exit(0);
    }

    # Return the location of the RIB file and time to start
    # retrieving updates from.
    return ($rib_file_loc, $s_time);
}

sub download_file {
    my ($url, $loc) = @_;
    my $retval = LWP::Simple::getstore($url, $loc);
    if (LWP::Simple::is_error($retval)) {
        return FALSE;
    }
    return TRUE;
}

1;
