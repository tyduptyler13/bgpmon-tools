package BGPmon::RIB::Entry;
use 5.014002;
use strict;
use warnings;
use constant FALSE => 0;
use constant TRUE => 1;
use Data::Compare qw/Compare/;
use Data::Dumper;
require Exporter;

BEGIN{
  our $VERSION = '0.01';
  our @ISA = qw /Exporter/;
  our @EXPORT_OK = qw/ new get_peer_address get_monitor_address 
    ingest_message get_all_prefixes flag_finished is_finished 
    get_error_msg get_error_code /;
  our @EXPORT = qw();
}



## Variable to hold error codes and messages
# No Errors
use constant NO_ERROR_CODE => 0;
use constant NO_ERROR_MSG => 'No Error. Relax with some tea.';

=head1 NAME

BGPmon::RIB::ENTRY

The BGPmon RIB ENTRY module, blah blah blah TODO

=head1 SYNOPSIS

The BGPmon::RIB::ENTRY module provides blah blah blah TODO functionality to connect to one of the

    use BGPmon::Translator::XFB2PerlHash qw /translate_msg/;
    use BGPmon::RIB::ENTRY;

    my $xml_msg = '<BGP_MONITOR_MESSAGE><...';
    my $hash = translate_msg($xml_msg);

    my $entry = BGPmon::RIB::ENTRY->new();
    $entry->

    my $ret = init_bgpdata();
    my $ret = connect_bgpdata();
    my $xml_msg = read_xml_message();
    if ( !defined($xml_msg) ){
        print get_error_code() . ": " . get_error_msg();
    }
    my $ret = is_connected();
    my $num_read = messages_read();
    my $uptime = uptime();
    my $ret = close_connection();
    my $downtime = connection_endtime();

=head1 EXPORT

None - this is Object Oriented

=cut

=head1 SUBROUTINES/METHODS


=head2 new

Initializes a new RIB given the peer's IP address and a BGPmon IP address.

Input: Update node from BGPmon::Message

Output: Undef on error
        RIB class reference on success
=cut

sub new{
  my $class = shift;
  my $update = shift;

  if(!defined($update)){
    return undef;
  }
  my $self = {
    xml_nodes => $update,
    nlri => {},
    mp_nlri => {},
    xml => undef, #this will be either nlri or mpreach nlri:
  };

  bless $self, $class;
  return $self;
}
=comment
sub add_update{
  my $self = shift;
  my $update = shift;
  if(!defined($update)){
    return 1;
  }
  $self->{'xml_nodes'} = $update;
  return 0;
}
=cut
sub get_xml_nodes{
  my $self = shift;
  return $self->{xml_nodes};
}

sub get_nlri{
  my $self = shift;
  return $self->{nlri};
}
sub get_mp_nlri{
  my $self = shift;
  return $self->{mp_nlri};
}

=head2 add_nlri

Adds an IP prefix to the list of prefixes which can be associated with
this RIB entry.

Input: IPv4

Output: 1 on error
        0 on success

=cut
sub add_nlri{
  my $self = shift;
  my $prefix = shift;
  my $nlri = shift;


  #check we get a prefix
  if(!defined($prefix)){
    #TODO make error code and return failure
    return 1;
  }

  #check we got an nlri
  if(!defined($nlri)){
    #TODO make error code and return failure
    return 1;
  }

  $self->{'nlri'}->{$prefix} = $nlri;

  return 0;
}
sub add_mp_nlri{
  my $self = shift;
  my $prefix = shift;
  my $nlri = shift;

  #check we get a prefix
  if(!defined($prefix)){
    #TODO return error code and fialure
    return 1;
  }

  #check we got an nlri
  if(!defined($nlri)){
    #TODO make error code and return failure
    return 1;
  }

  $self->{'mp_nlri'}->{$prefix} = $nlri;

  return 0;
}




=head2 remove

Will remove a prefix from the list of prefixes which are associated with
this RIB entry

Input:  IPv4/6 prefix

Output: 1 if error
        0 if error
=cut
sub remove{
  my $self = shift;
  my $p = shift;

  #Make sure we have been given a prefix
  if(!defined($p)){
    #TODO SEND BACK ERROR
    return 1;
  }

  #See if the prefix is in our list
  if(!exists($self->{'nlri'}->{$p}) && !exists($self->{'mp_nlri'}->{$p})){
    #TODO make error code and return failure
    return 1;
  }
  if(exists($self->{'nlri'}->{$p})){
    #Remove that prefix
    delete $self->{'nlri'}->{$p};
  }
  
  if(exists($self->{'mp_nlri'}->{$p})){
    #Remove that prefix
    delete $self->{'mp_nlri'}->{$p};
  }
  return 0;
}



=head2 is_similar

Will check to see if data from a new xml message is similar to that of this
RIB entry.  It will check the BGP Attributes found in 
BGP_MONITOR_MESSAGE/bgp:UPDATE/

Input: Hash of XML message generated from BGPmon::Translator::XFB2PerlHash

Output: undef if error
        0 if not simialr
        1 if similar

=cut
sub is_similar{
  my $self = shift;
  my $update = shift;

  if(!defined($update)){
    #TODO make error code and return failure
    return undef;
  }

  return Compare($self->{'xml_nodes'}, $update);
}






=head2 can_delete

Will check to see if there are any prefixes associated with this RIB entry.
If there are none, it is safe to remove this RIB entry.

Input: None

Output: 0 if not safe to delete
        1 if safe to delete

=cut
sub can_delete{
  my $self = shift;  
  #CHECK IF THERE ARE ANY OTHER PREFIXES
  my @prefs = keys($self->{'nlri'});
  push @prefs,keys($self->{'mp_nlri'});
  my $size = scalar @prefs;
  if($size == 0){
    return TRUE;
  }
  return FALSE;
}

=head1 ERROR CODES AND MESSAGES

The following error codes and messages are defined for the RIB module.

BLAH BLAH TODO

File uses Error codes 300-399
Archive uses Error codes 400-499
Client uses Error codes 500-599

    0:      No Error
            'No Error. Life is good.'

    101:    Too many or too few arguments were passed to connect_bgpdata or
                get_error_[code/message/msg]
            'Invalid number of arguments'

    102:    There is no connection (via connect_bgpdata) to a data source.
            'Not connected to a data source'

    103:    An invalid function name was passed to get_error_[code/message/msg]
            'Invalid function name specified'

=head1 AUTHOR

M. Lawrence Weikum, C<< <mweikum at rams.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bgpmon at netsec.colostate.edu>, or through
the web interface at L<http://bgpmon.netsec.colostate.edu>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::RIB

=cut

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2012 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\

    File: RIB.pm

    Authors: M. Lawrence Weikum
    Date: 15 January 2014

=cut


1;
