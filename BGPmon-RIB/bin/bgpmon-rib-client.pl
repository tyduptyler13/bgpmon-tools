#!/usr/bin/perl

$| = 1; #flush every time we print
our $VERSION = '1.092';
use strict;
use warnings;

use constant FALSE => 0;
use constant TRUE => 1;

# By default, connect to our archive.
use constant DEFAULT_BGPMON_ARCHIVE =>
'http://archive-test.netsec.colostate.edu/peers/';

# BGPmon modules
use BGPmon::Fetch;
use BGPmon::RIB;
use BGPmon::Configure;
use BGPmon::RIBClient;

# Other modules
use LWP::Simple;
use File::Path qw(make_path remove_tree);
use File::Slurp qw /read_file/;
use File::Basename;
use DateTime;
use POSIX qw /strftime/;

my $pid = $$;
my $tmp_loc = "/tmp/bgpmon-rib." . $pid;

# Parse command line arguments
unless (parse_and_check()) {
    exit 1;
}

# Location of BGPmon archive
my $bgpmon_archive = BGPmon::Configure::parameter_value("bgpmon-archive");
unless (defined($bgpmon_archive)){
    print STDERR "Using default archives from " . DEFAULT_BGPMON_ARCHIVE . "\n";
    $bgpmon_archive = DEFAULT_BGPMON_ARCHIVE;
}

# Get timing information
my $e_time = BGPmon::Configure::parameter_value("time");
unless (defined($e_time)){
    print STDERR "Please specify time (as UNIX epoch) at which RIB state
    is required.\n";
    exit(0);
}

# Sanity check time stamp.
if ($e_time =~ /\D/ || $e_time < 0) {
    printf STDERR "Invalid timestamp specified.\n";
    exit(0);
}

# Check output format.
my $format = BGPmon::Configure::parameter_value("format");
unless (defined($format)) {
    $format = "xml";
}

# Get the peer information. If not defined, quit.
my $peer_list = BGPmon::Configure::parameter_value("peers");
unless (defined($peer_list)) {
    print STDERR "Please specify peer.\n";
    exit(0);
}

my @peers = split(/\,/, $peer_list);
my %all_peers;
foreach my $peer (@peers) {
    print "$peer\n";
    # Append peer to the end of the URL.
    my $archive_url = $bgpmon_archive . $peer;

    # Sanity check the url
    $archive_url .= '/' unless ($archive_url =~ /\/$/);
    $archive_url = "http://" . $archive_url unless $archive_url =~ /http/;

    ##----- First, retrieve the RIBs. -----
    my ($rib_file, $s_time) =
        BGPmon::RIBClient::fetch_rib_file($archive_url, $e_time);
    print $s_time . "\n";
    unless (defined($rib_file) and defined($s_time)) {
        cleanup();
        exit(0);
    }

    # Open RIB file for reading.
    my $fh;
    open ($fh, $rib_file) || die "Could not open file $rib_file: $!";

    # Read messages from XML file
    my $xml_msg = "";
    while (my $line = <$fh>) {
        chomp($line);
        # Filter out first <xml> message.
        next if ($line =~ /<xml>/);
        # Filter out last </xml> message.
        last if ($line =~ /<\/xml>/);
        # Filter out any ARCHIVER status messages.
        next if ($line =~ /ARCHIVER/);

        $xml_msg = $line;
        # If not, create a RIB entry for peer if required.
        unless (exists($all_peers{$peer})) {
            $all_peers{$peer} = BGPmon::RIB->new($xml_msg);
        }

        # Now process the xml message.
        $all_peers{$peer}->ingest_message($xml_msg);
    }

# Close RIB file handle.
    close($fh);

##----- Now, retrieve the UPDATEs. -----
    if (BGPmon::Fetch::init_bgpdata()) {
        printf STDERR "init_bgpdata() failed: " .
        BGPmon::Fetch::get_error_msg('init_bgpdata') . "\n";
        cleanup();
        exit(0);
    }

    print "$archive_url, $s_time, $e_time\n";
    if (BGPmon::Fetch::connect_bgpdata($archive_url, $s_time, $e_time)) {
        print STDERR "connect_bgpdata() failed: " .
        BGPmon::Fetch::get_error_msg('connect_bgpdata') . "\n";
        cleanup();
        exit(0);
    }

    $xml_msg = "";
    while (1) {
        $xml_msg = BGPmon::Fetch::read_xml_message();
        unless (defined($xml_msg)) {
            print STDERR "Error reading XML message: " .
            BGPmon::Fetch::get_error_msg('read_xml_message');
            last; #TODO: Verify what read_xml_message() returns when it reaches EOF
        }

        $all_peers{$peer}->ingest_message($xml_msg);
    }

    BGPmon::Fetch::close_connection();

##----- Now, print the XML. -----
    foreach my $peer (keys %all_peers) {
        if ($format eq "bgpdump") {
            $all_peers{$peer}->print_table(*STDOUT, format => 'txt');
        } else {
            $all_peers{$peer}->print_table(*STDOUT);
        }
    }
# Cleanup after ourselves after we are done.
    cleanup();
}
##############################END MAIN PROGRAM#################################

# parse the command line arguments or configuration file
sub parse_and_check{
    my @params = (
        {
            Name => "peers",
            Type => BGPmon::Configure::ADDRESS,
            Default => undef,
            Description => "The peer(s) to filter for."
        },
        {
            Name => "bgpmon-archive",
            Type => BGPmon::Configure::STRING,
            Default => DEFAULT_BGPMON_ARCHIVE,
            Description => "The url of the BGPmon archive"
        },
        {
            Name => "time",
            Type => BGPmon::Configure::UNSIGNED_INT,
            Default => time() - 900,
            Description => "Time of desired RIB table [default:15m in the past]"
        },
        {
            Name => "format",
            Type => BGPmon::Configure::STRING,
            Default => "xml",
            Description => "Output format [default:xml]"
        },
    );

    # Checking that everything parsed correctly
    if (BGPmon::Configure::configure(@params)) {
        my $code = BGPmon::Configure::get_error_code("configure");
        my $msg = BGPmon::Configure::get_error_message("configure");
        print "$code: $msg\n";
        return FALSE;
    }

    #Checking that we have the minimum configuration to run
    return TRUE;
}

sub cleanup {
    # Delete our RIB file.
    remove_tree($tmp_loc);
}

sub make_dir {
    my $path = shift;
    unless (-d $path) {
        make_path($path, {error => \my $err, mode => 0755,});
        if (@$err) {
            for my $diag (@$err) {
                my ($file, $message) = %$diag;
                if ($file eq '') {
                    print STDERR
                    "General error creating tmp directory: $message\n";
                } else {
                    print STDERR
                    "Error creating tmp directory: $message\n";
                }
            }
            return FALSE;
        }
    }
    return TRUE;
}

