#!/usr/bin/perl
use strict;
use warnings;
use constant FALSE => 0;
use constant TRUE => 1;
use BGPmon::Log qw /log_init log_err log_err log_warn log_notice log_info debug log_close/;
use BGPmon::Configure;
use BGPmon::Fetch qw /connect_bgpdata read_xml_message is_connected close_connection/;
use BGPmon::Translator::XFB2PerlHash::Simpler qw /parse_xml_msg get_error_msg get_error_code extract_status extract_nlri extract_mpreach_nlri extract_sender_addr extract_monitor_addr/;

use Data::Dumper;

#-- Program name
my $progName = $0;

#-- Variables to hold the error messages

#-- Signal Handlers
$SIG{INT} = $SIG{TERM} = $SIG{KILL} = $SIG{HUP} = \&ribExit;
#For the safe exit of exiting the program
sub ribExit{
  close_connection() if(is_connected());
  log_info("Closing RIB get.");
  exit 0;
}

##--- Variables for Logging ---
#LOG_EMERG  : 0
#LOG_ALERT  : 1
#LOG_CRIT : 2
#LOG_ERR  : 3
#LOG_WARNING  : 4
#LOG_NOTICE : 5
#LOG_INFO : 6
#LOG_DEBUG  : 7

my $logLevel;
my $useSyslog;
my $logFile;



#---  BGPmon variables
my $server; #-- bgpmon server
my $port; #-- bgpmon port (the main port number)

#-- For the specific monitor and peer that we're going to look for
my $monitor = '128.223.51.15'; #TODO CHANGE THIS AWAY LATER
my $peer = '94.126.183.247'; #TODO - CHANGE THIS AWAY LATER

#--- Misc. variables
my $debug = FALSE;
my $output_file;
my $stdout;

if(!parseAndCheck()){
  exit 1;
}

if($debug){
  printDebugInfo();
}

###Starting the log
#moving old log file to .old
if(-e $logFile){
  my $newLogFileName = $logFile.".old";
  my $res = `mv $logFile $newLogFileName`;
}
# Starting the log file
my $logRetVal = log_init(use_syslog => 0,
    log_level => $logLevel,
    log_file => $logFile,
    prog_name => $progName);
if($logRetVal and defined($logFile)) {
  print STDERR "Error initilaizing log.\n";
  exit 1;
}
log_info("$progName has started the log file.");





# Connecting to BGPmon
log_info("Connecting to source: $server:$port");
print "Connecting to BGPmon\n" if $debug;
if(connect_bgpdata($server, $port)){
  print "Couldn't connect to the BGPmon server.  Aborting.\n";
  log_err("Coudln't connect to BGPmon server.");
  exit 1;
}
print "Connected to BGPmon server!\n" if $debug;
log_info("Connected to BGPmon server.");


# Hash for the prefixes we're about to see
my %prefixes = ();


#Waiting for the TABLE_START message
if(waitForTableStart()){
  exit 1;
}
#Reading in the RIB
if(readRib()){
  exit 1;
}

#Cleaning up connections
close_connection();
log_info("Closed connection to $server:$port");

#Writing out to stdout or to a file
if(writeOutPrefixes()){
  exit 1;
}


=comment
Will write a list of sorted prefixes to standard out or to a file, whichever
was selected from the options
=cut
sub writeOutPrefixes{

  #Getting and sorting prefixes
  my @prefs = getSortedPrefixes();
  my $num_prefixes = scalar(@prefs);
  log_info("Num Prefixes Found: $num_prefixes");
  print "Num Prefixes Found: $num_prefixes\n";
  print "Monitor: $monitor. Peer: $peer\n";

  #Writing out to a file
  if(defined($output_file)){
    log_info("Writing the prefixes to a file: $output_file");
    my $OUTFILE;
    open $OUTFILE, ">", $output_file or die "Error opening file $!";
    foreach(@prefs){
      print {$OUTFILE} "$_\n";# or croak "Cannot write to $output_file: $OS_ERROR";
    }
    close $OUTFILE;

  }


  #Printing to standard out
  if($stdout){
    log_info("Writing prefixes to standard out.");
    print "$_\n" foreach(@prefs);
  }
}


sub getSortedPrefixes{

  my @prefs = keys(%prefixes);

  #TODO - this will only sort lexographically, needs to sort by IPv4 prefixes

  return sort @prefs;
}














sub waitForTableStart{
  log_info("Awaiting for TABLE_START message");
  while(TRUE){
    if(!is_connected()){
      log_err("lost connection to BGPmon instance!");
      return 1;
    }
    my $xml_msg = read_xml_message();
    next if !defined($xml_msg) or $xml_msg eq "";


    if(parse_xml_msg($xml_msg)){
      my $error_code = get_error_code('parse_xml_msg');
      my $error_msg = get_error_msg('parse_xml_msg');
      log_err("Couldn't read message from stream!:$error_code:$error_msg");
      return 1;
    }
    my $status = extract_status();
    next if(!defined($status));

    if($status eq "TABLE_START"){
      #making sure we have the correct table start stream
      if(parse_xml_msg($xml_msg)){
        log_err("Coudn't parse xml message");
        next;
      }
      my $sender = extract_sender_addr();
      my $mon = extract_monitor_addr();

      #LAWRENCE - THIS IS FOR TESTING, REMOVE LATER!
      $peer = $sender;
      $monitor = $mon;
      print "LAWRENCE: PEER:$peer MONITOR:$monitor\n";

      if($sender eq $peer and $mon eq $monitor){
        log_info("Found the start of the table stream.");
        #Adding the information to the table
        addToTable();
        last;
      }
    }
  }
  return 0;
}


sub readRib{
  log_info("Reading RIB stream.");
  while(TRUE){

    if(!is_connected()){
      log_err("lost connection to BGPmon instance!");
      return 1;
    }

    my $xml_msg = read_xml_message();

    next if !defined($xml_msg) or $xml_msg eq "";

    if(parse_xml_msg($xml_msg)){
      my $error_code = get_error_code();
      my $error_msg = get_error_msg();
      log_err("Couldn't read message form stream!:$error_code:$error_msg");
      return 1;
    }

    #Making sure we have the data from the correct source
    my $sender = extract_sender_addr();
    my $mon = extract_monitor_addr();
    next if !defined($sender) or ($sender ne $peer) or ($mon ne $monitor);

    #Seeing if we have to stop
    my $status = extract_status();
    if(defined($status) and $status eq "TABLE_STOP"){
      log_info("Found the end of the RIB stream.");
      last;
    }

    #Adding the information to the table
    addToTable();
  }
  return 0;
}


#Is only called after BGPmon::Translator::XFB2PerlHash::Simpler has 
#parsed the message
sub addToTable{
    
    #Adding the information to the table
    my @nlris = extract_nlri();
    my @mp_nlris = extract_mpreach_nlri();

    if(@nlris){
      foreach(@nlris){
        $prefixes{$_} = 1;
      }
    }
    if(@mp_nlris){
      foreach(@mp_nlris){
        $prefixes{$_} = 1;
      }
    }
}



#TODO - ADD IN SPECIFIC MONITOR AND PEER TO SEARCH FOR!
sub parseAndCheck{
  my @params = (
      {
        Name  => BGPmon::Configure::CONFIG_FILE_PARAMETER_NAME,
        Type  => BGPmon::Configure::FILE,
        Default => undef, 
        Description => "This is the configuration file name.",
      },
      {
        Name => "server",
        Type => BGPmon::Configure::STRING,
        Default => undef,
        Description => "This is the BGPmon server address",
      },
      {
        Name => "port",
        Type => BGPmon::Configure::PORT,
        Default => 50002,
        Description => "This is the BGPmon server RIB port number",
      },
      {
        Name => "output_file",
        Type => BGPmon::Configure::FILE,
        Default => undef,
        Description => "This is where the list of prefixes will be saved ".
          "if the user wants them.",
      },
      {
        Name => "stdout",
        Type => BGPmon::Configure::BOOLEAN,
        Default => FALSE,
        Description => "This is will print the prefixes to standard out ".
          "if the user wants them.",
      },
      {
        Name => "log_file",
        Type => BGPmon::Configure::FILE,
        Default => undef,
        Description => "This is the location the log file will be saved",
      },
      {
        Name => "log_level",
        Type => BGPmon::Configure::UNSIGNED_INT,
        Default => 7,
        Description => "This is how verbose the user wants the log to be",
      },
      {
        Name => "debug",
        Type => BGPmon::Configure::BOOLEAN,
        Default => FALSE,
        Description => "This is for debugging purposes",
      },
      {
        Name => "validate",
        Type => BGPmon::Configure::BOOLEAN,
        Default => FALSE,
        Description => "This will tell the filter if it should validate each ".
          "xml message it receives",
      },
      );


  #Checking that everything parsed correctly
  if(BGPmon::Configure::configure(@params) ) {
    my $code = BGPmon::Configure::get_error_code("configure");
    my $msg = BGPmon::Configure::get_error_message("configure");
    print "$code: $msg\n";
    return FALSE;
  }

  #Moving all of the variables into the variables from previous version
  $server = BGPmon::Configure::parameter_value("server");
  $port = BGPmon::Configure::parameter_value("port");
  $debug = BGPmon::Configure::parameter_value("debug");
  $logFile = BGPmon::Configure::parameter_value("log_file");
  $logLevel = BGPmon::Configure::parameter_value("log_level");
  #$validate = BGPmon::Configure::parameter_value("validate");
  $output_file = BGPmon::Configure::parameter_value("output_file");
  $stdout = BGPmon::Configure::parameter_value("stdout");


  #Checking that we have the minimum configuration to run
  unless(defined($server)){
    print STDERR "You must specify a bgpmon-server to connect to.\n";
    return FALSE;
  }
  unless(defined($output_file) or $stdout){
    print STDERR "You must specify to save the data to an output file or ".
      "to standard out.";
    return FALSE;
  }

  return TRUE;
}

sub printDebugInfo{

}


