use warnings;
use strict;

use 5.014002;

use Test::More;
use BGPmon::RIB::Entry;
use BGPmon::Message;
use constant TRUE => 1;
use constant FALSE => 0;

use BGPmon::Translator::XFB2PerlHash qw /translate_msg reset/;
use Data::Dumper;
use Data::Compare;
BEGIN{

}


## Example XML message to use for parsing - ipv4, nlri
my $xml = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">94.126.183.247</ADDRESS><PORT>179</PORT><ASN4>59469</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390411034</TIMESTAMP><DATETIME>2014-01-22T17:17:14Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>753408484</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>59469</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>3257</bgp:ASN4><bgp:ASN4>15412</bgp:ASN4><bgp:ASN4>18101</bgp:ASN4><bgp:ASN4>45117</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">94.126.183.247</bgp:NEXT_HOP><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3257</bgp:ASN2><bgp:VALUE>3257</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>2</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>22</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>86</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>500</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>666</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>2064</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>11078</bgp:VALUE></bgp:COMMUNITY><bgp:NLRI afi="1">27.54.183.0/24</bgp:NLRI><bgp:NLRI afi="1">103.11.119.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF006A020000004B4001010040021A02060000E84D00000D1C00000CB900003C34000046B50000B03D4003045E7EB7F7C008200CB90CB90D1C00020D1C00160D1C00560D1C01F40D1C029A0D1C08100D1C2B46181B36B718670B77</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#Different xml message that just has NLRI, IPv4
my $xml2 = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">185.5.200.255</ADDRESS><PORT>179</PORT><ASN4>59715</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390415210</TIMESTAMP><DATETIME>2014-01-22T18:26:50Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>753702402</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>59715</bgp:ASN4><bgp:ASN4>3269</bgp:ASN4><bgp:ASN4>6762</bgp:ASN4><bgp:ASN4>2914</bgp:ASN4><bgp:ASN4>32787</bgp:ASN4><bgp:ASN4>19886</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">185.5.200.255</bgp:NEXT_HOP><bgp:NLRI afi="1">171.162.247.0/24</bgp:NLRI><bgp:NLRI afi="1">171.181.119.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF004702000000284001010040021A02060000E94300000CC500001A6A00000B620000801300004DAE400304B905C8FF18ABA2F718ABB577</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#XML message that has an IPv6/MP_REACH in it
my $xml3 = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2607:4200:10::2</ADDRESS><PORT>179</PORT><ASN4>19214</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390422185</TIMESTAMP><DATETIME>2014-01-22T20:23:05Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1943427430</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>19214</bgp:ASN4><bgp:ASN4>174</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>29119</bgp:ASN4><bgp:ASN4>58345</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:MP_REACH_NLRI optional="true" transitive="false" partial="false" extended="false" attribute_type="14" safi="1"><bgp:MP_NEXT_HOP afi="2">2607:4200:10::2</bgp:MP_NEXT_HOP><bgp:MP_NLRI afi="2">2a03:e080::/32</bgp:MP_NLRI></bgp:MP_REACH_NLRI></bgp:UPDATE></BGP_MONITOR_MESSAGE>';

#XML message that has a different IPv6/MP_REACH in it
my $xml4 = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2607:f278:0:ffff::2</ADDRESS><PORT>179</PORT><ASN4>6360</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:330f</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390422975</TIMESTAMP><DATETIME>2014-01-22T20:36:15Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>754489868</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>6360</bgp:ASN4><bgp:ASN4>19401</bgp:ASN4><bgp:ASN4>23911</bgp:ASN4><bgp:ASN4>4134</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>65535</bgp:ASN2><bgp:VALUE>65281</bgp:VALUE></bgp:COMMUNITY><bgp:MP_REACH_NLRI optional="true" transitive="false" partial="false" extended="true" attribute_type="14" safi="1"><bgp:MP_NEXT_HOP afi="2">2607:f278:0:ffff::2</bgp:MP_NEXT_HOP><bgp:MP_NLRI afi="2">2406:8500::/32</bgp:MP_NLRI></bgp:MP_REACH_NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0055020000003E400101004002120204000018D800004BC900005D6700001026C00804FFFFFF01900E001A000201102607F2780000FFFF0000000000000002002024068500</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';


#Solves an issue with testing in different directories
my $resp = `pwd`;
my $location = 't/';
if($resp =~ m/bgpmon-tools\/BGPmon-core\/t/){
        $location = '';
}

#Randomly created ip addresses for testing
my $test_peer = '129.82.2.2';
my $test_monitor = '129.82.138.77';


## Testing new
my $testRib = BGPmon::RIB::Entry->new();
ok(!defined($testRib), "Test BGPmon::RIB::Entry->new without update nodei");
my $bgpmsg = BGPmon::Message->new(string=>$xml);
my $update = $bgpmsg->remove_node('/BGP_MONITOR_MESSAGE/UPDATE');
$testRib = BGPmon::RIB::Entry->new($update);
ok(defined($testRib), "Testing BGPmon::RIB::Entry->new with update node");

## TEsting get_xml_nodes
#Testing that we get soemthihng back
my $ret = $testRib->get_xml_nodes();
ok(defined($ret), "Making sure we're getting back an ansewer of some sort");

## TEsting add_nlri
#Testing with no prefix
my $aRet = $testRib->add_nlri();
is($aRet, 1, 'Adding nlri with no prefix');
#Testing with no nlri
$aRet = $testRib->add_nlri('129.82.0.0/16');
is($aRet, 1, 'Adding nlri with no nlri, only prefix');
#Testing with both prefix and nlri
$bgpmsg = BGPmon::Message->new(string=>$xml);
my $nlri = $bgpmsg->remove_node('/BGP_MONITOR_MESSAGE/UPDATE/NLRI');
$aRet = $testRib->add_nlri('103.11.119.0/24', $nlri);
is($aRet, 0, "Adding nlri with prefix and nlri");

## TEsting add_mp_nlri
#TEsting wiht no prefix
my $mpRet = $testRib->add_mp_nlri();
is($mpRet, 1, 'Adding mp_nlri with no prefix');
#Testing with no nlri
$mpRet = $testRib->add_mp_nlri('129.82.0.0/16');
is($mpRet, 1, 'Adding mp_nlri with no nlri, only prefix');
#Testing with both prefix and nlrii
$bgpmsg = BGPmon::Message->new(string=>$xml3);
my $mp_nlri = $bgpmsg->get_mp_nlri_nodes(remove=>1);
$mpRet = $testRib->add_nlri('103.11.119.0/24', $mp_nlri);
is($mpRet, 0, "Adding mp_nlri with prefix and nlri");

## TEsting get_nlri
my $gRet = $testRib->get_nlri();
ok(defined($gRet), 'Testing getting nlri');

## TEsting get_mp_nlri
my $gnRet = $testRib->get_mp_nlri();
ok(defined($gnRet), 'Testing getting mp_nlri');

## TEsting remove
#Testing remove with no preofix.i
my $rRet = $testRib->remove();
is($rRet, 1, "Testing remove with no preifx");
#TEsting remove with a prefix that's not in it.
$rRet = $testRib->remove('0.0.0.0/0');
is($rRet, 1, "Testing remove with a preifx not in entry");
#Testing romove with a prefix that's in it.
$rRet = $testRib->remove('103.11.119.0/24');
is($rRet, 0, "Testing remove with a preifx in entry");


## TEsting is_similar
#Testing with no argument
my $sRet = $testRib->is_similar();
is($sRet, undef, "Testing is_simlar with no arguent");
#Testing with bad similarity
my $bgpmsg3 = BGPmon::Message->new(string=>$xml3);
my $update3 = $bgpmsg3->remove_node('/BGP_MONITOR_MESSAGE/UPDATE');
$sRet = $testRib->is_similar($update3);
is($sRet, 0, "Testing is_simlar with no arguent");
#Testing with good similarity
my $bgpmsg1 = BGPmon::Message->new(string=>$xml);
my $update1 = $bgpmsg1->remove_node('/BGP_MONITOR_MESSAGE/UPDATE');
$sRet = $testRib->is_similar($update1);
is($sRet, 1, "Testing is_simlar with no arguent");

## Testing can_delete
#Shouldn't delete if there are still prefixes
#  making sure we have soemthign in the nlri since we delted it before
$aRet = $testRib->add_nlri('103.11.119.0/24', $nlri);
my $delRef = $testRib->can_delete();
is($delRef, 0, 'Testing can_delete with full tables');
#removing all prefixes
$testRib->remove('103.11.119.0/24');
$delRef = $testRib->can_delete();
is($delRef, 1, 'Testing can_delete with full tables');



done_testing();



