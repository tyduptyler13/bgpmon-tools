use warnings;
use strict;

use Test::More;
use Test::Output;
use BGPmon::RIBClient;
use constant TRUE => 1;
use constant FALSE => 0;
use constant ARCHIVE => 'http://archive-test.netsec.colostate.edu/peers/128.223.51.15';

## Testing fetch_rib_file
my ($loc, $e_time) = BGPmon::RIBClient::fetch_rib_file();
ok (!defined($loc) && !defined($e_time), "Testing fetch_rib_file with no arguments");

($loc, $e_time) = BGPmon::RIBClient::fetch_rib_file("abc", 123, '123');
ok (!defined($loc) && !defined($e_time),
	"Testing fetch_rib_file with incorrect number of arguments");

($loc, $e_time) = BGPmon::RIBClient::fetch_rib_file(ARCHIVE, -1);
ok (!defined($loc) && !defined($e_time), "Testing fetch_rib_file with bad ts");

($loc, $e_time) = BGPmon::RIBClient::fetch_rib_file("foo.com/", 1395764972);
ok (!defined($loc) && !defined($e_time), "Testing fetch_rib_file with bad archive loc");

($loc, $e_time) = BGPmon::RIBClient::fetch_rib_file(ARCHIVE, 1395764972);
ok(defined($loc) && defined($e_time), "Testing with correct arguments.");

unlink './ribs.20140325.1312.128.223.51.15.xml';

done_testing();
