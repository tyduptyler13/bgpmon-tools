use warnings;
use strict;

use Test::More;
use BGPmon::RIB;
use constant TRUE => 1;
use constant FALSE => 0;

use Data::Dumper;

## sometimes we just want to dump our output
#open(my $dump, ">", "/dev/null") or die "Unable to open /dev/null\n";

#Solves an issue with testing in different directories
=comment
my $resp = `pwd`;
my $location = 't/';
if($resp =~ m/bgpmon-tools\/BGPmon-core\/t/){
        $location = '';
}
=cut


## A few samle xml messages to get things going.
#table start message to pass to new()
my $table_start = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements">
<SOURCE><ADDRESS afi="2">2001:4810::1</ADDRESS><PORT>179</PORT><ASN4>33437</ASN4></SOURCE>
<DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST>
<MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR>
<OBSERVED_TIME precision="false"> <TIMESTAMP>1390342758</TIMESTAMP><DATETIME>2014-01-21T22:19:18Z</DATETIME></OBSERVED_TIME>
<SEQUENCE_NUMBER>1942880692</SEQUENCE_NUMBER>
<STATUS Author_Xpath="/BGP_MONITOR_MESSAGE/MONITOR"><TYPE>TABLE_START</TYPE></STATUS></BGP_MONITOR_MESSAGE>';


my $xml = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">94.126.183.247</ADDRESS><PORT>179</PORT><ASN4>59469</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390411034</TIMESTAMP><DATETIME>2014-01-22T17:17:14Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>753408484</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>59469</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>3257</bgp:ASN4><bgp:ASN4>15412</bgp:ASN4><bgp:ASN4>18101</bgp:ASN4><bgp:ASN4>45117</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">94.126.183.247</bgp:NEXT_HOP><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3257</bgp:ASN2><bgp:VALUE>3257</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>2</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>22</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>86</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>500</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>666</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>2064</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>11078</bgp:VALUE></bgp:COMMUNITY><bgp:NLRI afi="1">27.54.183.0/24</bgp:NLRI><bgp:NLRI afi="1">103.11.119.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF006A020000004B4001010040021A02060000E84D00000D1C00000CB900003C34000046B50000B03D4003045E7EB7F7C008200CB90CB90D1C00020D1C00160D1C00560D1C01F40D1C029A0D1C08100D1C2B46181B36B718670B77</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

my $xml6 = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2607:4200:10::2</ADDRESS><PORT>179</PORT><ASN4>19214</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390422185</TIMESTAMP><DATETIME>2014-01-22T20:23:05Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1943427430</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>19214</bgp:ASN4><bgp:ASN4>174</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>29119</bgp:ASN4><bgp:ASN4>58345</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:MP_REACH_NLRI optional="true" transitive="false" partial="false" extended="false" attribute_type="14" safi="1"><bgp:MP_NEXT_HOP afi="2">2607:4200:10::2</bgp:MP_NEXT_HOP><bgp:MP_NLRI afi="2">2a03:e080::/32</bgp:MP_NLRI></bgp:MP_REACH_NLRI></bgp:UPDATE></BGP_MONITOR_MESSAGE>';

my $brokenXml = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">94.126.183.247</ADDRESS><PORT>179</PORT><ASN4>59469</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390411034</TIMESTAMP><DATETIME>2014-01-22T17:17:14Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>753408484</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>59469</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>3257</bgp:ASN4><bgp:ASN4>15412</bgp:ASN4><bgp:ASN4>18101</bgp:ASN4><bgp:ASN4>45117</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">94.';

my $withXML = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">216.218.252.164</ADDRESS><PORT>179</PORT><ASN4>6939</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.108</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.108</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390425647</TIMESTAMP><DATETIME>2014-01-22T21:20:47Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>990279161</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:WITHDRAW afi="1">113.212.75.0/24</bgp:WITHDRAW><bgp:WITHDRAW afi="1">113.212.78.0/24</bgp:WITHDRAW><bgp:WITHDRAW afi="1">69.26.162.0/24</bgp:WITHDRAW><bgp:WITHDRAW afi="1">109.161.64.0/20</bgp:WITHDRAW><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>6939</bgp:ASN4><bgp:ASN4>3549</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>31133</bgp:ASN4><bgp:ASN4>50928</bgp:ASN4><bgp:ASN4>34145</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">216.218.252.164</bgp:NEXT_HOP><bgp:NLRI afi="1">109.124.0.0/19</bgp:NLRI><bgp:NLRI afi="1">109.124.32.0/20</bgp:NLRI><bgp:NLRI afi="1">109.124.48.0/21</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF005B0200101871D44B1871D44E18451AA2146DA14000284001010040021A020600001B1B00000DDD00000D1C0000799D0000C6F000008561400304D8DAFCA4136D7C00146D7C20156D7C30</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';


my $unreachXML = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2001:418:0:1000::f000</ADDRESS><PORT>179</PORT><ASN4>2914</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390425913</TIMESTAMP><DATETIME>2014-01-22T21:25:13Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1943442354</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:MP_UNREACH_NLRI optional="true" transitive="false" partial="false" extended="true" attribute_type="15" safi="1"><bgp:MP_NLRI afi="2">2001:67c:118::/48</bgp:MP_NLRI></bgp:MP_UNREACH_NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0025020000000E900F000A000201302001067C0118</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

## Testing new
my $testRib = BGPmon::RIB->new();
is($testRib, undef, "Test BGPmon::RIB with too few arguments.");
$testRib = BGPmon::RIB->new($xml);
is($testRib, undef, "Test BGPmon::RIB with non table_start message.");
$testRib = BGPmon::RIB->new($table_start);
ok(defined($testRib), "Test BGPmon::RIB with sufficient arugments.");

## Testing get error message
my $errorMessage = $testRib->get_error_msg();
is($errorMessage, "No Error. Relax with some tea.", "Test get_error_msgs.");

## Testing get error code
my $errorCode = $testRib->get_error_code();
is($errorCode, 0, "Test get_error_code.");

## Testing get peer address
my $peerAddr = $testRib->get_peer_address();
is($peerAddr, '2001:4810::1', "Test getting peer address");

## Testing get bgpmon monitor address
my $monitorAddr = $testRib->get_monitor_address();
is($monitorAddr, '2001:468:d01:33::80df:3370', "Test getting mointor address");

## Testing flag finished and flag is finished
is($testRib->is_finished(), FALSE, "Testing is finished set to false");
is($testRib->flag_finished(), 0, "Testing setting the finished flag");
is($testRib->is_finished(), TRUE, "Testing is finished set to true");


## Testing ingest_message
my $ingRet = $testRib->ingest_message();
is($ingRet, 1, "Testing ingest message with no xml message");
$ingRet = $testRib->ingest_message($brokenXml);
is($ingRet, 1, "Testing ingest message with incomplete XML message");
$ingRet = $testRib->ingest_message($xml);
is($ingRet, 0, "Testing ingest message with complete XML message");
is($testRib->remove('103.11.119.0/24'), 0, "Testing removing prefix");
$testRib->remove('27.54.183.0/24');
my @prefixes = keys $testRib->{'_prefixes'} ;
is(scalar(@prefixes), 0, "Testing prefixes correctly removed.");
my @aspaths = keys $testRib->{'_as_path'} ;
is(scalar(@aspaths), 0, "Testing as_path correctly removed.");
$ingRet = $testRib->ingest_message($xml6);
#is($testRib->print_table($dump),0,"Testing print table");
is($ingRet, 0, "Testing ingest message with complete XML message with MP_REACH");
$ingRet = $testRib->ingest_message($withXML);
#is($testRib->print_table($dump),0,"Testing print table");
is($ingRet, 0, "Testing ingest message with complete XML withdraw message");
$ingRet = $testRib->ingest_message($unreachXML);
is($ingRet, 0, "Testing ingest message with complete XML UNREACH message");
#is($testRib->print_table($dump),0,"Testing print table");


##Testing latest timestamp
my $tsres = $testRib->get_latest_timestamp();
is($tsres, 1390425913, "Testing latest timestamp");

## testing the process for serializing a table.
## testRib has already been created and has a few messages in it.
$ingRet = $testRib->ingest_message($xml);
#is($testRib->print_table($dump,format=>'txt'),0,"Testing print table");
#is($testRib->print_table($dump),0,"Testing print table");



done_testing();



