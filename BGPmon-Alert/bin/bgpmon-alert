#!/usr/bin/perl
our $VERSION = '0.01';
use strict;
use warnings;
use constant FALSE => 0;
use constant TRUE => 1;
use BGPmon::Log qw(log_init log_fatal log_err log_warn log_notice log_info debug log_close);
use BGPmon::Fetch qw(connect_bgpdata read_xml_message close_connection is_connected);
use BGPmon::Configure;
use BGPmon::Translator::XFB2PerlHash::Simpler;
use BGPmon::Alert;
use List::MoreUtils qw(uniq);
use Time::HiRes qw(gettimeofday);

use Clone qw(clone);

use GraphViz;
use sigtrap 'handler' => \&shutd, 'INT';


use Data::Dumper;


##---- Global Variables
my $progName = $0;
my $exit = FALSE;
my $graph_thread;
$|=1; #flush every time we print

sub shutd{
  print "Stopping $progName.\n";
  $exit = TRUE;
  print "Closing connection.\n";
  close_connection()  if is_connected();
  print "Done.\n";
  exit 0;


}








my $server;
my $port;
my $debug;
my $daemon; #will keep track if the uer wants to have the process as a daemon
my $graph;
#my $graph_refresh_rate;
my $graph_filename;
#share($graph_refresh_rate);
my $graph_radius;
my $stdout;
##--- Variables for Logging ---
#LOG_EMERG	: 0
#LOG_ALERT	: 1
#LOG_CRIT	: 2
#LOG_ERR	: 3
#LOG_WARNING	: 4
#LOG_NOTICE	: 5
#LOG_INFO	: 6
#LOG_DEBUG	: 7

my $logLevel;
my $logFile;

##############################################################################
#                              MAIN PROGRAM
##############################################################################

if(!parseAndCheck()){
  print "Parsing error\n";
  exit 1;
}


if($debug){
#  print "Graph refresh rate: $graph_refresh_rate\n";

}



#moving old log file to .old
if(-e $logFile){
  my $newLogFileName = $logFile.".old";
  my $res = `mv $logFile $newLogFileName`;
}

# Starting the log file
my $logRetVal = log_init(use_syslog => 0,
    log_level => $logLevel,
    log_file => $logFile,
    prog_name => $progName);
if($logRetVal and defined($logFile)) {
  print STDERR "Error initilaizing log.\n";
  exit 1;
}
log_info("bgpmon-alert has started the log file.");




#Connecting to bgpmon-relay instance
log_info("Connecting to source: $server:$port");
#print "Connecting to BGPmon-Relay $server:$port\n" if $debug;
my $retVal = connect_bgpdata($server, $port);
if($retVal != 0){
  #print "Couldn't connect to the BGPmon server.  Aborting.\n";
  log_err("Coudln't connect to BGPmon server.");
  exit 1;
}
#print "Connected to BGPmon-Relay server!\n" if $debug;
log_info("Connected to BGPmon-Relay server.");


#Starting graph thread if wanted
#$graph_thread = threads->create('graph_refresh_thread');



=comment

three layer hash.  prefix->origin as -> peer

if another origin AS appears, possible origin hijack
if another peer sees it, add it to the list - normal announcement but different path

=cut
my %originHash = ();#to detect origin hijacks
my %pathHash = ();#to store path information
my %peerHash = ();

while(is_connected()){

  my $xmlMsg = undef;

  $xmlMsg = read_xml_message();

  next if(!defined($xmlMsg));

  my $res = BGPmon::Translator::XFB2PerlHash::Simpler::parse_xml_msg($xmlMsg);
  if($res){
    log_err("Coudln't parse xml message using BGPmon:Extract.");
    next;
  }

  #updating if graph is wanted
  if($graph or $stdout){
    my @aspath = BGPmon::Translator::XFB2PerlHash::Simpler::extract_aspath();
    if(scalar(@aspath) == 0){
      @aspath = BGPmon::Translator::XFB2PerlHash::Simpler::extract_as4path();
    }
    if(scalar(@aspath) != 0){
      my $numASNs = scalar(@aspath);

      for(my $i = 1; $i < $numASNs; $i+=1){
        my $asn1 = $aspath[$i-1];
        my $asn2 = $aspath[$i];

        my $one :shared = 1;
        #storing the values
        if(!exists($pathHash{$asn1})){
          $pathHash{$asn1} = {};
        }
        if(!exists($pathHash{$asn2})){
          $pathHash{$asn2} = {};
        }
        $pathHash{$asn1}{$asn2} = $one;
        $pathHash{$asn2}{$asn1} = $one;
      }
    }
  }



  #Getting the peer information
  my $peer = BGPmon::Translator::XFB2PerlHash::Simpler::extract_sender_asn();
  my $peerIP = BGPmon::Translator::XFB2PerlHash::Simpler::extract_sender_addr();
  if(!defined($peer) or !defined($peerIP)){
    log_err("Couldn't get the peer's information.");
    next;
  }
  $peerHash{$peer} = $peerIP;

  #Getting Withdraws and taking them out of the announcement table
  my @with_prefs = BGPmon::Translator::XFB2PerlHash::Simpler::extract_withdraw();
  my @unreach_prefs = BGPmon::Translator::XFB2PerlHash::Simpler::extract_mpunreach_nlri();
  my @prefs = (@with_prefs, @unreach_prefs);
  @prefs = uniq(@prefs);
  foreach(@prefs){
    if(exists $originHash{$_}){
      #print "Route $_ withdraw seen at $peer.\n";
      log_info("Route $_ withdraw seen at $peer.");
      delete $originHash{$_}; #TODO do a full delete on all of the items within the hash
    }
  }



  #Getting announcements and putting them into the table.
  my @nlri_prefs = BGPmon::Translator::XFB2PerlHash::Simpler::extract_nlri();
  my @reach_nlri_prefs = BGPmon::Translator::XFB2PerlHash::Simpler::extract_mpreach_nlri();
  my @anns = (@nlri_prefs, @reach_nlri_prefs);
  @anns = uniq(@anns);

  my $numAnn = scalar(@anns);
  next if $numAnn < 1;


  #Getting origin ASN
  my $origin = BGPmon::Translator::XFB2PerlHash::Simpler::extract_origin();
  if(!defined($origin)){
    log_err("Coudln't get the origin's information.");
    next;
  }


  #checking in hash
  foreach(@anns){
    if(exists $originHash{$_}){ #seeing if the prefix exists 

      my @oldOrigins = keys $originHash{$_};
      my $numOrigins = scalar(@oldOrigins);


      if($numOrigins == 1){
        my $oldOrig = $oldOrigins[0];
        if($oldOrig ne $origin){ #origin attack
          $originHash{$_}{$origin}{$peer} = 1;
          handleOriginAttack($_, $origin, $peer);
        }
        else{ #maybe a new peer
          #print "New peer, $peer, seeing announcement for $_ for origin $origin\n";
          $originHash{$_}{$origin}{$peer} = 1;
        }
      }
      else{ #already have an origin hijack, possibly update peer information now.
        if(exists $originHash{$_}{$origin}){ #maybe new peer and can send an update
          $originHash{$_}{$origin}{$peer} = 1;
          handleOriginAttackUpdate($_, $origin, $peer);

        }
        else{ #another origin hijack
          $originHash{$_}{$origin}{$peer} = 1;
          handleOriginAttack($_, $origin, $peer);
        }
      }

      
    }
    else{
      #print "New announcement for $_ announced from $origin, seen from peer $peer\n";
      log_err("New announcement for $_ announced from $origin, seen from peer $peer");
      
      $originHash{$_}{$origin}{$peer} = 1;

    }

  }
}









sub handleWithdraw{

}

sub handleNewAnn{


}



sub handleOriginAttack{

  my $prefix = shift;
  my $origin = shift;
  my $peer = shift;

  my @origins = keys $originHash{$prefix};

  log_warn("POSSIBLE ORIGN HIJACK: $prefix ANNOUNCED FROM $origin SEEN FROM $peer;ORIGIN(S): @origins");
  #print "POSSIBLE ORIGN HIJACK: $prefix ANNOUNCED FROM $origin SEEN FROM $peer;ORIGIN(S): @origins\n";
  
  updateAlert($prefix, $origin, $peer) if $graph or $stdout;

}


sub handleOriginAttackUpdate{

  my $prefix = shift;
  my $origin = shift;
  my $peer = shift;

  my @origins = keys $originHash{$prefix};

  #print "UPDATE TO ORIGIN HIJACK: $prefix ANNOUNCED FROM $origin SEEN FROM $peer;ORIGIN(S): @origins\n";
  log_warn("UPDATE TO ORIGN HIJACK: $prefix ANNOUNCED FROM $origin SEEN FROM $peer;ORIGIN(S): @origins");

  updateAlert($prefix, $origin, $peer) if $graph or $stdout;
}




sub parseAndCheck{

  my @params = (
      {
      Name  => BGPmon::Configure::CONFIG_FILE_PARAMETER_NAME,
      Type  => BGPmon::Configure::FILE,
      Default => undef,
      Description => "This is the configuration filename.",
      },
      {
      Name => "relay",
      Type => BGPmon::Configure::ADDRESS,
      Default => undef,
      Description => "BGPmon-Relay server IP address",
      },
      {
      Name => "port",
      Type => BGPmon::Configure::PORT,
      Default => undef,
      Description => "BGPmon server port number",
      },
      {
      Name => "log_file",
      Type => BGPmon::Configure::FILE,
      Default => "bgpmon-alert.log",
      Description => "This is the location the log file will be saved",
      },

      {
        Name => "log_level",
        Type => BGPmon::Configure::UNSIGNED_INT,
        Default => 7,
        Description => "This is how verbose the user wants the log to be",
      },

      {
        Name => "debug",
        Type => BGPmon::Configure::BOOLEAN,
        Default => FALSE,
        Description => "This is for debugging purposes",
      },
      {
        Name => "graph",
        Type => BGPmon::Configure::BOOLEAN,
        Default => FALSE,
        Description => "Set to true if the user would like an AS graph presented.",
      },
#=comment
#     {
#       Name => "graph_refresh",
#       Type => BGPmon::Configure::UNSIGNED_INT,
#       Default => 60,
#       Description => "The number of seconds in between AS graph refresh rates.",
#     },
#=cut
      {
        Name => "graph_filename",
        Type => BGPmon::Configure::STRING,
        Default => "graph.png",
        Description => "The name of the file to save the graph to (must be a png).",
      },

      {
        Name => "graph_radius",
        Type => BGPmon::Configure::UNSIGNED_INT,
        Default => 0,
        Description => "The radius to be used in the graph from the origin or attacking AS.",
      },
      {
        Name => "stdout_print",
        Type => BGPmon::Configure::BOOLEAN,
        Default => FALSE,
        Description => "If the user would like to have warnings printed to stdout for dynamic graph generation.",
      }

  );


#Checking that everything parsed correctly
  if(BGPmon::Configure::configure(@params) ) {
    my $code = BGPmon::Configure::get_error_code("configure");
    my $msg = BGPmon::Configure::get_error_message("configure");
    print "$code: $msg\n";
    return FALSE;
  }

#Moving all of the variables into the variables from previous version
  $server = BGPmon::Configure::parameter_value("relay");
  $port = BGPmon::Configure::parameter_value("port");
#$relay = BGPmon::Configure::parameter_value("server_relay");
  $debug = BGPmon::Configure::parameter_value("debug");
  $logFile = BGPmon::Configure::parameter_value("log_file");
  $logLevel = BGPmon::Configure::parameter_value("log_level");
  $graph = BGPmon::Configure::parameter_value("graph");
# $graph_refresh_rate = BGPmon::Configure::parameter_value("graph_refresh");
  $graph_filename = BGPmon::Configure::parameter_value("graph_filename");
  $graph_radius = BGPmon::Configure::parameter_value("graph_radius");
  $stdout = BGPmon::Configure::parameter_value("stdout_print");



#Making sure we have all the server information needed.
  if(!defined($server) or !defined($port)){
    print "You must specify the server information.\n";
    return FALSE;
  }
#$relay = FALSE if !defined($relay);

#making sure the filename ends with a .png
  if($graph_filename !~ /\.png$/){
    print "You must specify the graph filename to end with a .png\n";
    return FALSE;
  }

  return TRUE;

}



sub printStdout{
  my $prefix = shift;
  my $attacker = shift;
  my $peer = shift;

  my @origins = keys $originHash{$prefix};

}

sub updateAlert{

  my $prefix = shift;
  my $attacker = shift;
  my $peer = shift;

  #making sure we have to do the work
  if(!$stdout and !$graph){
    return 0;
  }


  my @origins = keys $originHash{$prefix};


  #Collecting nodes for the graph
  my %allNodes = ();
  foreach(keys %pathHash){
    $allNodes{$_} = 1;
    foreach(keys $pathHash{$_}){
      $allNodes{$_} = 1;
    }
  }


  # Getting the list of past origins and removing them from the allNodes
  my %pastOrig = ();
  $pastOrig{$_} = 1 foreach(@origins);
  delete $allNodes{$_} foreach(@origins);

  #Removing the attacker from the list of origins
  delete $pastOrig{$attacker};
  
  # Getting a list of past peers which have seen this and removing from allNodes
  my %pastPeers = ();
  $pastPeers{$_} = 1 foreach(keys $originHash{$prefix}{$attacker});
  delete $pastPeers{$peer};

  #making a list of all other peers which haven't seen this announcement
  my %allPeers =  %{ clone (\%peerHash) };
  delete $allPeers{$_} foreach(keys %pastPeers);
  delete $allPeers{$peer};

  if($stdout){
    #format prefix|past origins|attacking origin| peers infected | peers not infected

    #adding prefix
    my $toPrint = "$prefix|";

    #adding past origins
    my @pastOs = keys %pastOrig;
    my $numPastOrgs = scalar(@pastOs);
    for(my $i = 0; $i < $numPastOrgs; $i += 1){
      $toPrint = $toPrint.$pastOs[$i];
      if($i + 1 != $numPastOrgs){
        $toPrint .= ",";
      }
      else{
        $toPrint .= "|";
      }
    }

    #adding attacker
    $toPrint .= "$attacker|";

    #adding infected peers
    my @infected = keys %pastPeers;
    push(@infected, $peer);
    my $numPastInfect = scalar(@infected);
    for(my $i = 0; $i < $numPastInfect; $i += 1){
      $toPrint = $toPrint.$infected[$i]."-".$peerHash{$infected[$i]};
      if($i + 1 != $numPastInfect){
        $toPrint .= ",";
      }
      else{
        $toPrint .= "|";
      }
    }


    #adding clean peers
    my @cleanPeers = keys %allPeers;
    my $numClean = scalar(@cleanPeers);
    for(my $i = 0; $i < $numClean; $i += 1){
      $toPrint = $toPrint.$cleanPeers[$i].'-'.$peerHash{$cleanPeers[$i]};
      if($i + 1 != $numClean){
        $toPrint .= ",";
      }
    }


    #printing
    print "$toPrint\n";


  }


  if($graph){
    #Getting time and making the new filename
    my $retval = time();
    my $local_time = gmtime( $retval);
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime(time);
    $year = $year + 1900;
    $mon += 1;
    $mon = "0".$mon if length($mon) < 2;
    $mday = "0".$mday if length($mday) < 2;
    my $time = $year.$mon.$mday."$hour:$min:$sec";
    my $newGraphFileName = $prefix."-".$time.".png";
    $newGraphFileName =~ s/\//_/g;
    $graph_filename = $newGraphFileName;

    #Starting the graph
    my $g = GraphViz->new(layout=>'circo', directed => 0);
  

    #adding all of the past origins and the attacking orign
    $g->add_node($_, shape => 'square', color => 'green') foreach(keys %pastOrig);
    $g->add_node($attacker, shape => 'triangle', color => 'red');
  

    #adding past peers and the current peer
    $g->add_node($_, shape => 'octagon', color => 'magenta') foreach(keys %pastPeers);
    $g->add_node($peer, shape => 'hexagon', color => 'magenta');

    #adding all other peers
    $g->add_node($_, shape => 'hexagon', color => 'blue') foreach(keys %allPeers);

    #adding all other nodes (what is left of allNodes)
    $g->add_node($_) foreach(keys %allNodes);


    #Adding edges in between nodes
    my %donehash = ();
    foreach(keys %pathHash){
      my $key1 = $_;
      foreach(keys $pathHash{$key1}){
        my $key2 = $_;

        if($key1 < $key2){
          next if(exists $donehash{$key1}{$key2});
          $donehash{$key1}{$key2} = 1;

        }
        elsif($key2 < $key1){
          next if(exists $donehash{$key2}{$key1});
          $donehash{$key2}{$key1} = 1;

        }
        else{
          next;
        }

        $g->add_edge($key1 => $_);
      }
    }

    #Saving the file
    $g->as_png($graph_filename);
    log_info("Saved graph to $graph_filename");
    #print "Graph saved to $graph_filename\n";
  }
}

