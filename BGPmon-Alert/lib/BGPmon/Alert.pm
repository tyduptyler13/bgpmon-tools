package BGPmon::Alert;
use strict;
use warnings;
use constant FALSE => 0;
use constant TRUE => 1;
use BGPmon::Translator::XFB2PerlHash;
use Data::Dumper;

BEGIN{
  require Exporter;
  our $VERSION = '0.01';
  our $AUTOLOAD;
  our @ISA = qw(Exporter);
  our @EXPORT_OK = qw(init parse_config_filter get_error_msg get_error_code);
}


my $progName = $0;


# Variables to hold error codes and messages
my %error_code;
my %error_msg;

use constant NO_ERROR_CODE => 0;
use constant NO_ERROR_MSG => 'No Error. Relax with some tea.';

#Error codes for opening the configuration file.
use constant UNOPANABLE_CONFIG_FILE => 720;
use constant UNOPANABLE_CONFIG_FILE_MSG => 'Invalid filename given for filter config file.';
#Error codes for parsing the configuration file.
use constant INVALID_IPV4_CONFIG => 730;
use constant INVALID_IPV4_CONFIG_MSG => "Invalid IPv4 given in config file.";
use constant INVALID_IPV6_CONFIG => 731;
use constant INVALID_IPV6_CONFIG_MSG => "Invalid IPv6 given in config file.";
use constant INVALID_AS_CONFIG => 732;
use constant INVALID_AS_CONFIG_MSG => "Invalid AS given in config file.";
use constant UNKNOWN_CONFIG => 733;
use constant UNKNOWN_CONFIG_MSG => "Invalid line in config file.";


sub init{
  my $fname = 'init';
  $error_code{$fname} = NO_ERROR_CODE;
  $error_msg{$fname} = NO_ERROR_MSG;

  return 0;
}


=head2 get_error_msg

Will return the error message of the given function name.

Input:  A string that contains the function name where an error occured.

Output: The message which represents the error stored from that function.

=cut
sub get_error_msg{
  my $str = shift;
  my $fname = 'get_error_msg';
  my $toReturn = $error_msg{$str};
  return $toReturn;
}

=head2 get_error_code

Will return the error code of the given function name.

Input:  A string that represents the function name where an error occured.

Output: The code which represents the error stored from that function.

=cut
sub get_error_code{
  my $str = shift;
  my $fname = 'get_error_code';
  my $toReturn = $error_code{$str};
  return $toReturn;
}


sub parse_config_file{
  return undef; ##TODO
}


1;
