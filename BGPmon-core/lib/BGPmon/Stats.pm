package BGPmon::Stats;

our $VERSION = '1.0.3';

use strict;
use warnings;

use BGPmon::Message; #We need messages.

use constant FALSE => 0;
use constant TRUE => 1;

=head1 NAME

BGPmon::Stat - A container for statistics attributes of any given BGPmon live or archive
stream.

=head1 EXAMPLE

	use BGPmon::Stats;

	my $stat = new BGPmon::Stats();

	#You would have some code that collects a message here
	
	$stat->injestMessage($message);

	print "We have seen $stat->getTotal() messages\n";

=head1 METHODS

=head2 new

This creates a new stat object.

    my $stat = new BGPmon::Stats(name=>"Standard", description=>"Generic stat");

Future: This will accept an optional filter to only collect data on a specific type of message.

=cut

#STATIC varibale for id. Don't remove.
my $id = 1;

sub new {

	my $class = shift;

	my $self = {
		name => 'Anonymous',
		description => 'A stat object.',
       		id => $id++
    	};

    	#Totals of all countable types go here.
    	$self->{totals} = {
        	total		=> 0,
		withdraw	=> 0,
		mpReach		=> 0,
		mpUnreach	=> 0
    	};

	#Hash for tracking per peer information.
	$self->{peers} = {};

	my %args = @_;

	if (exists($args{name}) and defined($args{name})){
		$self->{name} = $args{name};
	}

	if (exists($args{description}) and defined($args{description})){
		$self->{description} = $args{'description'};
	}

	bless $self, $class;
	return $self;

}

=head2 reset

This will reset the data values inside of the stat. (Both totals and peers)

	$stat->reset();

=cut

sub reset {

	my $self = shift;

	$self->{peers} = {}; #Delete the peers.

	foreach (keys $self->{totals}){
		$self->{totals}{$_} = 0;
	}

}

=head2 name

Returns the name.

=cut

sub name {

    my $self = shift;

    return $self->{name};

}

=head2 description

Returns the description.

=cut

sub description {

    my $self = shift;

    return $self->{description};

}

=head2 id

Returns the id.

=cut

sub id {

    my $self = shift;

    return $self->{id};

}

=head2 Set

Sets attributes of the stat object.

Example:

	$stat->set('name', 'Awesome stat');

Available keys:

* name - Name of the stat.
* description - Description of the stat.
* id - An identifier of the stat.

Do not set totals or peers keys. This will break the object.

=cut

sub set {

	my ($self, $key, $value) = @_;

	$self->{$key} = $value;

}

=head2 injestMessage

This accepts a BGPmon::Message object and parses out the data it wants based on the filter
and the available stats possible within this object.

    $stat->injestMessage($message);

=cut

sub injestMessage {

	my ( $self, $message ) = @_;

	#Generic messages.
	$self->{totals}{total}++;
	
	#Requires parsed messages.
	$self->{totals}{nlri} += $message->get_nlris().length;
	$self->{totals}{withdraw} += $message->get_withdraws().length;
	$self->{totals}{mpReach} += $message->get_mp_nlris().length;
	$self->{totals}{mpUnreach} += $message->get_mp_unreach_nlris().length;

	#Requires using xml nodes instead of functions.
	#This gets the peer address for tracking per peer data.
	my $peer = $message->get_byid('source_address');
	
    #Check that we even know where this came from.
    #The messages don't always have a source.
    if (not defined $peer){
        return;
    }

	#Check if the peer is defined yet in the list.
	if (not exists $self->{peers}{$peer}){
		$self->{peers}{$peer} = 0;
	}

	$self->{peers}{$peer}++;

}

=head2 get

This gets internal values based on injested messages.

Available keys:

* total - Contains information of how many messages have been read.
* withdraw - Total withdraw messages. (A bgp message can contain 0-many withdraw messages)
* nlri - Total nlri messages. (A bgp message can contain 0-many nlri messages)
* mpReach - Total mp-reach messages. (0-many per message)
* mpUnreach - Total mp-unreach messages. (0-many per message)

Usage:

    my $data = $stat->get($key);

=cut

sub get {

	my ($self, $key) = @_;

	return $self->{totals}{$key};

}

=head2 getData

Returns the hash of the data. Currently this contains only elements that are considered
countable for easier automation and compatibility with later versions.

	my $data = $stat->getData();

=cut

sub getData {
	
	my $self = shift;

	return $self->{totals};

}

=head2 getPeers

This returns a hash of all of the peers that have been seen in a hash.

The keys are the ips that identify the peer. The values are the number of
messages that have been seen on the peer. This may change in the future.

=cut

sub getPeers {

	my $self = shift;

	return $self->{peers};

}

