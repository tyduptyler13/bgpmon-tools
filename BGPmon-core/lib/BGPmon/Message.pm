package BGPmon::Message;

use 5.14.0;

use strict;
use warnings;

use XML::LibXML;
use XML::Compile::Cache;
use Time::HiRes;
our $VERSION = '3.0';


#use Data::Dumper;
=head1 NAME

BGPmon::Message

This modules reads and validates XML messages from aBGPmon source. It will
extract information from all fields and provides an interface to access
that information.

=head1 SYNOPSIS

use BGPmon::Message;

my $xml_stirng = '<BGP_MONITOR_MESSAGE>...';

#Reading, validating, and parsing XML message

my $msg = BGPmon::Message->new('string'=>$xml_string);

say $msg->get_timestamp();

my @aspath = $msg->get_aspath();

say $_ foreach(@aspath);

=head1 SUBROUTINES/METHODS

=cut





## we only need one cache and it can be used repeatedly
my $cache = XML::Compile::Cache->new(allow_undeclared => 1);
## this function will add BGPmon/Message/xsd to the search path
## for schemas
$cache->addSchemaDirs(__FILE__);
$cache->importDefinitions([
    'bgp_monitor_2_00.xsd',
    'network_elements_2_00.xsd',
    'xfb_2_00.xsd'
    ]);

#my $reader = $cache->compile(READER =>
#        '{urn:ietf:params:xml:ns:bgp_monitor}BGP_MONITOR_MESSAGE',
#        #validation => $validateFlag);
#        );

my $message_ns = "urn:ietf:params:xml:ns:bgp_monitor";
my $update_ns = "urn:ietf:params:xml:ns:xfb";
my $write;
eval {
  $write = $cache->compile(WRITER =>
      '{urn:ietf:params:xml:ns:bgp_monitor}BGP_MONITOR_MESSAGE',
      use_default_namespace=>0,
      elements_qualified => 1,
      include_namespaces=>1,
      prefixes => [ bgp => $update_ns, msg => $message_ns ],
      hook=>{ID=>'ORIGIN',before =>\&BGPmon::Message::_fix_boolean}
      );
} or do {
  $write = undef;
};

# ELA, TESTING ALTERNATIVES FOR XML PARSING
my $xmlschema;
eval {
  my $fullpath="";
  foreach my $path (@INC) {
      $fullpath=$path."/BGPmon/Message/xsd/bgp_monitor_2_00.xsd";
      if (-e $fullpath) {
        $xmlschema = XML::LibXML::Schema->new( location => $fullpath );
      }
  }
};
my $xmlparser = XML::LibXML->new();
my $xpc = XML::LibXML::XPathContext->new();
$xpc->registerNs("bgpm", "urn:ietf:params:xml:ns:bgp_monitor");
$xpc->registerNs("bgp", "urn:ietf:params:xml:ns:xfb");

###############################################################################
############## GLOBAL STRUCTURES ##### ########################################
###############################################################################
my %nodemap=qw(
	source_address bgpm:SOURCE/bgpm:ADDRESS/text()
	source_asn bgpm:SOURCE/*[contains(local-name(),'ASN')]/text()
	source_asn_4 bgpm:SOURCE/bgpm:ASN4/text()
	source_asn_2 bgpm:SOURCE/bgpm:ASN2/text()
	source_port bgpm:SOURCE/bgpm:PORT/text()
	dest_address bgpm:DEST/bgpm:ADDRESS/text()
	dest_asn bgpm:DEST/*[contains(local-name(),'ASN')]/text()
	dest_asn_4 bgpm:DEST/bgpm:ASN4/text()
	dest_asn_2 bgpm:DEST/bgpm:ASN2/text()
	dest_port bgpm:DEST/bgpm:PORT/text()
	monitor_address bgpm:MONITOR/bgpm:ADDRESS/text()
	monitor_asn bgpm:MONITOR/*[contains(local-name(),'ASN')]/text()
	monitor_asn_4 bgpm:MONITOR/bgpm:ASN4/text()
	monitor_asn_2 bgpm:MONITOR/bgpm:ASN2/text()
	monitor_port bgpm:MONITOR/bgpm:PORT/text()
	relay_address bgpm:SOURCE/bgpm:ADDRESS/text()
	relay_asn bgpm:SOURCE/*[contains(local-name(),'ASN')]/text()
	relay_asn_4 bgpm:SOURCE/bgpm:ASN4/text()
	relay_asn_2 bgpm:SOURCE/bgpm:ASN2/text()
	timestamp bgpm:OBSERVED_TIME/bgpm:TIMESTAMP/text()
	datetime bgpm:OBSERVED_TIME/bgpm:DATETIME/text()
	sequence_number bgpm:SEQUENCE_NUMBER/text()
	collection_method bgpm:COLLECTION
	status bgpm:STATUS/bgpm:TYPE
	octets bgpm:OCTET_MESSAGE/text()
        metadata_nodes bgpm:METADATA
        update bgp:UPDATE
        nlri_nodes bgp:UPDATE/bgp:NLRI
        nlris bgp:UPDATE/bgp:NLRI/text()
        withdraw_node bgp:UPDATE/bgp:WITHDRAW
        withdraws bgp:UPDATE/bgp:WITHDRAW/text()
        bgp_origin bgp:UPDATE/bgp:ORIGIN/text()
        aspath bgp:UPDATE/bgp:AS_PATH/bgp:AS_SEQUENCE/*[contains(local-name(),'ASN')]/text()
        aspath_4 bgp:UPDATE/bgp:AS_PATH/bgp:AS_SEQUENCE/ASN4/text()
        aspath_2 bgp:UPDATE/bgp:AS_PATH/bgp:AS_SEQUENCE/ASN2/text()
        nexthop bgp:UPDATE/bgp:NEXT_HOP/text()
        med bgp:UPDATE/bgp:MULTI_EXIT_DISC/text()
        atomic_agg bgp:UPDATE/bgp:ATOMIC_AGGREGATE
        aggregator bgp:UPDATE/bgp:AGGREGATOR
        aggregator_node bgp:UPDATE/bgp:AGGREGATOR
        aggregator_address bgp:UPDATE/bgp:AGGREGATOR/bgp:IPv4_ADDRESS/text()
        aggregator_asn bgp:UPDATE/bgp:AGGREGATOR/*[contains(local-name(),'ASN')]/text()
        aggregator_asn_4 bgp:UPDATE/bgp:AGGREGATOR/bgp:ASN4/text()
        aggregator_asn_2 bgp:UPDATE/bgp:AGGREGATOR/bgp:ASN2/text()
        community_nodes bgp:UPDATE/bgp:COMMUNITY
        mp_reach_node bgp:UPDATE/bgp:MP_REACH_NLRI
        mp_safi bgp:UPDATE/bgp:MP_REACH_NLRI/@safi
        mp_nlri_nodes bgp:UPDATE/bgp:MP_REACH_NLRI/@safi
        mp_nlri_nexthop bgp:UPDATE/bgp:MP_REACH_NLRI/bgp:MP_NEXT_HOP
        mp_nlri bgp:UPDATE/bgp:MP_REACH_NLRI/bgp:MP_NLRI
        mp_nlris bgp:UPDATE/bgp:MP_REACH_NLRI/bgp:MP_NLRI/text()
        mp_reach_next_hop_node bgp:UPDATE/bgp:MP_REACH_NLRI/bgp:MP_NEXT_HOP
        mp_unreach_node bgp:UPDATE/bgp:MP_UNREACH_NLRI
        mp_unreach_nlris bgp:UPDATE/bgp:MP_UNREACH_NLRI/bgp:MP_NLRI/text()
        mp_unreach_safi bgp:UPDATE/bgp:MP_UNREACH_NLRI/@safi
        ext_comm_nodes bgp:UPDATE/bgp:EXTENDED_COMMUNITIES
        as4_path bgp:UPDATE/bgp:AS4_PATH/*[contains(local-name(),'SEQUENCE')]/*[contains(local-name(),'ASN')]/text()
        as4_path_4 bgp:UPDATE/bgp:AS4_PATH/bgp:AS4_SEQUENCE/bgp:ASN4/text()
        as4_path_2 bgp:UPDATE/bgp:AS4_PATH/bgp:AS4_SEQUENCE/bgp:ASN2/text()
        as4_aggregator_node bgp:UPDATE/bgp:AS4_AGGREGATOR
        as4_aggregator_address bgp:UPDATE/bgp:AS4_AGGREGATOR/bgp:IPv4_ADDRESS/text()
        as4_aggregator_asn bgp:UPDATE/bgp:AS4_AGGREGATOR/*[contains(local-name(),'ASN')]/text()
        as4_aggregator_asn_4 bgp:UPDATE/bgp:AS4_AGGREGATOR/bgp:ASN4/text()
        as4_aggregator_asn_2 bgp:UPDATE/bgp:AS4_AGGREGATOR/bgp:ASN2/text()
);


###############################################################################
############## INTERNALS FOR THE CLASS ########################################
###############################################################################
=head2 new

Creates a new BGPmon::Message object.  Will read in the XML string,
validate its contents, and then parse it to extract information later.

Input: 'string' => xml string - message you wish to parse
       'RIB'    => 1/0        - flag to say if this is an update or RIB message

Output: undef on error
        hash refrence to object on success
=cut
sub new{
  my $class = shift;
  my (%args) = @_;
  my $hash;


  my $validateFlag = 0;
  if(exists($args{'validation'})){
    $validateFlag = $args{'validation'};
  }

  ## check to see what kind of input we got
  if(exists($args{'string'})){
    eval {
      # ELA, USE LibXML FOR PARSING
      #$hash = $reader->($args{'string'});
      my $doc = $xmlparser->parse_string($args{'string'});
      unless ($validateFlag == 0) {
	eval {
      		$xmlschema->validate( $doc );
	};
	print $@;
      }
      $hash = $doc->documentElement();
    } or do {
      return undef;
    };
  }else{
    ## start a new message
    $hash = {};
  }


  #LAWRENCE ADDITION - MARKING FOR RIBS
  my $is_rib_msg = 0;
  if(exists($args{'RIB'})){
    $is_rib_msg = $args{'RIB'} ;
  }


  my $self = bless {
    _data => $hash,
    rib_msg => $is_rib_msg,
  }, $class;
  return $self;
}

=head2 remove_node

Removes a node of a given path if it exists.
E.g., remove_node('/BGP_MONITOR_NODE/SOURCE)

Input: path - the path of the node you wish to remove

Output: undef if the node wasn't found
        hash refrence to object which was removed on success
=cut
sub remove_node{
  my $self = shift;
  my $path = shift;

  return $self->get_node($path,remove=>1);
}

=head2 get_node

Returns a node of a given path if it exists.
E.g., get_node('/BGP_MONITOR_NODE/SOURCE)

Input: path - the path of the node you wish to gain access to

Output: undef if the node wasn't found
        hash refrence to node if it was found
=cut

# ELA, USE LibXML FOR PARSING
sub get_node{
  my $self = shift;
  my $path = shift;
  my %opts = @_;

  my $ref = $self->{'_data'};
  if(undef($ref)){
      return undef;
  }

  my @nodes = $xpc->findnodes($path, $self->{'_data'});

  my @list=();
  push(@list, $self->build_map($_,%opts)) foreach (@nodes);

  if (scalar @list == 0) {
      return undef;
  }

  if (scalar @list > 1) {
    return @list;
  }

  my $mapref = $list[0];

  if (ref($mapref) eq "HASH" && !keys(%{$list[0]})) {
      return undef;
  }

  if (ref($mapref) eq "HASH" && keys(%{$mapref}) == 1) {
      my @list = values(%{$mapref});
      return $list[0];
  }

  return $mapref;
}


=head2 build_map

Returns a node value or a map

Input: Node

Output: text for a text node or attribute
        hash map for an internal node
=cut
# ELA, USE LibXML FOR PARSING
sub build_map{
  my $self = shift;
  my $elem = shift;
  my %opts = @_;

  my %map = ();

  if(exists($opts{'remove'}) && $opts{'remove'}){
    my $parent = $elem->parentNode;
    $elem = $parent->removeChild($elem);
  }

  if ($elem->nodeType == XML::LibXML::XML_TEXT_NODE
      || $elem->nodeType == XML::LibXML::XML_ATTRIBUTE_NODE){
      return $elem->nodeValue;
  }

  if ($elem->nodeType == XML::LibXML::XML_ELEMENT_NODE
     && !$elem->hasChildNodes()){
     $map{$_->nodeName}=$_->nodeValue foreach ($elem->attributes);
     return \%map;
  }

  if ($elem->nodeType == XML::LibXML::XML_ELEMENT_NODE
     && $elem->hasChildNodes()){
     if (!$elem->hasAttributes()
        && $elem->firstChild->nodeType == XML::LibXML::XML_TEXT_NODE) {
        return $elem->firstChild->nodeValue;
     }
     if ($elem->hasAttributes()
        && $elem->firstChild->nodeType == XML::LibXML::XML_TEXT_NODE) {
        $map{'_'} = $elem->firstChild->nodeValue;
        $map{$_->nodeName}=$_->nodeValue foreach ($elem->attributes);
        return \%map;
     }

     if (length($elem->prefix)) {
        my $readerName="{".$elem->namespaceURI()."}".substr($elem->nodeName, length($elem->prefix)+1, length($elem->nodeName));
        my $reader2 = $cache->reader($readerName);
        return  $reader2->($elem);
     }

     my %tchildren = ();
     foreach ($elem->childNodes) {
        my $cname = "cho_".$_->nodeName;
        my $carrayref;
        if (!exists($tchildren{$cname})) {my @carray = (); $carrayref = \@carray; $tchildren{$cname} = $carrayref;}
        $carrayref = $tchildren{$cname};
        push(@{$carrayref},$self->build_map($_));
     }
     while (my ($key,$value) = each(%tchildren)){
        if (@{$value} == 1) {$map{substr($key,4,100)}=@{$value}[0];}
        else {$map{$key}=$value;}
     }
     $map{$_->nodeName}=$_->nodeValue foreach ($elem->attributes);
     return \%map;
  }

  return \%map;
}

sub get_node_orig{
  my $self = shift;
  my $path = shift;
  my %opts = @_;

  $path =~ s/^\///;
  my @elements = split /\//,$path;
  shift @elements;
  if(exists($opts{'ignore_ns'})){
    my $ref = $self->{'_data'};
    foreach my $elem (@elements){
      my $found = 0;
      foreach my $candidates (keys %{$ref}){
        if($candidates =~ /\}$elem\$/){
          $ref = $ref->{$candidates};
          $found = 1; last;
        }
      }
      if(!defined($ref) || !$found){
        return undef;
      }
    }
    return $ref;
  }
  my $ref = $self->{'_data'};
  my ($elem,$old_ref);
  foreach(@elements){
    $elem = $_;
    $old_ref = $ref;
    $ref = $ref->{$elem};
    if(!defined($ref)){
      return undef;
    }
  }
  if(exists($opts{'remove'}) && $opts{'remove'}){
    return delete $old_ref->{$elem};
  }
  return $old_ref->{$elem};
}

=head2 get_att

Looks for a specific attribute in the UPDATE section of a BGP Update message.
E.g., get_att('attribute' => 'AS4_AGGREGATOR')

Input: 'attribute' => (name of attribute) - the path of the node you wish to remove

Output: undef if the node wasn't found
        hash refrence to attribute node if it was found
=cut
# ELA, USE LibXML FOR PARSING
# we do not need it anymore, remove
sub get_att{
  my $self = shift;
  my %opts = @_;

  #Making sure we've been given an attribute
  return undef if(!exists($opts{'attribute'}));

  #Getting attribute we wish to find
  my $att = $opts{'attribute'};

  #Grabbing all of the attributes in the Update Section
  my $atts = $self->get_node('/BGP_MONITOR_MESSAGE/UPDATE/cho_ORIGIN/');

  #Making sure we have an Update section
  return undef if !defined($atts);

  #Looking over each attribute to see if it matches the one we're looking for
  foreach(@{$atts}){
    #found!
    if(exists($_->{$att})){
      return $_->{$att};
    }
  }
  return undef;
}


#######
# _fix_boolean
#
#
#
# Created by Cathie
# fixes the boolean of all path attributes to use the "true" and "false"
# instead of just 1 or 0 or "1" or "0"
######
sub _fix_boolean{
  my ($doc, $values, $path, $tag, $r) = @_;
  if($path =~ /ORIGIN$/ || $path =~ /AS_PATH$/ || $path =~ /NEXT_HOP$/ ||
      $path =~ /MULTI_EXIT_DISC$/ || $path=~ /LOCAL_PREF$/ ||
      $path =~ /ATOMIC_AGGREGATE$/ || $path =~ /AGGREGATOR$/ ||
      $path=~/COMMUNITY$/||$path =~ /ORIGINATOR_ID$/||$path =~ /CLUSTER_LIST$/||
      $path =~ /EXTENDED_COMMUNITIES$/ || $path =~ /AS4_PATH$/ ||
      $path =~ /MP_REACH_NLRI$/ || $path =~ /MP_UNREACH_NLRI$/ ||
      $path =~ /AS4_AGGREGATOR$/ || $path =~ /UNKNOWN_ATTRIBUTE$/){

      #Lawrence Efficienty Edit - will check to see if these properties exist
      #for the path attributes.  #Changes them to a string to see if they're
      #set properly; if not, changes it to an int and then changes it to
      #the correct string.  Note that we don't know if the input for these
      #are 'true', 'false', '0', '1', 0, or 1.
      my @values = ('optional', 'partial', 'extended', 'transitive');
      foreach(@values){
        if(exists($values->{$_})){
          my $op = $values->{$_};
          if(!("$op" eq "true" || "$op" eq "false")){
            $op = $op + 0;
            if($op == 1){
              $values->{$_} = "true";
            }elsif($op == 0){
              $values->{$_} = "false";
            }
          }
        }
      }
  }
  return $values;
}

=head2 to_txt_string

Will return a hash with bgpdump representation of the BGP message if it's an
update message.  The hash is keyed by a string representing the SAFI, and
it resolves to the string to print.

E.g., $ret->{'1'} gives the string to print for unicast
$ret->{'2'} gives the string for multicast

Note that neither key need exist

Input: None

Output: undef if the BGP message isn't an update message
        hashref for strings in BGPdump format if sucessful.
TODO
=cut
sub to_txt_string{
  my $self = shift;

  #Making sure we have an update message
  if(!defined($self->get_node('bgp:UPDATE'))){
    return "";
  }
  #Choosing the correct string starter
  my $start_str;
  if($self->{'rib_msg'}){
    $start_str =  'TABLE_DUMP2';
  }else{
    $start_str = 'BGP4MP';
  }

  #String we're going to return
  my $output = "";

  #Safis for later use
  my $mpsafi = $self->get_mp_safi();
  $mpsafi = '1' if !defined($mpsafi);
  my $mpunsafi = $self->get_mp_unreach_safi();
  $mpunsafi = '1' if !defined($mpunsafi);

  ##Gathering announcements for Unicast
  my @uanns = ();
  #Adding nlris
  my @nlris = $self->get_nlris();
  push(@uanns, @nlris);
  #adding mp_reach_nlri
  if($mpsafi eq '1'){
    my @mpnlris = $self->get_mp_nlris();
    push(@uanns, @mpnlris);
  }

  ##Gathering withdraws for Unicast
  my @uwiths = ();
  #adding withdraws
  my @withdraws = $self->get_withdraws();
  push(@uwiths, @withdraws);
  #adding mp_unreach_nlri
  if($mpunsafi eq '1'){
    my @unnlris = $self->get_mp_unreach_nlris();
    push(@uwiths, @unnlris);
  }


  ##Gathering announcements for Multicast
  my @manns = ();
  if($mpsafi eq '2'){
    push @manns, $self->get_mp_nlris();
  }

  ##Gathering withdraws for Multicast
  my @mwiths = ();
  if($mpunsafi eq '2'){
    push @mwiths, $self->get_mp_unreach_nlris();
  }


  #Preparing timestamp, source, and as path information
  my $timestamp = $self->get_timestamp();
  my $source_ip = $self->get_source_address();
  my $source_as = $self->get_source_asn();
  my $as_path = $self->get_aspath_as_str();

  #Returning a hash based on safi.  $hash->{'1'|'2'} = string
  my $outputHash = {};

  ##Creating Unicast string
  my $unicast = "";
  #Adding updates to output
  foreach my $p (@uanns){
    my $line = "$start_str|$timestamp|A|$source_ip|$source_as|$p|$as_path\n";
    $unicast .= $line;
  }
  #Adding withdraws to output
  foreach my $p (@uwiths){
    my $line = "$start_str|$timestamp|W|$source_ip|$source_as|$p\n";
    $unicast .= $line;
  }
  #putting into output hash
  $outputHash->{'1'} = $unicast if $unicast ne "";

  ##Creating Multicast string
  my $multicast = "";
  #Adding updates to output
  foreach my $p (@manns){
    my $line = "$start_str|$timestamp|A|$source_ip|$source_as|$p|$as_path\n";
    $multicast .= $line;
  }
  #Adding withdraws to output
  foreach my $p (@mwiths){
    my $line = "$start_str|$timestamp|W|$source_ip|$source_as|$p\n";
    $multicast .= $line;
  }
  #putting into output hash
  $outputHash->{'2'} = $multicast if $multicast ne "";


  #Returning
  return $outputHash;
}

=head2 to_string

Will return a string of the BGP message.  If the optional argument 'format' is
set to 'txt', it will return the message in a bgpdump format. Otherwise, it
will return an XML string in UTF-8

Input: None

Output: undef on error
        string if sucessful.
=cut
sub to_string{
  my $self = shift;
  my %opts = @_;

  #Checking to see if bgpdump was requested
  if(exists($opts{'format'}) && $opts{'format'} eq 'txt'){
    return $self->to_txt_string();
  }

  # Making sure the writer was made correctly.
  return undef unless defined($write);

  #Creatign the document
  my $doc;
  $doc = XML::LibXML::Document->new('1.0','UTF-8');
  return undef unless(defined($doc));

  my $start = Time::HiRes::time();
  my $out = $write->($doc,$self->{_data});
  $doc->setDocumentElement($out);
  my $format = 1;
  my $msg;
  if (exists($opts{'pretty_print'})) {
    $format = $opts{'pretty_print'};
  }
  $msg = $doc->toString($format);
  return undef unless(defined($msg));
  my @msg_lines = split(/\n/, $msg);
  if (exists($opts{'remove_xml_decl'}) && $opts{'remove_xml_decl'} == 1) {
    shift @msg_lines;
    $msg = join("\n", @msg_lines);
    $msg .= "\n";
  }
  my $end = Time::HiRes::time();
  my $el_time = ($end - $start) * 1000;
#  print "write_data_creation_time=$el_time";
  return $msg;
}

#=head2 add_node
#
#Will create an xml tag at the top layer of the message.
#E.g. add_node("STATUS", "TEST STATUS");
#Creates <STATUS>TEST STATUS</STATUS>
#
#Input: node_name - name for the XML tag
#       node_content - wht will be stored in the tag
#
#Output: None
#
#=cut
#sub add_node{
#  my $self = shift;
#  my $node_name = shift;
#  my $node_content = shift;
#  $self->{_data}->{$node_name} = $node_content;
#}


#sub print_something{
#
#  my $self = shift;
#  my $xml = shift;
#
#  my $doc = XML::LibXML::Document->new('1.0','UTF-8');
#  my $write = $cache->compile(WRITER =>
#      '{urn:ietf:params:xml:ns:bgp_monitor}BGP_MONITOR_MESSAGE',
#      use_default_namespace=>1);
#  my $out = $write->($doc,$xml);
#  $doc->setDocumentElement($out);
#  return $doc->toString(1);
#}

#sub read_something{
#  my $self = shift;
#  my $xml = shift;
#
#  my $reader = $cache->compile(READER =>
#      '{urn:ietf:params:xml:ns:bgp_monitor}BGP_MONITOR_MESSAGE');
#
## run often
#  my $hash = $reader->($xml);
## Data::Dumper is your friend
#  print STDERR Dumper $hash;
#  return $hash;
#}
#sub dump{
#  my $self = shift;
#  return Dumper($self->{_data});
#}

###############################################################################
############################### AUTOLOAD ######################################
###############################################################################
sub AUTOLOAD {
    my $fname = our $AUTOLOAD;
    $fname =~ s/.*:://;

    my $prefix = substr($fname, 0, 4);
    if ($prefix eq "get_") {
      my $what = substr($fname,4,100);
      my $self = shift;
      return $self->get_byid($what,@_);
    }
}

###############################################################################
############## GETTER AND SETTER FOR XML FIELDS################################
###############################################################################

###############################################################################
### BGP_MOINITOR_MESSAGE HEADERS
###############################################################################
=head2 get_byid

Returns the xpath query result identified by id in $nodemap

Input: What to get

Output: xpath result or undef on error

=cut
sub get_byid{
  my $self = shift;
  my $what = shift;
  my %opts = @_;

  my $path = $nodemap{$what};
  if (!defined($path) || $path eq "") {return undef;}
  my @patharray=();
  push(@patharray,$path);
  return $self->get_node(@patharray,%opts);
}

#Source
#sub set_souce_address{
#
#}
#sub set_source_asn{
#
#}
#sub set_source_port{
#}
#Destination
#sub set_dest_address{
#}
#sub set_dest_asn{
#}
#sub set_dest_port{
#}

#Monitor
#sub set_monitor_address{
#}
#sub set_monitor_asn{
#}
#sub set_monitor_port{
#}

#Relay
#sub set_relay_address{
#}
#sub set_relay_asn{
#}

#Observed Time
#sub set_timestamp{
#  my $self = shift;
#  #TODO
#}

#sub set_datetime{
#  my $self = shift;
#  #TODO
#}


#Sequence Number
#sub set_sequence_number{
#
#}

#Collection
#sub set_collection_method{
#
#}

#Parse Error
#sub get_parse_error{
#
#}
#sub set_parse_error{
#
#}

#Status
#sub set_status{
#  my $self = shift;
#  #TODO
#}

#Octet Message
#sub set_octets{
#
#}

#Metadata
=head2 get_metadata_nodes

Returns the head node of the metadata for the BGP message if there is any

Input: None

Output: undef on error
        arrayrefrence to metadata on success
=cut
sub get_metadata_nodes{
  my $self = shift;
  my @md = $self->get_byid('metadata_nodes');
  if (!defined($md[0])) {
    return undef;
  }

  return \@md;
}

#sub set_metadata{
#}


=head2 get_type

Returns the collection method of the message: Live or Table Dump

Input: None

Output: undef on error
        string of collection method
=cut
sub get_type{
  my $self = shift;
  return $self->get_collection_method();
}
#sub set_type{
#  my $self = shift;
#  #TODO
#}


##############################################################################
### NLRI SECTION
##############################################################################
=head2 add_nlri_node

Add a node to the NLRI list.

Input: NLRI node (WARNING - THIS IS NOT CHECKED FOR CORRECTNESS)

Output:
=cut
sub add_nlri_node{
  my $self = shift;
  my $node = shift;

  if(exists($self->{_data}->{'UPDATE'}->{'NLRI'})){
    push @{$self->{_data}->{'UPDATE'}->{'NLRI'}},$node;
  }else{
    $self->{_data}->{'UPDATE'}->{'NLRI'} = [$node];
  }
  return;
}
=head2 get_nlri_node

Returns a hashref to the NLRI section

Input: 'remove' => 1 - optional argument if NLRI node should be removed

Output: undef on error
        hashref to the head NLRI node
=cut
sub get_nlri_nodes{
  my $self = shift;
  my %opts = @_;
  my $res = $self->get_byid('nlri_nodes', %opts);
  if (ref($res) eq "HASH") {
    my @array=();
    push(@array, $res);
    return @array;
  }
  return $res;
}
=head2 get_nlris

Returns a list of the NLRIs in the NLRI section

Input: None

Output: array of NLRI prefixes - may be of size 0
=cut
sub get_nlris(){
  my $self = shift;
  my @nlris = $self->get_byid('nlris');
  if (!defined($nlris[0])) {
    @nlris=();
  }

  return @nlris;
}

##############################################################################
### WITHDRAW SECTION
##############################################################################
#sub add_withdraw_node{
#  my $self = shift;
##TODO
#}


=head2 get_withdraws

Returns a list of prefixes being withdrawn within the BGP message

Input: None

Output: Array of all withdraws found - may be empty if none found
=cut
sub get_withdraws{
  my $self = shift;
  my @withs = $self->get_byid('withdraws');
  if (!defined($withs[0])) {
    @withs=();
  }

  return @withs;

}


##############################################################################
### UPDATE SECTION
##############################################################################
#Type 1 - Origin
#sub set_bgp_origin{
#}

#Type 2 - AS_Path

=head2 get_aspath

Returns a list of ASNs in the AS_Path path attribute if it exists

Input: None

Output: array of ASNs found in AS_Path - may be of size 0
=cut
sub get_aspath{
  my $self = shift;
  my @asns = $self->get_byid('aspath');
  if (!defined($asns[0])) {
    @asns=();
  }

  return @asns;
}

=head2 get_aspath_as_str

Returns a string of the ASNs in the AS_Path path attribute, seperated by
spaces, if it exists

Input: None

Output: '' on error or if no AS_Path is found
        string of ASNs found in AS_Path
=cut
sub get_aspath_as_str{
  my $self = shift;
  my @asns = $self->get_aspath();
  #return '' if(!@asns);
  return '' if(scalar(@asns) == 0);
  return join(' ', @asns);
}

#Type 3 -  next hop
#sub set_nexthop{
#  my $self = shift;
##TODO
#}

#Type 4 -  med
#sub set_med{
#}

#Type 5 -  local pref
=head2 get_local_pref

Returns value of Local_pref path attribute if it exists

Input: None

Output: undef on error
        string of Local_pref if exists
=cut
sub get_local_pref{
  my $self = shift;
#TODO
}
#sub set_local_pref{
#  my $self = shift;
##TODO
#}

#Type 6 -  atomic aggregate
#sub set_atomic_agg{
#  my $self = shift;
##TODO
#}

#Type 7 -  aggregator
#sub set_aggregator{
#  my $self = shift;
##TODO
#}

#Type 8 -  community
=head2 get_community_nodes

Returns arrayref to Community path attribute nodes if they exists

Input: None

Output: undef on error
        arrayref to Communities
=cut
sub get_community_nodes{
  my $self = shift;
  my @communities = $self->get_byid('community_nodes');
  if (!defined($communities[0])) {
    return undef;
  }
  return \@communities;
}


#sub get_communities{
##TODO - get community nodes will return a list of all communities.
##maybe there is a way to return them with (asn,value) pairs in a list
#}
#sub set_community{
#  my $self = shift;
##TODO
#}

#Type 9 -  originator id
=head2 get_originator_id

TODO

Input: None

Output: undef on error
        SOMETHIGN ELSE HERE
=cut
sub get_originator_id{
  my $self = shift;
#TODO
}
#sub set_originator_id{
#  my $self = shift;
##TODO
#}

#Type 10 - cluster list
=head2 get_cluster_list

TODO

Input: None

Output: undef on error
        SOMETHIGN ELSE HERE
=cut
sub get_cluster_list{
  my $self = shift;
#TODO
}
#sub set_cluster_list{
#  my $self = shift;
##TODO
#}

#Type 14 -  mp reach nlri

=head2 get_mp_nlris

Returns an array of MP_NLRI prefixes found in MP_REACH_NLRI if it exists

Input: None

Output: Array of prefixes - could be of length 0
=cut
sub get_mp_nlris{
  my $self = shift;
  my @nlris = $self->get_byid('mp_nlris');
  if (!defined($nlris[0])) {
    @nlris=();
  }
  return @nlris;
}


=head2 add_mp_nlri_nodes

Will add element to the MP_NLRI nodes

TODO

Input: LibXML node

Output: None
=cut
sub add_mp_nlri_nodes{
  my $self = shift;
  my $node = shift;

  my $path = $nodemap{'mp_reach_node'};
  my @nodes= $xpc->findnodes($path, $self->{'_data'});
  if (scalar @nodes == 0){
    return undef;
  }
  my $parent=$nodes[0];
  $parent->addChild($node);

  return;
}

#Type 15 - mp unreach nlri
#sub add_mp_unreach_nlri_nodes{
##TODO UPDATE THIS FOR UNREACH
#  my $self = shift;
#}

=head2 get_mp_unreach_nlris

Returns an array of MP_NLRI prefixes found in MP_UNREACH_NLRI if it exists

Input: None

Output: Array of prefixes - could be of length 0
=cut
sub get_mp_unreach_nlris{
  my $self = shift;
  my @nlris = $self->get_byid('mp_unreach_nlris');
  if (!defined($nlris[0])) {
    @nlris=();
  }

  return @nlris;
}


#Type 16 - extended communities


#Type 17 - as4 path
=head2 get_as4_path

Returns an array of ASNs found in AS4_PATH if they exist

Input: None

Output: Array of ASNs - could be of size 0
=cut
sub get_as4_path{
  my $self = shift;
  my @aspath = $self->get_byid('as4_path');
  if (!defined($aspath[0])) {
    @aspath=();
  }
  return @aspath;
}

#sub set_as4_path{
#  my $self = shift;
##TODO
#}
=head2 get_as4_path_as_str

Returns a string of ASNs found in AS4_PATH if they exist

Input: None

Output: string of ASNs
=cut
sub get_as4path_as_str{
  my $self = shift;
  my @asns = $self->get_as4_path();
  return '' if(!@asns);
  return join(' ', @asns);
}

#Type 18 - as4 aggregator
#sub set_as4_aggregator{
#  my $self = shift;
##TODO
#}


#Type Unknown
#sub get_unknown_att{
##TODO
##UNKNOWN_ATTRIBUTE
#}




#Other pieces needed for above
=head2 get_safi

Returns an int of the SAFI for the update message

Input: None

Output: undef on error
        string on success
=cut
=comment TAKING OUT
sub get_safi(){
  my $self = shift;
  my $mpreachnode = $self->get_mp_reach_node();
  return 1 if(!defined($mpreachnode));
  return $mpreachnode->{'safi'};
}
=cut
=head1 AUTHOR

M. Lawrence Weikum, C<< <mweikum at rams.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bgpmon at netsec.colostate.edu>, or through
the web interface at L<http://bgpmon.netsec.colostate.edu>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::Message

=cut

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2012 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\

    File: Fetch.pm

    Authors: M. Lawrence Weikum, Kaustubh Gadkari, Cathie Olschanowsky
    Date: 7 March, 2014

=cut



1;

