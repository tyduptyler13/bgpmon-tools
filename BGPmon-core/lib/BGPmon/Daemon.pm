package BGPmon::Daemon;

use 5.14.0;
use strict;
use warnings;
use POSIX;

our $VERSION = '1.0';

require Exporter;
our @ISA = qw /Exporter/;
our @EXPORT_OK = qw(write_pid_file daemonize);

=head1 NAME

BGPmon::Daemon

This module daemonizes a Perl script which utilizes the BGPmon modules.

=head1 SYNOPSIS

use BGPmon::Daemon;

BGPmon::Daemon::daemonize();

=head1 SUBROUTINES/METHODS

=head2 write_pid_file

Writes a PID file to /tmp.

=cut

#---- Write PID file. ----
sub write_pid_file {
    my %args = @_;

    my $pid = $$;
    if (exists($args{pid})) {
        $pid = $args{pid};
    }

# Set state directory.
    my $state_dir = "/tmp";
    if (exists($args{state_dir})) {
        $state_dir = $args{state_dir};
    }

# Set pid file name.
    my $pid_file_name = "archiver.pid";
    if (exists($args{pid_file_name})) {
        $pid_file_name = $args{pid_file_name};
    }

# Open pid file and write pid. Close file after writing pid.
    my $pid_file = "$state_dir/$pid_file_name";
    my $pid_file_fh;
    if (open ($pid_file_fh, ">$pid_file")) {
        my $bytes_written = syswrite($pid_file_fh, "$pid\n");
        if (!defined($bytes_written) || $bytes_written == 0) {
            print $pid;
        }
        close($pid_file_fh);
    } else {
        print "Could not open PID file $pid_file. Archiver PID is $pid.\n";
    }
}

=head2 daemonize

Daemonize the calling script.

=cut

#---- Put the archiver in daemon mode. ----
sub daemonize {
# Fork and exit parent. Makes sure we are not a process group leader.
    my $pid = fork;
    exit 0 if $pid;
    exit 1 if not defined $pid;

# Become leader of a new session, group leader of new
# process group and detach from any terminal.
    setsid();
    $pid = fork;
    exit 0 if $pid;
    exit 1 if not defined $pid;

# Change directory to / so that we don't block this filesystem.
    chdir '/' or die ("Could not chdir to /: $!");

# Clear umask for file creation.
    umask 0;

# Write pid file.
    write_pid_file(pid => $pid);
}

=head1 AUTHOR

Kaustubh Gadkari, C<< <kaustubh at cs.colostate.edu> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bgpmon at netsec.colostate.edu>, or through
the web interface at L<http://bgpmon.netsec.colostate.edu>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc BGPmon::Message

=cut

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2012 Colorado State University

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.\

    File: Fetch.pm

    Authors: Kaustubh Gadkari
    Date: 31 March, 2014
=cut

1;
