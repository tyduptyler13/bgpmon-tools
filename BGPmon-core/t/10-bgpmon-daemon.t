use strict;
use warnings;
use Test::More;

BEGIN {
    use_ok('BGPmon::Daemon');
}

use BGPmon::Daemon;

system("t/daemon-test.pl");
ok(-e "/tmp/archiver.pid", "Check if pid file exists");
unlink "/tmp/archiver.pid";

