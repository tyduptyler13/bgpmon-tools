use strict;
use warnings;
use Test::More;

use BGPmon::Message;
use Data::Dumper;


### XML messages for testing

#message with a table start
my $table_start = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements">
<SOURCE><ADDRESS afi="2">2001:4810::1</ADDRESS><PORT>179</PORT><ASN4>33437</ASN4></SOURCE>
<DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST>
<MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR>
<OBSERVED_TIME precision="false"> <TIMESTAMP>1390342758</TIMESTAMP><DATETIME>2014-01-21T22:19:18Z</DATETIME></OBSERVED_TIME>
<SEQUENCE_NUMBER>1942880692</SEQUENCE_NUMBER>
<STATUS Author_Xpath="/BGP_MONITOR_MESSAGE/MONITOR"><TYPE>TABLE_START</TYPE></STATUS></BGP_MONITOR_MESSAGE>';

#simple xml message
my $xml = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements">
<SOURCE><ADDRESS afi="1">94.126.183.247</ADDRESS><PORT>179</PORT><ASN4>59469</ASN4></SOURCE>
<DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST>
<MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR>
<OBSERVED_TIME precision="false"><TIMESTAMP>1390411034</TIMESTAMP><DATETIME>2014-01-22T17:17:14Z</DATETIME></OBSERVED_TIME>
<SEQUENCE_NUMBER>753408484</SEQUENCE_NUMBER>
<COLLECTION>LIVE</COLLECTION>
<bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>59469</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>3257</bgp:ASN4><bgp:ASN4>15412</bgp:ASN4><bgp:ASN4>18101</bgp:ASN4><bgp:ASN4>45117</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">94.126.183.247</bgp:NEXT_HOP><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3257</bgp:ASN2><bgp:VALUE>3257</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>2</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>22</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>86</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>500</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>666</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>2064</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>3356</bgp:ASN2><bgp:VALUE>11078</bgp:VALUE></bgp:COMMUNITY><bgp:NLRI afi="1">27.54.183.0/24</bgp:NLRI><bgp:NLRI afi="1">103.11.119.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF006A020000004B4001010040021A02060000E84D00000D1C00000CB900003C34000046B50000B03D4003045E7EB7F7C008200CB90CB90D1C00020D1C00160D1C00560D1C01F40D1C029A0D1C08100D1C2B46181B36B718670B77</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#xml message wtih IPv6 in it
my $xml6 = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2607:4200:10::2</ADDRESS><PORT>179</PORT><ASN4>19214</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390422185</TIMESTAMP><DATETIME>2014-01-22T20:23:05Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1943427430</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>19214</bgp:ASN4><bgp:ASN4>174</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>29119</bgp:ASN4><bgp:ASN4>58345</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:MP_REACH_NLRI optional="true" transitive="false" partial="false" extended="false" attribute_type="14" safi="1"><bgp:MP_NEXT_HOP afi="2">2607:4200:10::2</bgp:MP_NEXT_HOP><bgp:MP_NLRI afi="2">2a03:e080::/32</bgp:MP_NLRI></bgp:MP_REACH_NLRI></bgp:UPDATE></BGP_MONITOR_MESSAGE>';

#xml message with IPv6 and multicast
my $xml6safi2 = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2607:4200:10::2</ADDRESS><PORT>179</PORT><ASN4>19214</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1390422185</TIMESTAMP><DATETIME>2014-01-22T20:23:05Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1943427430</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>19214</bgp:ASN4><bgp:ASN4>174</bgp:ASN4><bgp:ASN4>3356</bgp:ASN4><bgp:ASN4>29119</bgp:ASN4><bgp:ASN4>58345</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:MP_REACH_NLRI optional="true" transitive="false" partial="false" extended="false" attribute_type="14" safi="2"><bgp:MP_NEXT_HOP afi="2">2607:4200:10::2</bgp:MP_NEXT_HOP><bgp:MP_NLRI afi="2">2a03:e080::/32</bgp:MP_NLRI></bgp:MP_REACH_NLRI></bgp:UPDATE></BGP_MONITOR_MESSAGE>';

#message containing withdraws
my $withmsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">134.222.87.4</ADDRESS><PORT>179</PORT><ASN4>286</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.108</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.108</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1393881551</TIMESTAMP><DATETIME>2014-03-03T21:19:11Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>306236329</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:WITHDRAW afi="1">205.89.160.0/20</bgp:WITHDRAW></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF001B02000414CD59A00000</OCTET_MESSAGE><METADATA><NODE_PATH>//bgp:UPDATE["205.89.160.0/20"]/bgp:WITHDRAW</NODE_PATH><ANNOTATION>WITH</ANNOTATION></METADATA></BGP_MONITOR_MESSAGE>';

#message with origin attribute
my $originmsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2607:fad8::1:3</ADDRESS><PORT>179</PORT><ASN4>22652</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1393883461</TIMESTAMP><DATETIME>2014-03-03T21:51:01Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>47584509</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN></bgp:UPDATE><OCTET_MESSAGE></OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#message with next hop attribute
my $nexthopmsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">205.166.205.202</ADDRESS><PORT>179</PORT><ASN4>6360</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1393904978</TIMESTAMP><DATETIME>2014-03-04T03:49:38Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1673855361</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">205.166.205.202</bgp:NEXT_HOP><bgp:NLRI afi="1">199.187.118.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00520200000037400101004002220208000018D8000010E3000002BD000004D700003997000039970000399700002B2E400304CDA6CDCAC00804FFFFFF0118C7BB76</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#message with MED attribute
my $medmsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2001:1620:1::203</ADDRESS><PORT>179</PORT><ASN4>13030</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.112</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:3370</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1393905945</TIMESTAMP><DATETIME>2014-03-04T04:05:45Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>47736645</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:MULTI_EXIT_DISC optional="true" transitive="false" partial="false" extended="false" attribute_type="4">16777216</bgp:MULTI_EXIT_DISC></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0083020000006C40010100400232020C000032E600001B1B0000CD5C0000CD5C0000CD5C0000CD5C0000CD5C0000CD5C0000CD5C0000CD5C0000CD5C0000CD5C80040400000001C0080C32E6003D32E6064D32E6CAC0800E1A0002011020011620000100000000000000000203002028040CE0</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#message with community attribute
my $communityMsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">205.166.205.202</ADDRESS><PORT>179</PORT><ASN4>6360</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1394050473</TIMESTAMP><DATETIME>2014-03-05T20:14:33Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1676017778</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">205.166.205.202</bgp:NEXT_HOP><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>65535</bgp:ASN2><bgp:VALUE>65281</bgp:VALUE></bgp:COMMUNITY><bgp:NLRI afi="1">91.147.158.0/24</bgp:NLRI><bgp:NLRI afi="1">91.147.156.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF005A020000003B400101004002260209000018D8000010E300001B6A000000AE000099DA000099DA000099DA000099DA0000AAFF400304CDA6CDCAC00804FFFFFF01185B939E185B939C</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#message with as4_path attribute
my $as4pathMsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">195.66.224.39</ADDRESS><PORT>179</PORT><ASN2>3561</ASN2></SOURCE><DEST><ADDRESS afi="1">195.66.241.146</ADDRESS><PORT>179</PORT><ASN2>6447</ASN2></DEST><MONITOR><ADDRESS afi="1">195.66.225.222</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1394049184</TIMESTAMP><DATETIME>2014-03-05T19:53:04Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>2057215406</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN2>3561</bgp:ASN2><bgp:ASN2>3356</bgp:ASN2><bgp:ASN2>3549</bgp:ASN2><bgp:ASN2>28329</bgp:ASN2><bgp:ASN2>23456</bgp:ASN2><bgp:ASN2>23456</bgp:ASN2></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">195.66.224.39</bgp:NEXT_HOP><bgp:AS4_PATH optional="true" transitive="true" partial="true" extended="false" attribute_type="17"><bgp:AS_SEQUENCE><bgp:ASN4>3549</bgp:ASN4><bgp:ASN4>28329</bgp:ASN4><bgp:ASN4>262502</bgp:ASN4><bgp:ASN4>263641</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS4_PATH><bgp:NLRI afi="1">179.127.153.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF004C02000000314001010040020E02060DE90D1C0DDD6EA95BA05BA0400304C342E027E01112020400000DDD00006EA900040166000405D918B37F99</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

#message wtih mp_unreach attribute
my $unreachMsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2001:1890:111d::1</ADDRESS><PORT>179</PORT><ASN2>7018</ASN2></SOURCE><DEST><ADDRESS afi="1">128.223.51.108</ADDRESS><PORT>179</PORT><ASN2>6447</ASN2></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:336c</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1394050854</TIMESTAMP><DATETIME>2014-03-05T20:20:54Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>309208468</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:MP_UNREACH_NLRI optional="true" transitive="false" partial="false" extended="false" attribute_type="15" safi="1"><bgp:MP_NLRI afi="2">2607:fd28:100::/48</bgp:MP_NLRI><bgp:MP_NLRI afi="2">2401:8f00::/32</bgp:MP_NLRI></bgp:MP_UNREACH_NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00290200000012800F0F000201302607FD2801002024018F00</OCTET_MESSAGE><METADATA><NODE_PATH>//bgp:UPDATE/bgp:MP_REACH2607:fd28:100::/48]/NLRI</NODE_PATH><ANNOTATION>WITH</ANNOTATION></METADATA><METADATA><NODE_PATH>//bgp:UPDATE/bgp:MP_REACH2401:8f00::/32]/NLRI</NODE_PATH><ANNOTATION>WITH</ANNOTATION></METADATA></BGP_MONITOR_MESSAGE>';
my $unreachSafi2Msg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2001:1890:111d::1</ADDRESS><PORT>179</PORT><ASN2>7018</ASN2></SOURCE><DEST><ADDRESS afi="1">128.223.51.108</ADDRESS><PORT>179</PORT><ASN2>6447</ASN2></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:336c</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1394050854</TIMESTAMP><DATETIME>2014-03-05T20:20:54Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>309208468</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:MP_UNREACH_NLRI optional="true" transitive="false" partial="false" extended="false" attribute_type="15" safi="2"><bgp:MP_NLRI afi="2">2607:fd28:100::/48</bgp:MP_NLRI><bgp:MP_NLRI afi="2">2401:8f00::/32</bgp:MP_NLRI></bgp:MP_UNREACH_NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00290200000012800F0F000201302607FD2801002024018F00</OCTET_MESSAGE><METADATA><NODE_PATH>//bgp:UPDATE/bgp:MP_REACH2607:fd28:100::/48]/NLRI</NODE_PATH><ANNOTATION>WITH</ANNOTATION></METADATA><METADATA><NODE_PATH>//bgp:UPDATE/bgp:MP_REACH2401:8f00::/32]/NLRI</NODE_PATH><ANNOTATION>WITH</ANNOTATION></METADATA></BGP_MONITOR_MESSAGE>';

#message with extended community attribute
my $extCommMsg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="1">129.250.1.248</ADDRESS><PORT>179</PORT><ASN4>2914</ASN4></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN4>6447</ASN4></DEST><MONITOR><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1394053853</TIMESTAMP><DATETIME>2014-03-05T21:10:53Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1676178958</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN4>2914</bgp:ASN4><bgp:ASN4>12956</bgp:ASN4><bgp:ASN4>3816</bgp:ASN4></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:NEXT_HOP optional="false" transitive="true" partial="false" extended="false" attribute_type="3" afi="1">129.250.1.248</bgp:NEXT_HOP><bgp:MULTI_EXIT_DISC optional="true" transitive="false" partial="false" extended="false" attribute_type="4">151060480</bgp:MULTI_EXIT_DISC><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>2914</bgp:ASN2><bgp:VALUE>420</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>2914</bgp:ASN2><bgp:VALUE>1008</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>2914</bgp:ASN2><bgp:VALUE>2000</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>2914</bgp:ASN2><bgp:VALUE>3000</bgp:VALUE></bgp:COMMUNITY><bgp:COMMUNITY optional="true" transitive="true" partial="false" extended="false" attribute_type="8"><bgp:ASN2>65504</bgp:ASN2><bgp:VALUE>12956</bgp:VALUE></bgp:COMMUNITY><bgp:EXTENDED_COMMUNITIES optional="true" transitive="true" partial="false" extended="false" attribute_type="16" extended_communities_type="Two-Octet AS Specific Extended Community" extended_communities_subtype="Route Target"><bgp:ASN2>3816</bgp:ASN2><bgp:VALUE>01CFDE01</bgp:VALUE></bgp:EXTENDED_COMMUNITIES><bgp:NLRI afi="1">200.21.43.0/24</bgp:NLRI></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF006002000000454001010040020E020300000B620000329C00000EE840030481FA01F880040400000109C008140B6201A40B6203F00B6207D00B620BB8FFE0329CC0100800020EE801CFDE0118C8152B</OCTET_MESSAGE><METADATA><NODE_PATH>//bgp:UPDATE/bgp:MP_REACH[MP_NLRI="200.21.43.0/24"]/MP_NLRI</NODE_PATH><ANNOTATION>NANN</ANNOTATION></METADATA></BGP_MONITOR_MESSAGE>';

#message with atomic aggregator, aggregator, and as4_aggregtor
my $aggaggagg = '<BGP_MONITOR_MESSAGE xmlns:xsi="http://www.w3.org/2001/XMLSchema" xmlns="urn:ietf:params:xml:ns:bgp_monitor" xmlns:bgp="urn:ietf:params:xml:ns:xfb" xmlns:ne="urn:ietf:params:xml:ns:network_elements"><SOURCE><ADDRESS afi="2">2001:15a8:a:a::2</ADDRESS><PORT>179</PORT><ASN2>29449</ASN2></SOURCE><DEST><ADDRESS afi="1">128.223.51.15</ADDRESS><PORT>179</PORT><ASN2>6447</ASN2></DEST><MONITOR><ADDRESS afi="2">2001:468:d01:33::80df:330f</ADDRESS><PORT>0</PORT><ASN4>0</ASN4></MONITOR><OBSERVED_TIME precision="false"><TIMESTAMP>1394054366</TIMESTAMP><DATETIME>2014-03-05T21:19:26Z</DATETIME></OBSERVED_TIME><SEQUENCE_NUMBER>1676203080</SEQUENCE_NUMBER><COLLECTION>LIVE</COLLECTION><bgp:UPDATE bgp_message_type="2"><bgp:ORIGIN optional="false" transitive="true" partial="false" extended="false" attribute_type="1">IGP</bgp:ORIGIN><bgp:AS_PATH optional="false" transitive="true" partial="false" extended="false" attribute_type="2"><bgp:AS_SEQUENCE><bgp:ASN2>29449</bgp:ASN2><bgp:ASN2>174</bgp:ASN2><bgp:ASN2>6453</bgp:ASN2><bgp:ASN2>4755</bgp:ASN2><bgp:ASN2>9885</bgp:ASN2><bgp:ASN2>55824</bgp:ASN2><bgp:ASN2>23456</bgp:ASN2></bgp:AS_SEQUENCE></bgp:AS_PATH><bgp:ATOMIC_AGGREGATE optional="false" transitive="true" partial="false" extended="false" attribute_type="6"/><bgp:AGGREGATOR optional="true" transitive="true" partial="false" extended="false" attribute_type="7"><bgp:ASN2>23456</bgp:ASN2><bgp:IPv4_ADDRESS afi="1">115.248.111.113</bgp:IPv4_ADDRESS></bgp:AGGREGATOR><bgp:AS4_AGGREGATOR optional="true" transitive="true" partial="true" extended="false" attribute_type="18"><bgp:ASN4>132043</bgp:ASN4><bgp:IPv4_ADDRESS afi="1">115.248.111.113</bgp:IPv4_ADDRESS></bgp:AS4_AGGREGATOR></bgp:UPDATE><OCTET_MESSAGE>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0081020000006A400101004002100207730900AE19351293269DDA105BA0400600C007065BA073F86F71E0111A0206000000AE00001935000012930000269D0000DA10000203CBE01208000203CB73F86F71800E1C00020110200115A8000A000A0000000000000002003020010DF00264</OCTET_MESSAGE></BGP_MONITOR_MESSAGE>';

####Testing generic new and prints

# Testing to_string()
my $msg1 = BGPmon::Message->new(string=>$xml);
my $out_xml = $msg1->to_string();
ok(defined($out_xml), "Testing to string defined");
#TODO take out all whitespace in between tags and compare equil

# Testing Pretty Print (Kaustubh additions)
$out_xml = $msg1->to_string(remove_xml_decl => 1);
ok(defined($out_xml), "Testing pretty print off");
#TODO check output is what we want
$out_xml = $msg1->to_string(remove_xml_decl => 1, pretty_print => 0);
ok(defined($out_xml), "Testing pretty print on");
#TODO check output is what we want

####Testing internals

##Testing get_node
my $msg11 = BGPmon::Message->new(string=>$xml);
#my $source = $msg11->get_node("/BGP_MONITOR_MESSAGE/SOURCE");
my $source = $msg11->get_node("bgpm:SOURCE");
ok(defined($source), 'Get node that exists');
is($source->{'ADDRESS'}->{'_'}, '94.126.183.247', 'Get Node pieces are correct');
is($source->{'ASN4'}, '59469', 'Get Node pieces are correct');
my $dest = $msg11->get_node("bgpm:FAKE_TAG");
ok(!defined($dest), 'Get node that doest exist');

$msg11 = BGPmon::Message->new(string=>$withmsg);
my $withdraw = $msg11->get_node("bgp:UPDATE/bgp:WITHDRAW");
ok(defined($withdraw), 'Get node that exists');
is($withdraw->{'_'}, '205.89.160.0/20', 'Get Node pieces are correct');

##Testing remove node

#Removing something once
my $msg = BGPmon::Message->new(string=>$table_start);
$msg->remove_node('bgpm:STATUS');
$out_xml = $msg->to_string();
ok(defined($out_xml), 'Remove Node still gives string');
is(index($out_xml, 'STATUS'), -1, 'Remove Node has removed node');

#Removing something twice
my $msg12 = BGPmon::Message->new(string=>$xml);
my $nlri = $msg12->remove_node("bgp:UPDATE/bgp:NLRI");
my $test_nlri = $msg12->get_node("bgp:UPDATE/bgp:NLRI");
is($test_nlri,undef, 'Remove node, cannot get node');

#Removing tag that doesn't exist
my $bad_remove = $msg12->remove_node("bgp:UPDATE/bgp:FAKE_TAG");
is($bad_remove, undef, 'Remove node that does not exist');

##Making our own message
=comment
TODO must try this again - get pieces, make new message, add pieces
my $msg2 = BGPmon::Message->new();
$msg2->add_node("SOURCE",$source);
$msg2->add_node("DEST",$dest);
$msg2->add_node("MONITOR",$monitor);
$msg2->add_node("OBSERVED_TIME",$observed_time);
$msg2->add_node("SEQUENCE_NUMBER",{_=>0});
$msg2->add_node("UPDATE",$update_test);
$out_xml = $msg2->to_string();
ok(defined($out_xml), 'Add_node gives string');
#TODO make sure the pieces we wanted in there are in there
=cut


##making bgpdump pieces

#Testing with update stream
my $upMsg = BGPmon::Message->new(string=>$xml);
my $str = $upMsg->to_txt_string();
my $supposeStr = "BGP4MP|1390411034|A|94.126.183.247|59469|27.54.183.0/24|59469 3356 3257 15412 18101 45117
BGP4MP|1390411034|A|94.126.183.247|59469|103.11.119.0/24|59469 3356 3257 15412 18101 45117
";
is($str->{'1'}, $supposeStr, "To bgpdump update stream");
#Testing with rib stream
#my $ribMSG = BGPmon::Message->new(string=>$xml, RIB=>1);
#$str = $ribMSG->to_txt_string();
#$supposeStr = "TABLE_DUMP2|1390411034|A|94.126.183.247|59469|27.54.183.0/24|59469 3356 3257 15412 18101 45117
#TABLE_DUMP2|1390411034|A|94.126.183.247|59469|103.11.119.0/24|59469 3356 3257 15412 18101 45117
#";
#is($str->{'1'}, $supposeStr, "To bgpdump rib stream");
#Testing with update stream
$upMsg = BGPmon::Message->new(string=>$withmsg);
$supposeStr = "BGP4MP|1393881551|W|134.222.87.4|286|205.89.160.0/20
";
$str = $upMsg->to_txt_string();
is($str->{'1'}, $supposeStr, "To bgpdump with withdraw");
#Testing mp reach with safi = 2
my $multicastMsg = BGPmon::Message->new(string=>$xml6safi2);
$supposeStr = "BGP4MP|1390422185|A|2607:4200:10::2|19214|2a03:e080::/32|19214 174 3356 29119 58345
";
$str = $multicastMsg->to_txt_string();
is($str->{'2'}, $supposeStr, "To bgpdump with mp_reach safi 2");
#Testing mp unreach with safi = 2
$multicastMsg = BGPmon::Message->new(string=>$unreachSafi2Msg);
$supposeStr = "BGP4MP|1394050854|W|2001:1890:111d::1|7018|2607:fd28:100::/48|
BGP4MP|1394050854|W|2001:1890:111d::1|7018|2401:8f00::/32|
";
$str = $multicastMsg->to_txt_string();
is($str->{'2'}, $supposeStr, "To bgpdump with unreach safi 2");
#TODO test the bgpdump status messages


####Testing Headers

##Testing source information
my $tableMessage = BGPmon::Message->new(string=>$table_start);
my $sourceAddr = $tableMessage->get_source_address();
is($sourceAddr, "2001:4810::1", "Testing get peer address");
is($tableMessage->get_source_asn(), '33437', "Testing get source ASN");
is($tableMessage->get_source_port(), '179', "Testing get source port");
#TODO test setters

##Testing destination information
my $tableMessage2 = BGPmon::Message->new(string=>$table_start);
is($tableMessage2->get_dest_address(), '128.223.51.112', "Testing get dest address");
is($tableMessage2->get_dest_asn(), '6447', "Testing get dest ASN");
is($tableMessage2->get_dest_port(), '179', "Testing get dest port");
#TODO test setters


##Testing monitor information
my $monitormsg = BGPmon::Message->new(string=>$xml);
is($monitormsg->get_monitor_address(), '128.223.51.15', 'Getting monitor information');
is($monitormsg->get_monitor_asn(), '0', 'Getting monitor information');
is($monitormsg->get_monitor_port(), '0', 'Getting monitor information');



##Testing date and time information

#Testing Timestamp
my $timemsg = BGPmon::Message->new(string=>$xml);
is($timemsg->get_timestamp(), '1390411034', 'Testing get timestamp');
#TODO test setters

#Testing Datetime
is($timemsg->get_datetime(), '2014-01-22T17:17:14Z', 'Testing get date time');
#TODO test setters


##Testing sequence number information
my $seqmsg = BGPmon::Message->new(string=>$xml);
is($seqmsg->get_sequence_number(), '753408484', 'Testing get sequence number');
#TODO test setters

##Testing Status message informatioin
my $statusMessage = BGPmon::Message->new(string=>$table_start);
is($statusMessage->get_status(), "TABLE_START", "Testing retrieving a status message");
my $statusMessage2 = BGPmon::Message->new(string=>$xml);
is($statusMessage2->get_status(), undef, "Testing retrieving a status message where there isn't one");
#TODO test setters




####Testing Withdraws
#Getting withdraws where there are none
my $withMsgBad = BGPmon::Message->new(string=>$table_start);
is($withMsgBad->get_withdraw_node(), undef, 'Testing get_withdraws where there are none');

#Getting withdraw node
my $withMsg = BGPmon::Message->new(string=>$withmsg);
my $wresp = $withMsg->get_withdraw_node();
ok(defined($wresp), 'Testing get_withdraw_node');

#Get list of withdraws
my @withs = $withMsg->get_withdraws();
is($withs[0], '205.89.160.0/20', 'Testing get_withdraws');
#TODO test setters


####Testing NLRI's
my $nlriMsgBad = BGPmon::Message->new(string=>$table_start);
is($nlriMsgBad->get_nlri_nodes(), undef, 'Testing get_nlri_nodes where there are none');

#Getting nlri node
my $nlriMsg = BGPmon::Message->new(string=>$xml);
my $nlrires = $nlriMsg->get_nlri_nodes();
ok(defined($nlrires), 'Testing get_nlri_nodes');

#Get list of nlris
my @nlris = $nlriMsg->get_nlris();
is($nlris[0], '27.54.183.0/24', 'Testing get_nlris address 1');
is($nlris[1], '103.11.119.0/24', 'Testing get_nlris address 2');
#TODO test add_nlri_node





####Testing Update Section

##Type 1 - Origin
my $origMsg = BGPmon::Message->new(string=>$table_start);
is($origMsg->get_bgp_origin(), undef, 'Testing origin attribute where there is none');
$origMsg = BGPmon::Message->new(string=>$originmsg);
is($origMsg->get_bgp_origin(), 'IGP', 'Testing origin attribute');
#TODO test setters

##Type 2 -  ASPath
#Testing as path where there isnt' one
my $aspathMsg = BGPmon::Message->new(string=>$table_start);
my @aspath = $aspathMsg->get_aspath();
print Dumper @aspath;
is(scalar(@aspath), 0, "Getting AS Path where there is none");

#Testing get_aspath
$aspathMsg = BGPmon::Message->new(string=>$xml);
@aspath = $aspathMsg->get_aspath();
ok(@aspath, "Parsed AS Path");
is(scalar(@aspath), 6, 'Length of AS Path');
is($aspath[0], '59469', 'AS Path first ASN');
is($aspath[-1], '45117', 'AS Path last ASN');

#Testing get_aspath_as_str
is($aspathMsg->get_aspath_as_str(), '59469 3356 3257 15412 18101 45117', "Testing AS Path as string");
#TODO test setters

##Type 3 - Next Hop
my $nhmsg = BGPmon::Message->new(string=>$table_start);
is($nhmsg->get_nexthop(), undef, 'Getting next hop where there is none');
$nhmsg = BGPmon::Message->new(string=>$nexthopmsg);
is($nhmsg->get_nexthop(), '205.166.205.202', 'Getting next hop');
#TODO test setters


##Type 4 - MED
my $medMsg = BGPmon::Message->new(string=>$table_start);
is($medMsg->get_med(), undef, 'Getting MED where there is none');
$medMsg = BGPmon::Message->new(string=>$medmsg);
is($medMsg->get_med(), '16777216', 'Getting MED');
#TODO test setters

##Type 5 - Local Pref
##TODO

##Type 6 - Atomic Aggregate
my $atoMsg = BGPmon::Message->new(string=>$table_start);
is($atoMsg->get_atomic_agg(), undef, 'Getting Atomic Aggregate where there is none');
$atoMsg = BGPmon::Message->new(string=>$aggaggagg);
ok(defined($atoMsg->get_atomic_agg()), 'Getting Atomic Aggregate');
#TODO test setters

##Type 7 - Aggregator
my $aggMsg = BGPmon::Message->new(string=>$table_start);
is($aggMsg->get_aggregator_node(), undef, 'Getting Aggregator where there is none');
$aggMsg = BGPmon::Message->new(string=>$aggaggagg);
ok(defined($aggMsg->get_aggregator_node()), 'Getting Aggregator Node');
is($aggMsg->get_aggregator_address(), '115.248.111.113', 'Getting Aggregator IP address');
is($aggMsg->get_aggregator_asn(), '23456', 'Getting Aggregator ASN');
#TODO test setters

##Type 8 - Community
my $commMsg = BGPmon::Message->new(string=>$table_start);
is($commMsg->get_community_nodes(), undef, 'Getting Community where there is none');
$commMsg = BGPmon::Message->new(string=>$communityMsg);
my $communities = $commMsg->get_community_nodes();
is(scalar(@{$communities}), 1, "Getting Number of Communities");
is($communities->[0]->{'VALUE'}, '65281', 'Gettinig Community Value');
is($communities->[0]->{'ASN2'}, '65535', 'Gettinig Community ASN');
#TODO test setters

##Type 9 - Originator ID
##TODO

##Type 10 - Cluster List
##TODO


##Type 14 - MP Reach NLRI
my $msg6 = BGPmon::Message->new(string=>$xml6);
ok(defined($msg6), 'Read MP Reach Message');
#Getting and removing mp_nlri_node
#my $mp_nlri = $msg6->get_mp_nlri_nodes(remove=>1);
#ok(defined($mp_nlri), 'get mp_nlri_nodes');
#TODO test pieces retrieved
#ok(!defined($msg6->get_mp_nlri_nodes()), 'Duplicate remove get_mp_nlri_nodes');
#Adding mp nlri dones
#TODO
#$msg6->add_mp_nlri_nodes($mp_nlri);
#ok(defined($msg6->get_mp_nlri_nodes()), 'Adding mp_nlri nodes');
#$msg6 = BGPmon::Message->new(string=>$xml6);
#my @mpnlris = $msg6->get_mp_nlris();
#is(scalar(@mpnlris), 1, 'Testing get_mp_nlris');
#is($mpnlris[0], '2a03:e080::/32', 'Comparing prefixes in get_mp_nlris');
#is($msg6->get_mp_safi(), '1', "Comparing SAFI of mp_reach node");
#my $safiMessage2 = BGPmon::Message->new(string=>$xml6safi2);
#is($safiMessage2->get_mp_safi(), '2', "Comparing SAFI of mp_reach multicast node");



#TODO finishteting setters

##Type 15 - MP Unreach Nlri
my $unreachmsg = BGPmon::Message->new(string=>$table_start);
ok(!defined($unreachmsg->get_mp_unreach_node()),
   'Getting MP_UNREACH where there is none');
$unreachmsg = BGPmon::Message->new(string=>$unreachMsg);
ok(defined($unreachmsg), "Parsed MP_UNREACH message");
my @unreaches = $unreachmsg->get_mp_unreach_nlris();
is(scalar(@unreaches), 2, "Getting NLRIs from MP_UNREACH message");
is($unreaches[0], '2607:fd28:100::/48', 'Getting first MP_NLRI from UNREACH');
is($unreaches[1], '2401:8f00::/32', 'Getting second MP_NLRI from UNREACH');
is($unreachmsg->get_mp_unreach_safi(), '1', 'Getting Unicast SAFI from MP_UNREACH');
$unreachmsg = BGPmon::Message->new(string=>$unreachSafi2Msg);
is($unreachmsg->get_mp_unreach_safi(), '2', 'Getting Multicast SAFI from MP_UNREACH');
#TODO Test setters



##Type 16 - Extended Communities
my $extCommmsg = BGPmon::Message->new(string=>$table_start);
is($extCommmsg->get_ext_comm_nodes(), undef, 'Getting Extended Community where there is none');
$extCommmsg = BGPmon::Message->new(string=>$extCommMsg);
ok(defined($extCommmsg), "Parsed Extended Community");
my $extcomm = $extCommmsg->get_ext_comm_nodes();
ok(defined($extcomm),'Getting Extended Communities');
is($extcomm->{'VALUE'}, '01CFDE01', 'Gettinig Extended Community Value');
is($extcomm->{'ASN2'}, '3816', 'Gettinig Extended Community ASN');
#TODO test setters


##Type 17 - AS4 Path
my $as4path = BGPmon::Message->new(string=>$table_start);
is(scalar($as4path->get_as4_path()), 0, 'Getting AS4_Path where there is none');
$as4path = BGPmon::Message->new(string=>$as4pathMsg);
my @as4p = $as4path->get_as4_path();
ok(@as4p, "Parsed AS4_Path");
is(scalar(@as4p), 4, 'Length of AS4 Path');
is($as4p[0], '3549', 'AS4 Path first ASN');
is($as4p[-1], '263641', 'AS4 Path last ASN');

is($as4path->get_as4path_as_str(), '3549 28329 262502 263641', "Testing AS4_Path as string");
#TODO test setters

##Type 18 - AS4 Aggregator
my $agg4Msg = BGPmon::Message->new(string=>$table_start);
is($agg4Msg->get_as4_aggregator_node(), undef, 'Getting AS4 Aggregator where there is none');
$agg4Msg = BGPmon::Message->new(string=>$aggaggagg);
ok(defined($agg4Msg->get_as4_aggregator_node()), 'Getting AS4 Aggregator Node');
is($agg4Msg->get_as4_aggregator_address(), '115.248.111.113', 'Getting AS4 Aggregator IP address');
is($agg4Msg->get_as4_aggregator_asn(), '132043', 'Getting AS4 Aggregator ASN');
#TODO test setters

##Type unknown
##TODO

###Testing misc pieces

##Testing Safi
=comment
my $safiMessage = BGPmon::Message->new(string=>$xml6);
my $safi = $safiMessage->get_safi();
is($safi, 1, "Testing MP_REACH_NLRI safi 1");
my $safiMessage2 = BGPmon::Message->new(string=>$xml6safi2);
my $safi2 = $safiMessage2->get_safi();
is($safi2, 2, "Testing MP_REACH_NLRI safi 2");
=cut
##Testing get octets
my $octets = BGPmon::Message->new(string=>$table_start);
my $os = $octets->get_octets();
ok(!defined($os), "Testing get octets where there are none");
$octets = BGPmon::Message->new(string=>$aggaggagg);
my $os2 = $octets->get_octets();
is($os2, 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0081020000006A400101004002100207730900AE19351293269DDA105BA0400600C007065BA073F86F71E0111A0206000000AE00001935000012930000269D0000DA10000203CBE01208000203CB73F86F71800E1C00020110200115A8000A000A0000000000000002003020010DF00264', "Testing get octets");

##Testing get type
my $typemsg = BGPmon::Message->new(string=>$table_start);
is($typemsg->get_type(), undef, "Testing get type where there are none");
$typemsg = BGPmon::Message->new(string=>$aggaggagg);
is($typemsg->get_type(), 'LIVE', "Testing get type");
is($typemsg->get_collection_method(), 'LIVE', "Testing get collection method");
#TODO test setters

##Testing get meatadata
my $metamsg = BGPmon::Message->new(string=>$table_start);
is($metamsg->get_metadata_nodes(), undef, "Testing get metadata where there is none");
$metamsg = BGPmon::Message->new(string=>$withmsg);
my $meta = $metamsg->get_metadata_nodes();
ok($meta, "Testing get metadata");
is(scalar(@{$meta}), 1, 'Testing size of metadata');
is($meta->[0]->{'NODE_PATH'}, '//bgp:UPDATE["205.89.160.0/20"]/bgp:WITHDRAW', "Testing get metadata node path");
is($meta->[0]->{'ANNOTATION'}, 'WITH', "Testing get metadata annotation");
#TODO test setters



#testing validation flag
##Testing get meatadata
my $tempmsg = BGPmon::Message->new(string=>$xml, validation=>0);
is($tempmsg->get_source_address(), '94.126.183.247', "Testing message with validation turned off");



done_testing();
