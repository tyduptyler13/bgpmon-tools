#!/usr/bin/perl
# *
# *
# *      Copyright (c) 2012 Colorado State University
# *
# *      Permission is hereby granted, free of charge, to any person
# *      obtaining a copy of this software and associated documentation
# *      files (the "Software"), to deal in the Software without
# *      restriction, including without limitation the rights to use,
# *      copy, modify, merge, publish, distribute, sublicense, and/or
# *      sell copies of the Software, and to permit persons to whom
# *      the Software is furnished to do so, subject to the following
# *      conditions:
# *
# *      The above copyright notice and this permission notice shall be
# *      included in all copies or substantial portions of the Software.
# *
# *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# *      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# *      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# *      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# *      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# *      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# *      FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# *      OTHER DEALINGS IN THE SOFTWARE.
# *
# *
# *  File: bgpmon-rib-archiver.pl
# *  Authors: Kaustubh Gadkari
# *  Date: Feb. 12, 2014
# *
# *

use strict;
use warnings;
use Getopt::Long;
use POSIX qw(strftime setsid getpid);
use File::Path qw/make_path/;
use BGPmon::Log ':all';
use BGPmon::Fetch::Live;
use BGPmon::Message;
use BGPmon::Configure;
use Cwd;
use Fcntl 'SEEK_END';
use DateTime;
use File::Copy qw/move/;

our $VERSION = '2.0';

# SAFI of -255 implies XML message.
use constant XML_SAFI => -255;

# Use a special peer IP of -1 for status messages.
use constant STATUS_PEER => "-1";

#---- Default settings ----
# These settings are used if the user does not specify the values
# via either the config file or the command line.
use constant DEFAULT_RIB_PORT => 50002;
use constant DEFAULT_SERVER => 'bgpdata-test.netsec.colostate.edu';
use constant DEFAULT_LOG_LEVEL => BGPmon::Log::LOG_WARNING;
use constant DEFAULT_RETRY_INTERVAL => 30;

$| = 1;

#---- Get program name. ----
my $prog_name = $0;

#---- Get pid. ----
my $pid = getpid();

#---- Get current working directory. ----
my $cwd = getcwd();

#---- Global variables. ----
my $debug = 1;

my %txt_files;
my %xml_files;
my %file_fhs;

##--- Current output directory. ---
my $curr_dir_path = "";

##--- Variables for logging ---
my $log_level;
my $use_syslog;
my $log_file;

#---- BGPmon variables ----
my $server;
my $port;

#---- Archiver config variables ----
my $out_dir;
my $peer_out_dir;
my $config_file;
my $archive_bgpdump;
my $status_file;
my $status_fh;
my $use_stdout_for_status = 0;
my $is_daemon;
my $retry_interval;
my $no_pid;

#---- Hash to store config options. ----
my %config;

#---- Archiver status variables. ----
my $messages_read = 0;
my $messages_archived = 0;
my $curr_messages_archived = 0;
my %all_peer_pairs;

#---- BEGIN main ----

# Set signal handlers.
$SIG{INT} = $SIG{TERM} = $SIG{KILL} = $SIG{HUP} = \&archiver_exit;
$SIG{PIPE} = 'ignore';

#---- Get the command line options. ----

my @params = (
    {# Read from the configuration file if there is one
        Name  => BGPmon::Configure::CONFIG_FILE_PARAMETER_NAME,
        Type  => BGPmon::Configure::FILE,
        Default => undef,
        Description => "Filename of the configuration file.",
    },
    {# Connect to livebgp.netsec.colostate.edu by default.
        Name => "server",
        Type => BGPmon::Configure::STRING,
        Default => DEFAULT_SERVER,
        Description => "This is the BGPmon server address",
    },
    {# Connect to the RIB stream by default
        Name => "port",
        Type => BGPmon::Configure::PORT,
        Default => DEFAULT_RIB_PORT,
        Description => "This is the BGPmon server port number",
    },
    {# Set the retry interval to 30s by default
        Name => "retry_interval",
        Type => BGPmon::Configure::UNSIGNED_INT,
        Default => DEFAULT_RETRY_INTERVAL,
        Description => "Interval in seconds for retrying connections to BGPmon server",
    },
    {# The default logging level is LOG_WARNING.
        Name => "log_level",
        Type => BGPmon::Configure::UNSIGNED_INT,
        Default => DEFAULT_LOG_LEVEL,
        Description => "This is how verbose the user wants the log to be",
    },
    {# Do not use syslog by default.
        Name => "use_syslog",
        Type => BGPmon::Configure::BOOLEAN,
        Default => 0,
        Description => "If syslog should be used instead of another file",
    },
    {# No log file by default - will print to stdout if not
        Name => "log_file",
        Type => BGPmon::Configure::FILE,
        Default => undef,
        Description => "This is the location the log file will be saved",
    },
    {# Unless specified, do not archive bgpdump format messages.
        Name => "archive_bgpdump",
        Type => BGPmon::Configure::BOOLEAN,
        Default => 0,
        Description => "If bgpdump format should be archived alongside XML.",
    },
    {# Set default output directory. If not specified at cmd line
        # or config file, set to ./archives/date.
        Name => "out_dir",
        Type => BGPmon::Configure::STRING,
        Default => "$cwd/archives/date",
        Description => "Where the archives sorted by time should be stored.",
    },
    {# Set default output directory for per peer archives.
        # If not specified at cmd line or config file, set to ./archives/peers
        Name => "peer_out_dir",
        Type => BGPmon::Configure::STRING,
        Default => "$cwd/archives/peers",
        Description => "Where the archives sorted by peers should be stored.",
    },
    {# Set the path for the archiver to store status messages.
        Name => "status_file",
        Type => BGPmon::Configure::FILE,
        Default => "$cwd/archiver_status",
        Description => "Where to store status messages.",
    },
    {# See if we check the PID
        Name => "no_pid",
        Type => BGPmon::Configure::BOOLEAN,
        Default => 0,
        Description => "If the PID should not be checked",
    },
    {# Daemonize the program?
        Name => "daemonize",
        Type => BGPmon::Configure::BOOLEAN,
        Default => 0,
        Description => "If bgpmon-archiver.pl should be a daemon or a user applicaiton",
    },
    {# Push debug information
        Name => "debug",
        Type => BGPmon::Configure::BOOLEAN,
        Default => 0,
        Description => "If the user wants debug information",
    });

#Checking that everything parsed correctly
if(BGPmon::Configure::configure(@params) ) {
    print BGPmon::Configure::get_error_code("configure").": ".
    BGPmon::Configure::get_error_message("configure")."\n";
    exit 1;
}

#Setting our configurations
$config{server} = BGPmon::Configure::parameter_value('server');
$config{port} = BGPmon::Configure::parameter_value('port');
$retry_interval = BGPmon::Configure::parameter_value('retry_interval');
$config{log_level} = BGPmon::Configure::parameter_value('log_level');
$config{use_syslog} = BGPmon::Configure::parameter_value('use_syslog');
$config{log_file} = BGPmon::Configure::parameter_value('log_file');
$config{archive_bgpdump}=BGPmon::Configure::parameter_value('archive_bgpdump');
$config{out_dir} = BGPmon::Configure::parameter_value('out_dir');
$config{peer_out_dir} = BGPmon::Configure::parameter_value('peer_out_dir');
$config{status_file} = BGPmon::Configure::parameter_value('status_file');
$config{no_pid} = BGPmon::Configure::parameter_value('no_pid');
$config{port} = DEFAULT_RIB_PORT;
$is_daemon = BGPmon::Configure::parameter_value('daemonize');
$debug = BGPmon::Configure::parameter_value('debug');

#Printint out settings for debugging purposes
if($debug){
    print "Server: $config{server}\n";
    print "Port: $config{port}\n";
    print "Log Level: $config{log_level}\n";
    print "Use Syslog: $config{use_syslog}\n";
    print "Log file: $config{log_file}\n" if defined $config{log_file};
    print "Log file: <NONE>\n" if not defined $config{log_file};
    print "BGPdump: $config{archive_bgpdump}\n";
    print "Out dir: $config{out_dir}\n";
    print "Peer dir: $config{peer_out_dir}\n";
    print "Status file: $config{status_file}\n";
    print "No PID: $config{no_pid}\n";
    print "Retry Interval: $retry_interval\n";
    print "Daemon: $is_daemon\n";
}


#---- Initialize the log. ----
if (BGPmon::Log::log_init(use_syslog => $config{use_syslog},
        log_level => $config{log_level},
        log_file => $config{log_file},
        prog_name => $prog_name) != 0) {
    my $err_msg = BGPmon::Log::get_error_message('log_init');
    print STDERR "Error initializing log: $err_msg\n";
    exit 1;
}

#---- Open the archiver status file. If there is an error opening the
# status file, archiver will log status messages to STDOUT. ----
unless (open($status_fh, ">>$config{status_file}")) {
    if (defined($is_daemon) && $is_daemon == 1) {
        BGPmon::Log::log_err("Could not open status file for writing. Exiting.");
        BGPmon::Log::log_close();
        exit 1;
    } else {
        BGPmon::Log::log_warn("Could not open status file for writing. Writing ".
            "status messages to STDOUT.");
        $status_fh = *STDOUT;
        $use_stdout_for_status = 1;
    }
}

#---- Daemonize if needed. ----
if (defined($is_daemon) && $is_daemon == 1) {
    daemonize();
} else {
    write_pid_file();
}

#---- Print status message to archiver status file. ----
write_status_message("STARTED:");

#---- Connect to the BGPmon instance. ----
BGPmon::Log::log_info("Connecting to BGPmon server at
    $config{server} port $config{port}");
my $conn = BGPmon::Fetch::Live->new(server => $config{server},
    port => $config{port});
my $ret = $conn->activate();
unless ($ret == 0) {
    my $err_msg = $conn->get_error_msg('connect_bgpdata');
    BGPmon::Log::log_fatal(
        "Could not connect to BGPmon server at $config{server} port ".
        "$config{port}: $err_msg");
}

#---- Print status message to archiver status file. ----
write_status_message("CONNECTED: server=$config{server} port=$config{port}");

#---- Start a read forever loop to receive data from BGPmon. ---
while (1) {
    my $err_msg = "";
    my $xml_message = $conn->read_xml_message();
    my $retval;

    # Check if we got a XML message.
    # If not, log warning and try to re-establish connection.
    unless (defined($xml_message)) {
        $err_msg = $conn->get_error_msg('read_xml_message');
        BGPmon::Log::log_warn("Error reading XML message from BGPmon: $err_msg");
        write_status_message("STOPPED: Failed to read XML message");

        #Checking to see if there was just an error reading a message or if the
        #socket has an error.
        if($conn->is_connected()){
            next;
        }

        while ($conn->activate() != 0){
            $err_msg = $conn->get_error_msg('connect_bgpdata');
            BGPmon::Log::log_info(
                "Could not reconnect to BGPmon: $err_msg. Sleeping ".
                "$retry_interval seconds.");
            sleep($retry_interval);
            if ($retry_interval < 120) {
                $retry_interval *= 2;
            } else {
                $retry_interval = 120;
            }
        }
        BGPmon::Log::log_info("Reconnected to BGPmon ($config{server}, ".
            "$config{port})");
        # Print status message to archiver status file.
        write_status_message("CONNECTED: server=$config{server} ".
            "port=$config{port}");
        next;
    }

    $messages_read++;
    message_handler($xml_message);
}

sub message_handler {
    my $xml_message = shift;
    my $retval;
    # Create message object and parse it.
    my $msg = BGPmon::Message->new(string => $xml_message, RIB => 1);
    unless (defined($msg)) {
        BGPmon::Log::log_err("Error converting XML message to Message object.");
        BGPmon::Log::debug($xml_message) if ($debug == 1);
        return;
    }
    my $peer_ip = $msg->get_source_address();
    my $monitor_ip = $msg->get_monitor_address();
    # Sanity check of peer-monitor pair.
    if (defined $peer_ip and exists($all_peer_pairs{$peer_ip})) {
        my @all_monitors = split(/\,/, $all_peer_pairs{$peer_ip});
        unless (defined $monitor_ip and $monitor_ip ~~ @all_monitors) {
            BGPmon::Log::log_err(
                "Monitor changed for peer: $peer_ip
                Expected monitor(s): $all_peer_pairs{$peer_ip}
                Saw monitor: $monitor_ip.");
            $all_peer_pairs{$peer_ip} =
                join(',', $all_peer_pairs{$peer_ip}, $monitor_ip);
        }
    } else {
        $all_peer_pairs{$peer_ip} = $monitor_ip;
    }

    my $status = $msg->get_status();
    my $ts = $msg->get_timestamp();

    my $msg_string = $msg->to_string(remove_xml_decl => 1, pretty_print => 0);
    unless (defined($msg_string)) {
        BGPmon::Log::log_err("Error converting XML message to string.");
        BGPmon::Log::debug($xml_message) if ($debug == 1);
        next;
    }

    # If the message is a table start message, open a new file.
    if (defined($status)) {
        if ($status eq "TABLE_START") {
            # Open new files - we have a new peer.
            BGPmon::Log::debug("TABLE_START seen for peer $peer_ip.") if ($debug == 1);
            my $file_name = get_file_name($peer_ip, $ts, "xml");
            open (my $file_handle, ">", $file_name) or
                BGPmon::Log::log_err("Error opening file $file_name: $!");

            # Add file handle to hash.
            $xml_files{$peer_ip} = $file_name;
            $file_fhs{$file_name} = $file_handle;

            # Write opening <xml> tag to file.
            my $retval = syswrite($file_handle, "<xml>\n");
            if (!defined($retval) || $retval == 0) {
                BGPmon::Log::log_err("Error writing opening xml tag to file: $!");
                delete($file_fhs{$file_name});
                close($file_handle);
            }

            # Open a bgpdump file if required.
            if ($config{archive_bgpdump} == 1) {
                $file_name = get_file_name($peer_ip, $ts, "bgpdump");
                open (my $txt_file_handle, ">", $file_name) or
                    BGPmon::Log::log_err("Error opening file $file_name: $!");
                # Add to hash.
                $txt_files{$peer_ip} = $file_name;
                $file_fhs{$file_name} = $txt_file_handle;
            }
        } elsif ($status eq "TABLE_STOP") {
            # Close files for the given peer.
            if (exists($xml_files{$peer_ip})) {
                write_status_message("CLOSING: Files for peer $peer_ip\n");
                $retval = syswrite($file_fhs{$xml_files{$peer_ip}}, $msg_string);
                if (!defined($retval) || $retval == 0) {
                    BGPmon::Log::log_err("Error writing bgpdump message to file: $!");
                }
                close_file($xml_files{$peer_ip});
                delete($xml_files{$peer_ip});

                if ($config{archive_bgpdump} == 1) {
                    close_file($txt_files{$peer_ip});
                    delete($txt_files{$peer_ip});
                }
            }
        } elsif ($status =~ /CHAINS_STATUS/ ||
            $status =~ /QUEUES_STATUS/ ||
            $status =~ /SESSION_STATUS/ ||
            $status =~ /MRT_STATUS/ ||
            $status =~ /BGPMON_START/ ||
            $status =~ /BGPMON_STOP/) {
            BGPmon::Log::debug("Received status message: $msg_string\n")
                if $debug == 1;
            # Set the peer IP to the STATUS peer.
            $peer_ip = STATUS_PEER;

            # Open a new status file if required.
            my $file_name = get_file_name($peer_ip, $ts, "status");
            open (my $status_fh, ">>$file_name") or
               BGPmon::Log::log_err
                    ("Error opening file for BGPmon status messages: $!");
            $retval = syswrite($status_fh, $msg_string);
            if (!defined($retval) || $retval == 0) {
                BGPmon::Log::log_err
                    ("Error writing BGPmon status message to file: $!");
                BGPmon::Log::debug
                    ("Error writing $msg_string to file.")
                        if ($debug == 1);
            }
            close($status_fh);
        }
    }

    if (defined($peer_ip) and exists($xml_files{$peer_ip})) {
        $retval = syswrite($file_fhs{$xml_files{$peer_ip}}, $msg_string);
        if (!defined($retval) || $retval == 0) {
            BGPmon::Log::log_err("Error writing XML message to file: $!");
            BGPmon::Log::debug("Error writing $xml_message to file.")
                if ($debug == 1);
        }
        if ($config{archive_bgpdump} == 1 && !defined($status)) {
            my $bgpdump_msg = $msg->to_string(format => 'txt');
            unless (defined($bgpdump_msg)) {
                BGPmon::Log::log_err
                    ('Error converting XML message to bgpdump format.');
                BGPmon::Log::debug($xml_message) if ($debug == 1);
                next;
            }
            if (exists($txt_files{$peer_ip})) {
                foreach my $safi (keys $bgpdump_msg) {
                    my $retval = syswrite
                        ($file_fhs{$txt_files{$peer_ip}},
                            $bgpdump_msg->{$safi});
                    if (!defined($retval) || $retval == 0) {
                        BGPmon::Log::log_err
                            ("Error writing bgpdump message to file: $!");
                        BGPmon::Log::debug
                            ("Error writing $bgpdump_msg to file.")
                                if ($debug == 1);
                    }
                }
            }
        }
    }
}

#---- END main ----

#---- SIGINT Signal Handler ----
sub archiver_exit {
    foreach my $xml_fname (keys %xml_files) {
        close_file($xml_fname);
    }

    foreach my $txt_fname (keys %txt_files) {
        close_file($txt_fname);
    }

    # Close connection to BGPmon
    $conn->deactivate();

    # Close the log.
    BGPmon::Log::log_close();

    # Print archiver status message that we are now stopped.
    write_status_message("STOPPED:");

    # Close the archiver status file.
    if ($use_stdout_for_status != 1) {
        close($status_fh);
    }

    # Delete PID file.
    my $pid_file = "/tmp/bgpmon-archiver/archiver.pid";
    if (-e $pid_file) {
        unlink($pid_file);
        if ($!) {
            print "Error deleting PID file $pid_file: $!\n";
        }
    }

    exit 0;
}

#---- Write PID file. ----
sub write_pid_file {
    if ($config{no_pid}) {
        return;
    }

# Get PID.
    my $pid = getpid();

# Set state directory.
    my $state_dir;
    if (defined($ENV{'ARCHIVER_STATE'})) {
        $state_dir = $ENV{'ARCHIVER_STATE'};
    } else {
        $state_dir = "/tmp/bgpmon-archiver";
    }

# Open pid file and write pid. Close file after writing pid.
    my $pid_file = "$state_dir/archiver.pid";
    my $pid_file_fh;
    if (open ($pid_file_fh, ">$pid_file")) {
        my $retval = syswrite($pid_file_fh, "$pid\n");
        if (!defined($retval) || $retval == 0) {
            print $pid;
        }
        close($pid_file_fh);
    } else {
        print "Could not open PID file $pid_file. Archiver PID is $pid.\n";
    }
}

#---- Put the archiver in daemon mode. ----
sub daemonize {
# Fork and exit parent. Makes sure we are not a process group leader.
    my $pid = fork;
    exit 0 if $pid;
    exit 1 if not defined $pid;

    # Become leader of a new session, group leader of new
    # process group and detach from any terminal.
    setsid();
    $pid = fork;
    exit 0 if $pid;
    exit 1 if not defined $pid;

    # Change directory to / so that we don't block this filesystem.
    chdir '/' or die ("Could not chdir to /: $!");

    # Clear umask for file creation.
    umask 0;

    # Write pid file.
    write_pid_file();
}

#---- Write a status message to archiver status file. ----
sub write_status_message {
    my $msg = shift;
    my $header = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime());
    my $status_msg = "$header $msg\n";
    my $retval = syswrite($status_fh, $status_msg);
    if (!defined($retval) || $retval == 0) {
        BGPmon::Log::log_err(
            "Could not write status message to status file: $status_msg");
    }
}

#---- Generate a new file name. Create appropriate paths if necessary ----
sub get_file_name {
    my ($peer_ip, $time_stamp, $type) = @_;

    # Set the base directory name to YYYY.MM.
    my $dir_name = strftime("%Y.%m/%d", gmtime($time_stamp));
    BGPmon::Log::debug("New directory name is $dir_name.") if $debug;

    # Make the RIBS directory.
    my $dir_path = join("/", $config{out_dir}, $dir_name);
    $dir_path = join("/", $dir_path, "RIBS");

    make_dir($dir_path);
    BGPmon::Log::debug("Created directory $dir_path.") if $debug;

    # Set the current working directory to the newly created directory.
    $curr_dir_path = $dir_path;

    # Generate new XML file name.
    my $ts;
    if ($peer_ip eq STATUS_PEER) {
        $ts = strftime('%Y%m%d', gmtime($time_stamp));
        $ts .= ".0000";
    } else {
        $ts = strftime('%Y%m%d.%H%M', gmtime($time_stamp));
    }

    my $file_name = "";

    # Check if this is a status file or a update/rib file.
    if ($peer_ip eq "-1") {
        $file_name = join(".", "status", $ts);
    } else {
        $file_name = join(".", "ribs", $ts);
    }

    # Append peer ip and file type to file name.
    # Append incomplete to the file name.
    # This will be removed later when the file is closed.
    if ($peer_ip ne STATUS_PEER) {
        $file_name .= ".$peer_ip";
    }
    if ($peer_ip eq STATUS_PEER) {
        $type = "xml";
    }
    $file_name .= ".$type";
    if ($peer_ip ne STATUS_PEER) {
        $file_name .= ".incomplete";
    }

    # Return the new file name.
    BGPmon::Log::debug("New file name is $curr_dir_path/$file_name") if $debug;
    return "$curr_dir_path/$file_name";
}

#---- Given a directory path, create a new directory. ----
sub make_dir {
    my $dir_path = shift;

# Create output directory if required.
    unless (-d $dir_path) {
        BGPmon::Log::debug("Creating new output directory $dir_path.") if $debug;
        make_path($dir_path, {error => \my $err, mode => 0755, });
        if (@$err) {
            for my $diag (@$err) {
                my ($file, $message) = %$diag;
                if ($file eq '') {
                    BGPmon::Log::log_fatal(
                        "General error creating output directory $dir_path: $message\n");
                } else {
                    BGPmon::Log::log_fatal(
                        "Error creating output directory $dir_path: $message\n");
                }
            }
            return -1;
        }
    }
    return 0;
}

#---- Close a given file name, after checking for the right interval. ----
sub close_file {
    my $file = shift;
    my $final_ret = 0;

    my @file_elems = split(/\//, $file);
    my $file_name = $file_elems[-1];
    my $file_path = join("/", @file_elems[0..$#file_elems - 1]);

    # Write trailing </xml> tag to XML files.
    if ($file =~ /xml/) {
        my $retval = syswrite($file_fhs{$file}, "</xml>\n");
        if (!defined($retval) || $retval != 7) {
            BGPmon::Log::log_warn('Error writing closing "</xml>" tag to file.');
        }
    }

    # Close file associated with this file name. Delete hash entry.
    close($file_fhs{$file});
    delete($file_fhs{$file});

    # Print status message to archiver status file.
    write_status_message("CLOSING: file $file_name");

    # Remove trailing .incomplete from file name.
    my @file_name_elems = split(/\./, $file_name);
    pop @file_name_elems;
    my $new_file_name = join(".", @file_name_elems);

    move($file, "$file_path/$new_file_name");
    $file = "$file_path/$new_file_name";

    # Get the peer information from the file name.
    my $peer_ip = "peer ip";
    if (scalar(@file_name_elems) == 5) {
    # IPv6 peer address.
        $peer_ip = $file_name_elems[3];
    } elsif (scalar(@file_name_elems) == 8) {
    # IPv4 peer address.
        $peer_ip = join(".", @file_name_elems[3, 4, 5, 6]);
    } elsif (scalar(@file_name_elems) == 4) {
    # Status file has peer name of status.
        $peer_ip = "status";
    }

    # Get timestamp from the file name.
    $file_name_elems[1] =~ /(\d\d\d\d)(\d\d)(\d\d)/;
    my ($yr, $mon, $d) = ($1, $2, $3);
    $file_name_elems[2] =~ /(\d\d)(\d\d)/;
    my ($hr, $m) = ($1, $2);
    my $dt = DateTime->new(
        year => $yr,
        month => $mon,
        day => $d,
        hour => $hr,
        minute => $m,
        time_zone => "GMT",
    );
    # Make output directory.
    my $dir_name = strftime("%Y.%m", gmtime($dt->epoch));
    BGPmon::Log::debug("New directory name is $dir_name.") if $debug;

    # Make the RIBS directory.
    my $dir_path = join("/", $config{peer_out_dir}, $peer_ip, $dir_name);
    $dir_path = join("/", $dir_path, "RIBS");

    # Make output directory if required.
    make_dir($dir_path);

    # Create symlink for current file in the appropriate path.
    my $link = $new_file_name;
    unless (-e "$dir_path/$link") {
        if (symlink($file, "$dir_path/$link") != 1) {
            BGPmon::Log::log_warn(
                "Error creating symlink to file $file in $dir_path/$link: $!");
            $final_ret = 1;
        }
    }
    return $final_ret;
}
