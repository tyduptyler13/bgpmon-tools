#!/bin/bash

#Detect where the script is.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Start the server regardless of where we call this script from.
perl $DIR/demo.pl | node --debug $DIR/../etc/node/server.js

