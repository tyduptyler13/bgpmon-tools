use strict;
use warnings;

use BGPmon::Fetch::Live;
use BGPmon::Stats::Manager;
use BGPmon::Stats;

my $fetch = new BGPmon::Fetch::Live(
	server => 'bgpdata-test.netsec.colostate.edu',
	port   => 50001
);

my $manager = new BGPmon::Stats::Manager( fetch => $fetch );

my $stat = new BGPmon::Stats(
	name => 'Stats',
	description => 'This is a statistic based on default options.'.
	' Currently we are reading from bgpdata-test.'.
	' This stat contains per peer totals and several global bgp statistics.'
);

$manager->addStat($stat);

print STDERR "Starting server. Use crtl + c to exit.\n";

$manager->start();

print STDERR "Finished.\n";
