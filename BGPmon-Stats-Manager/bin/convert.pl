#!perl
use warnings;
use strict;

use JSON;
use Time::HiRes qw (time);

my $header = 0;
my @list;

while (my $line = <STDIN>){
    
    my $json = from_json($line);

    if (defined $json->{data}){

        if ($header == 0){
        
            print "time\t";
            
            foreach (keys $json->{data}){
                push(@list, $_);
                print $_ . "\t";
            }
            print "\n";

            $header = 1;
        }

        print time . "\t";

        foreach (@list){
            print $json->{data}->{$_} . "\t";
        }

        print "\n";

    }

}

