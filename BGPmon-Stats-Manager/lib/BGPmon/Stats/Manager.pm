package BGPmon::Stats::Manager;

use strict;
use warnings;
use 5.14.2;

use Time::HiRes qw(gettimeofday);

use JSON;

use BGPmon::Message;
use BGPmon::Fetch;
use BGPmon::Stats;

use Data::Dumper;

our $VERSION = '1.0.4';

#Constants
use constant TRUE  => 1;
use constant FALSE => 0;

=head1 NAME

BGPmon::Stats::Manager

This is a module designed to manage a stat object collection. It uses a Fetch object
and parses messages into Message objects.

=head1 SYNOPSIS

This is experimental and may not work yet.

    use BGPmon::Stats::Manager;
    use BGPmon::Fetch::Live;

    my $fetch = new BGPmon::Fetch::live(server=>'bgpdata3.netsec.colostate.edu', port=>50001);
    my $manager = new BGPmon::Stats::Manager($fetch);


=head1 SUBROUTINES/METHODS

=head2 new

Creates a new Stats Manager that will consume messages from the Fetch
module passed into it.

Parameters:
* fetch - Any fetch object.
* interval - Update frequency for messages (seconds). (Default: 5)

If a problem occurs, this will return undefined.

This clears the file every time it is started!

=cut

sub new {

	my $class = shift;

	my $self = { interval => 1 };

	$self->{stats} = [];
	$self->{cache} = {};

	my %args = @_;

	if ( exists( $args{fetch} ) && $args{fetch}->isa('BGPmon::Fetch') ) {
		$self->{fetch} = $args{fetch};
	} else {
		print "Error: Fetch is a required parameter.";
		return undef;
	}

	if ( exists( $args{interval} ) ) {
		$self->{interval} = $args{interval};
	}

	bless $self, $class;
	return $self;

}

=head2 addStat

This adds a Stat object to the Manager. It will then have data added/changed
inside of it based on the incoming stream of the engine.

Parameters:
Stat - Any stat object

Returns:
TRUE  - success
FALSE  - Failed

=cut

sub addStat {

	my ( $self, $stat ) = @_;

	my $stats = $self->{stats};

	if ( $stat ~~ $stats ) {    #contains
		return FALSE;           #Exists in list already.
	} else {
		push( $stats, $stat );
		return TRUE;
	}

}

=head2 injest

Injests a message into all stats objects.

    $man->injest($message);

=cut

sub injest {

	my ( $self, $message ) = @_;

	my @stats = @{ $self->{stats} };

	foreach (@stats) {
		$_->injestMessage($message);
	}

}

=head2 start

Start is a function that begins the data collection and the hosting of
the stats file. 

This call blocks until the process is killed or until the fetch stream closes.

=cut

sub start {

	my $self = shift;

	#Fetch only exists on this thread.
	my $fetch = $self->{fetch};

    #The max time the socket can wait is the length of our interval.
    #This makes it so the worst lag that can occure is 2 intervals.
    $fetch->set_timeout($self->{interval});

	if ( not $fetch->is_activated() ) {
		$fetch->activate();
		if ( not $fetch->is_activated() ) {
			print STDERR "[ERROR] Fetch failed to activate\n";
			print STDERR Dumper($fetch);
			die;
		}
	}

	#Stats are printed on STDOUT.
	foreach ( @{ $self->{stats} } ) {

		my $json = to_json(
			{
				type        => "definition",
				name        => $_->name,
				description => $_->description,
				id          => $_->id
			}
		) . "\n";

		print $json;

	}

	#Setup event loops.

	autoflush STDOUT 1;
    
    my $tick = sub {

		foreach ( @{ $self->{stats} } ) {

			my $stat = $_;

			my $data = $stat->getData();

			my $hash = {
				id   => $stat->id,
				type => "update",
				data => {}
			};

			if ( not exists( $self->{cache}->{ $stat->id } ) ) {

			    #We haven't read this stat before. Its values
				#are what we report.

				my $cache = $self->{cache}->{ $stat->id } = {};

				foreach ( keys $data ) {
					$hash->{data}->{$_} = $cache->{$_} = $stat->get($_);
				}

            } else {

		        #We have seen it before and we need to first consult the cache.

				my $cache = $self->{cache}->{ $stat->id };

				foreach ( keys $data ) {

					#Calculate the difference between then and now.
					$hash->{data}->{$_} = $stat->get($_) - $cache->{$_};

					#Update the then to be now.
					$cache->{$_} = $stat->get($_);

				}
						
				#TODO peers.

			}

			print to_json($hash) . "\n";

		}

    };

	#Main loop

    my $nextTime = gettimeofday + $self->{interval};

	while ( $fetch->is_activated() ) {

		my $xml = $fetch->read_xml_message();
			
		#When xml is not defined, the timeout was reached and we are just forcing a tick.
		if (defined $xml){

			my $msg = new BGPmon::Message( 'string' => $xml );

            if (defined $msg){

			    $self->injest($msg);

            } else {

                print STDERR "Message could not be parsed! Text is as follows: \"". $xml ."\"\n";

            }
	
       	}

        if ((my $time = gettimeofday) > $nextTime){
            $tick->();
            $nextTime = $time + $self->{interval};
        }

	}

	print STDERR "[WARN] Fetch closed early. Stopped server.\n";

}

=head1 Client interface (JSON)

This object creates a socket and will stream data to all peers
that wish to listen.

Warning: This program must not wait on your programs and therefore
will not wait for your buffers. You must read data as fast as it
comes to you or you may lose data. The data flow is very small but
will still not wait.

The socket will be fed json objects separated by newlines.

=head2 Description of json objects:

Define stat:

Appears when a stat is created or when the stream starts.

    {
        'type': 'definition',
        'name': 'some name',
        'description': 'description of stat',
        'id': 'unique id of stat'
    }

Stat update:

Appears whenever the client sends an update. (Default of 1 sec interval)

    {
        'type': 'update',
        'id': 'unique id of stat',
        'data': {
            'total': 'change in total since last update'
        }
    }

The data will change as more information is added to BGPmon::Stat

See BGPmon::Stat for all data values.

=cut

