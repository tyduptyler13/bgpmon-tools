/**
This example create a single example connection from the east coast to the UK. The traffic
is a simulated normal distribution of traffic that changes color based on this artificial
speed. The example is still missing key features and is only a proof of concept. The code
below could support many connection displays and multiple hop points as one connection.

The code is still in development and can be expanded to include statistics on mouseover,
more details in the info display, and Event driven updates. More documentation is available on request.
 */

var map; //Global map.

function initialize() {
	var mapOptions = {
			center: new google.maps.LatLng(37, -59),
			zoom: 2,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map($("#map-canvas")[0], mapOptions);

	nv = new NetworkVisualizer(map);
	dataDemo(nv);
}

$(initialize);

function dataDemo(nv) {
	var connection = new NetworkConnection({
		speed: 1000,
		path: [new google.maps.LatLng(40.027, -77.519),
		       new google.maps.LatLng(52.207, -0.966)]
	});
	nv.addConnection(connection);
	nv.draw();

	setInterval(function () {
		connection.setSpeed(rnd(1000, 500));
		nv.draw();
	}, 1000);
}

//The code below is a custom written library for handling the display.

NetworkVisualizer = function (map) {
	this.map = map;
	this.connections = {};
};
NetworkVisualizer.prototype.constructor = NetworkVisualizer;

NetworkVisualizer.prototype.addConnection = function (connection) {
	this.connections[connection.id] = connection;
};

NetworkVisualizer.prototype.removeConnection = function (connection) {
	connection.destroy();
	this.connections[connection.id] = null;
};

NetworkVisualizer.prototype.draw = function () {
	var connection;
	for (connection in this.connections) {
		try {
			this.connections[connection].getLargestSpeed();
		} catch (e) {}
	}

	for (connection in this.connections) {
		try {
			this.connections[connection].draw(this.map);
		} catch (e) {}
	}
};

NetworkConnection = function (data) {
	this.id = NetworkConnection.idCount++;
	this.speed = (exists(data.speed)) ? data.speed : 0;
	this.path = (exists(data.path)) ? data.path : [];
	this.line = null;
	this.info = new google.maps.InfoWindow({
		content: "<h3>Network Demo</h3><p>This would normally be filled with data " +
		"pertaining to the endpoints and some statistics. This example is incomplete.</p>",
		maxWidth: 400
	});
	this.needsUpdate = false;
};
NetworkConnection.prototype = {
		constructor: NetworkConnection,
		setSpeed: function (speed) {
			this.speed = speed;
			this.needsUpdate = true;
		},
		draw: function (map) {
			if (this.path.length < 2) return;
			if (this.line === null) {
				this.line = new google.maps.Polyline({
					path: this.path,
					geodesic: true,
					strokeColor: this.getColor(),
					strokeOpacity: 1,
					strokeWeight: 2
				});
				this.line.setMap(map);
				var scope = this;
				google.maps.event.addListener(this.line, 'click', function () {
					scope.info.setPosition(scope.path[0]);
					scope.info.open(map);
				});
			} else if (this.needsUpdate) {
				this.line.set("strokeColor", this.getColor());
				needsUpdate = false;
			}
		},
		getLargestSpeed: function () {
			if (this.speed > NetworkConnection.largestSpeed) {
				NetworkConnection.largestSpeed = this.speed;
			}
		},
		destroy: function () {
			this.line.setMap(null);
		},
		getColor: function () { //This can be improved.
			var val = iMap(this.speed, NetworkConnection.largestSpeed, 0, 255);
			return "#" + val.toString(16) + "0000";
		}
};
NetworkConnection.idCount = 0;
NetworkConnection.largestSpeed = 0;
NetworkConnection.resetSpeed = function () {
	NetworkConnection.largestSpeed = 0;
};

function exists(d) {
	return (typeof d != "undefined");
}

//Avoid conflicts with map. This function scales values.
function Map(actual, scale, min, max) {
	return (actual / scale) * (max - min) + min;
}

//This function does the same as Map except rounds the value to an integer.
function iMap(actual, scale, min, max) {
	return Math.round(Map(actual, scale, min, max));
}

function rnd_snd() {
	return (Math.random() * 2 - 1) + (Math.random() * 2 - 1) + (Math.random() * 2 - 1);
}

//Normal distribution random
function rnd(mean, stdev) {
	return Math.round(rnd_snd() * stdev + mean);
}