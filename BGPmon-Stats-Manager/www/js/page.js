
$(function(){

	var stats = {};

	var notice = function(message, overlay){

	};

	$(document).on('click', '.collapse', function(){
		var target = $(this);
		target.toggleClass('flip');
		target.siblings('.content').slideToggle();
	});

	$(document).on('click', 'label:not([for])', function(){
		var input = $(this).prev();
		input.prop("checked", !input.prop("checked"));
		input.change();
	});

	$('#side .button').click(function(){
		var ele = $(this);

		if (ele.hasClass('active')){ //Disable all elements.

		} else if (ele.hasClass('inactive')){ //Enable all elements.

		} else { //Elements can't be changed.

		}

	});

	var socket = io(window.location.hostname + ':5000');

	socket.on('connect', function(){

		//Remove loading window or lost connection windows.
		$('#loading, #content .lost').finish().slideUp(1000, function(){
			$(this).remove();
		});

		socket.on('disconnect', function(reason){
			console.log("Disconnected beacuse: ", reason)
			$("#templates .block.error.lost").clone().appendTo('#content');
		});

		socket.on('dead', function(){
			$("#templates .block.error.dead").clone().appendTo('#content');
		});
		socket.on('update', function(data){
			//Pass this along.
			stats[data.id].update(data);
		});

		socket.on('defs', function(data){
			//Setup function.
			data.forEach(function(element){
				var stat = new StatsBlock(socket, element.name, element.description, element.id);
				$('#content #'+element.id).fadeOut(function(){
					$(this).remove();
				});
				stat.appendTo('#content');
				stats[element.id] = stat; //Hashmap
			});
		});

		socket.on('history', function(data){
			stats[data.id].onHistory(data);
		});

	});

});

