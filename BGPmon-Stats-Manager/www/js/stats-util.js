
function StatsBlock(socket, name, description, id){

	this.element = $("#templates .stat").clone();

	this.element.attr('id', id);

	this.id = id;

	this.element.find('.name').text(name);
	this.element.find('.desc').text(description);

	this.socket = socket;

	this.data = {
			'1': [],
			'12': [],
			'24': []
	};

	this.dirty = {
			12: false,
			24: false,
	};

	this.index = {
			time: 0,
			total: null,
			nlri: null,
			mpReach: null,
			mpUnreach: null,
			withdraw: null,
			max: 0
	};

	//Default labels.
	this.labels = ['Time'];

	this.niceLabels = {
			total: 'BGP',
			nlri: 'NLRI',
			mpReach: 'MP Reach',
			mpUnreach: 'MP Unreach',
			withdraw: 'Withdraw'
	};

	//Watch total
	this.socket.emit('watch', {
		id: this.id,
		key: 'total'
	});
	this.addIndex('total');

	this.charts = null;

	var scope = this;

	this.element.find('.views input[type=checkbox]').change(function(){
		var attr = $(this).attr('data-toggle');
        var checked = $(this).is(':checked');
        if (attr == 'log'){
            $.each(scope.charts, function(index, chart){
                chart.updateOptions({ logscale: checked });
            });
        } else {
		    if (checked){
			    scope.addIndex(attr);
    			scope.socket.emit('watch', {
	    			id: scope.id,
		    		key: attr
		    	});
    		} else {
	    		scope.removeIndex(attr);
		    	scope.socket.emit('unwatch', {
		    		id: scope.id,
		    		key: attr
		    	});
		    }
        }
	});

}

StatsBlock.prototype.constructor = StatsBlock;

StatsBlock.prototype.addIndex = function(key){
	var index = ++(this.index.max);
	this.labels.splice(index, 0, this.niceLabels[key]);
	this.index[key] = index;

	//Add a row for the new data.
    for (var i = 0; i < this.data[1].length; ++i){
        this.data[1][i][index] = null;
    }

    for (var i = 0; i < this.data[12].length; ++i){
        this.data[12][i][index] = [null, null, null];
    }

    for (var i = 0; i < this.data[24].length; ++i){
        this.data[24][i][index] = [null, null, null];
    }
};

StatsBlock.prototype.removeIndex = function(key){
	var index = this.index[key];
	this.labels.splice(index, 1);
	this.index[key] = null;
	--this.index.max;
	for (key in this.index){
		if (this.index[key] > this.index.max){
			this.index[key]--; //All labels that came after need to be shifted.
		}
	}

	//Remove that index of data.
	for (var i = 0; i < this.data[1].length; ++i){
		this.data[1][i].splice(index, 1);
	}

	for (var i = 0; i < this.data[12].length; ++i){
		this.data[12][i].splice(index, 1);
	}

	for (var i = 0; i < this.data[24].length; ++i){
		this.data[24][i].splice(index, 1);
	}

};

StatsBlock.prototype.appendTo = function(target){

	$(target).append(this.element);

	this.charts = {
			1: new Dygraph(
					this.element.find('.charts .1').get(0),
					this.data[1],
					{//Options
						labels: this.labels,
						xlabel: 'Time',
						ylabel: 'Messages per Second'
					}
			),
			12: new Dygraph(
					this.element.find('.charts .12').get(0),
					this.data[12],
					{
						labels: this.labels,
						xlabel: 'Time',
						ylabel: 'Message stats over time',
						customBars: true //Allows for error bars.
					}
			),
			24: new Dygraph(
					this.element.find('.charts .24').get(0),
					this.data[24],
					{
						labels: this.labels,
						xlabel: 'Time',
						ylabel: 'Message stats over time',
						customBars: true //Allows for error bars.
					}
			)
	};

};

StatsBlock.prototype.getElement = function(){
	return this.element;
};

StatsBlock.prototype.getIndex = function(graph, key){
	//Check if the last element in the array with the index of the key exists. If it does make a new one.
	for (var i = 1; i <= this.data[graph].length; ++i){ //This should only run a few iterations. (approx 3 or less)
		var element = this.data[graph][this.data[graph].length - i];
		if (element[this.index[key]] != undefined){ //Has data.
			if (i == 1){
				var end = [];
				this.data[graph].push(end);
			} else {
				//Return the element after this one.
				return this.data[graph][this.data[graph].length - i + 1];
			}
		}
	}

};

StatsBlock.prototype.update = function(update){

	var data = this.getIndex(1, update.key);
	data[this.index['time']] = new Date();
	data[this.index[update.key]] = update[1];

	if (update[12] != undefined){
		var data = this.getIndex(12, update.key);
		data[this.index['time']] = new Date();
		data[this.index[update.key]] = [update[12].min,
		                                update[12].avg,
		                                update[12].max];

		this.dirty[12] = true;

	}

	if (update[24] != undefined){
		var data = this.getIndex(24, update.key);
		data[this.index['time']] = new Date();
		data[this.index[update.key]] = [update[24].min,
		                                update[24].avg,
		                                update[24].max];

		this.dirty[24] = true;

	}

	while (this.data[1].length > 3600){this.data[1].shift();}
	while (this.data[12].length > 60 * 12){this.data[12].shift();}
	while (this.data[24].length > 24){this.data[24].shift();}

	this.render();

};

StatsBlock.prototype.onHistory = function(history){

	var time1 = StatsBlock.genTimeArray(history[1].length, 1000);

	for (var i = 0; i < history[1].length; ++i){
		if (this.data[1][i] == undefined){
			this.data[1][i] = [];
		}

		var element = this.data[1][i];
		element[this.index['time']] = time1[i];
		element[this.index[history.key]] = history[1][i];
	}

	var time12 = StatsBlock.genTimeArray(history[12].avg.length, 1000 * history[12].res);

	for (var i = 0; i < history[12].avg.length; ++i){
		var element = this.data[12][i] = (this.data[12][i] != undefined)?this.data[12][i]:[];
		element[this.index['time']] = time12[i];
		element[this.index[history.key]] = [history[12].min[i], history[12].avg[i], history[12].max[i]];
	}

	if (this.data[12].length > 0){
		this.dirty[12] = true;
	}

	var time24 = StatsBlock.genTimeArray(history[24].avg.length, 1000 * history[24].res);

	for (var i = 0; i < history[24].avg.length; ++i){
		var element = this.data[24][i] = (this.data[24][i] != undefined)?this.data[24][i]:[];
		element[this.index['index']] = time24[i];
		element[this.index[history.key]] = [history[24].min[i], history[24].avg[i], history[24].max[i]];
	}

	if (this.data[24].length > 0){
		this.dirty[24] = true;
	}

	this.render();

};

/**
 * Creates an artificial time array, the end is now.
 * 
 * @param length - How many elements are needed.
 * @param interval - How long between each time in ms.
 */
StatsBlock.genTimeArray = function(length, interval, offset){

	if (offset == undefined) offset = 0;

	var now = Date.now();
	var ret = new Array(length);

	for (var i = 0; i < length; ++i){
		ret[length-i-1] = new Date(now - interval * i - offset);
	}

	return ret;

};

/**
 * This allows for a massive performance increase using asynchronous rendering.
 */
StatsBlock.prototype.render = function(){

	//1 is always ready
	this.charts[1].updateOptions({file:this.data[1]});

	if (this.dirty[12]){
		this.charts[12].updateOptions({file:this.data[12]});
		this.dirty[12] = false;
	}

	if (this.dirty[24]){
		this.charts[24].updateOptions({file:this.data[24]});
		this.dirty[24] = false;
	}

}

StatsBlock.prototype.setLimit = function(limit){
	this.limit = limit;
};
