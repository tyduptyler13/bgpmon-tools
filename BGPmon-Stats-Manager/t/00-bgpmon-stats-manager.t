use strict;
use warnings;
use Test::More;

BEGIN { use_ok( 'BGPmon::Stats::Manager' ); }
require_ok( 'BGPmon::Stats::Manager' );

#Dependency for constructor.
use BGPmon::Fetch::Live;

my $fetch = new BGPmon::Fetch::Live({server=>'bgpdata3.netsec.colostate.edu', port=>50001});

my @args = (
        fetch => $fetch
    );

my $engine = new_ok('BGPmon::Stats::Manager', \@args);


done_testing();

