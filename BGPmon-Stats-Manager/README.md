BGPmon-Stats-Manager
====================

Version 1

This is a simple tool that allows you to collect data from a BGPmon::Fetch
and send it to a server.

Currently only appliest to Fetch::Live but will support all fetch modules
in the future.

Demo
----

We have constructed a web interface to view what this is capable of.

To start the server simply run `perl bin/test.pl | node etc/server.js`

This pipes the needed data to a websocket server for handling.


Manager
-------

This is a perl service that runs in a blocking way. It will collect data from
a fetch object and every pre defined interval, output information about that
data. The messages are printed to stdout and all logging information is printed
to stderr. This way you can redirect the output wherever you like.

