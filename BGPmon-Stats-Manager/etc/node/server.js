"strict"; //Uses strict javascript rules. Better for speed and size.

var config = require('./config.json');
var byline = require('byline');
var io = require('socket.io')(config.port);
var express = null;

if (config.webServer === true){ //Optional web server.

	try {

		express = require('express');

		var app = express();

		console.log("Starting server on: " + __dirname + "/" + config.webRoot);
		app.use(express.static(__dirname + "/" + config.webRoot));

		var server = app.listen(config.webPort, function() {
			console.log("Server listening on port", server.address().port);
		});

	} catch (e) {
		console.warn("Could not load webserver!", e);
	}

}

/*
 * Begin class section
 */

function Manager(){

	/**
	 * {
	 * 	1:{
	 * 		1:{
	 * 			total:[], withdraw:[], ... timestamp:[]
	 * 		},
	 * 		12:{...},
	 * 		24:{...},
	 * 	2:{...},
	 * 	...
	 * }
	 */
	this.stats = {};

	this.clients = [];

	this.definitions = [];

	this.dead = false;

	this.total = 0; //Total number of updates.

}

/**
 * Pushes data into the updates hash.
 * 
 * Needs to update 1, 12, and 24 hours with all relevant data.
 * 
 * @param id - Id of stat (it could be filtered at the server)
 * @param data - Data update
 */
Manager.prototype.update = function(id, data){

	if (this.stats[id] == undefined){
		//Setup data structure.
		this.stats[id] = {1:{}, 12:{}, 24:{}};
	}

	var update = this.stats[id];

	//Times are now assumed by client.
	//data.timestamp = Date.now();

	//Break hash into hash of arrays for optimal performance.
	for (var key in data){

		if (update[1][key] == undefined){
			update[1][key] = [];
		}

		update[1][key].push(data[key]);

		while (update[1][key].length > 3600) update[1][key].shift(); //Slide off old data.

	}

	if (this.total % config.resolution['12'] === 0 && this.total > config.resolution['12']){
		for (var key in update[1]){

			if (update[12][key] == undefined){
				update[12][key] = [];
			}

			update[12][key].push(createSpan(update[1][key].slice(-config.resolution['12'])));

			while (update[12][key].length > 43200 / config.resolution[12]) update[12][key].shift();

		}

		update[12].dirty = true;

	}

	if (this.total % config.resolution['24'] === 0 && this.total > config.resolution['24']){
		for (var key in update[1]){

			if (update[24][key] == undefined){
				update[24][key] = [];
			}

			update[24][key].push(createSpan(update[1][key].slice(-config.resolution['24'])));

			while (update[24][key].length > 86400 / config.resolution[24]) update[24][key].shift();

		}

		update[24].dirty = true;

	}

	this.total++;

	this.printStats();

	this.send(id);

};

Manager.prototype.printStats = function(){
	process.stdout.write("\rTotal connected clients: " + this.clients.length.toString() +
			" Total updates: " + this.total);
};

/**
 * This is the initial update for a client just connecting or switching
 * views.
 * 
 * Time is now assumed by client.
 */
Manager.prototype.getHistory = function(id, key){

	if (this.stats[id][1][key] == undefined){
		console.error("The data doesn't exist for " + key);
		return;
	}

	if (this.stats[id][12][key] == undefined) this.stats[id][12][key] = [];
	if (this.stats[id][24][key] == undefined) this.stats[id][24][key] = [];

	var len12 = this.stats[id][12][key].length;
	var len24 = this.stats[id][24][key].length;

	var data = {
			key: key,
			id: id,
			1: new Uint32Array(this.stats[id][1][key]),
			12: {
				res: config.resolution[12],
				min: new Uint32Array(len12),
				max: new Uint32Array(len12),
				avg: new Float32Array(len12)
			},
			24: {
				res: config.resolution[24],
				min: new Uint32Array(len24),
				max: new Uint32Array(len24),
				avg: new Float32Array(len24)
			}
	};

	//Transpose other matrices for minimal bandwidth.

	for (var i=0; i < len12; ++i){
		data['12'].min[i] = this.stats[id][12][key][i].min;
		data['12'].max[i] = this.stats[id][12][key][i].max;
		data['12'].avg[i] = this.stats[id][12][key][i].avg;
	}

	for (var i=0; i < len24; ++i){
		data['24'].min[i] = this.stats[id][24][key][i].min;
		data['24'].max[i] = this.stats[id][24][key][i].max;
		data['24'].avg[i] = this.stats[id][24][key][i].avg;
	}

	return data;

};

/**
 * This is the update over time function for clients.
 * 
 * It uses a subscriber model to make the bandwidth usage minimal.
 * 
 * Timestamp can be assumed to be whenever the data is received. (Saves space)
 */
Manager.prototype.send = function(id){

	var update = this.stats[id];

	for (var key in update[1]){

		var data = { //1 is always dirty. Add it anyways.
				id: id,
				key: key,
				1: update[1][key][update[1][key].length - 1]
		}

		if (update[12].dirty === true){
			data['12'] = update[12][key][update[12][key].length -1];
		}

		if (update[24].dirty === true){
			data['24'] = update[24][key][update[24][key].length -1];
		}

		io.to(id + key).emit('update', data);

	}

    if (update[12].dirty === true){
        update[12].dirty = false;
    }

    if (update[24].dirty === true){
        update[24].dirty = false;
    }

};

function createSpan(list){
	var span = {
			min: Infinity,
			max: -Infinity,
			avg: 0
	};
	var total = 0;

	for (var i=0; i < list.length; ++i){
		if (list[i] < span.min) span.min = list[i];
		if (list[i] > span.max) span.max = list[i];
		total += list[i];
	}

	span.avg = total / list.length;

	return span;
};

/*
 * Begin main code section.
 */

var manager = new Manager();

io.on('connection', function(socket){

	socket.emit("defs", manager.definitions);

	manager.clients.push(socket);

	if (manager.dead){
		socket.emit('dead'); //Notify the client that we are holding old data only.
	}

	socket.on('disconnect', function(){
		manager.clients.splice(manager.clients.indexOf(socket), 1); //Remove
		manager.printStats();
	});

	/**
	 * Updates the list of variables a client is observing.
	 */
	socket.on('watch', function(data){
		socket.join(data.id + data.key); //Join new room

		socket.emit('history', manager.getHistory(data.id, data.key));
	});

	/**
	 * Removes a client from a list.
	 */
	socket.on('unwatch', function(data){
		socket.leave(data.id + data.key); //Leave old room
	});

	manager.printStats();

});

var input = byline.createStream(process.stdin);

input.on('data', function(line){

	var data = JSON.parse(line.toString('ascii'));

	if (data.type == "definition"){
		manager.definitions.push(data);
	} else if (data.type == "update"){
		manager.update(data.id, data.data);
	}

});

input.on('end', function(){

	console.log("Detected that the pipe has closed. Something must be wrong with the feed.");

	manager.dead = true;

	manager.clients.forEach(function(client){
		client.emit("dead");
	});

});

process.on('exit', function(){

	io.emit("Server closed");

	console.log("Server closed. Goodbye.");

});
