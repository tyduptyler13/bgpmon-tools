Stats Socket Server
===================

Setup
-----
Copy example-config.json to config.json and set the values you would like to use, this
is done to keep changes to the config out of svn.

Config
------

* port: Where to host the server.
* webRoot: Where the web files are held (optional)
* webServer: Use the builtin webserver? (boolean)
* resolution: Size of a tick for each graph resolution. (max: 3600)!

Note: If the resolution exceeds the max of 3600 seconds, the data will never appear due
to data culling. We only keep 1 hour of max resolution data.
