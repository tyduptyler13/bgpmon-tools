use warnings;
use strict;
use Test::More;

use BGPmon::Filter::Prefix;

use constant TRUE => 1;
use constant FALSE => 0;

require_ok('Net::IP');

my $resp = `pwd`;
my $location = 't/';
if($resp =~ m/bgpmon-tools\/BGPmon-core\/t/){
	$location = '';
}




## Testing new
# Testing with no arguments
my $badItem = new BGPmon::Filter::Prefix();
is($badItem, undef, "Giving new no arguments");

# Testing no version given
my $badItem2 = new BGPmon::Filter::Prefix(prefix => '129.82.0.0/16', moreSpecific => 1);
is($badItem2, undef, "Giving no version number");

# Testing no prefix given
my $badItem3 = new BGPmon::Filter::Prefix(version => '4', moreSpecific => 1);
is($badItem3, undef, "Giving no prefix");

# Testing no more specific given
my $badItem4 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16');
is($badItem4, undef, "Giving no more specific to new");

# Testing bad prefix given
my $badItem5 = new BGPmon::Filter::Prefix(version=> '4', prefix => 'bad prefix oh no', moreSpecific => 1);
is($badItem5, undef, "Giving bad prefix to new");

# Making good item
my $goodItem = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16', moreSpecific => 1);
ok(defined($goodItem), "Giving good information to new");


## Testing equals
# Testing not equals with different prefix
my $v1 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16', moreSpecific => 1);
my $v2 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.83.0.0/16', moreSpecific => 1);
my $v3 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16', moreSpecific => 0);
is($v1->equals($v2), FALSE, "Testing equals with different prefixes");

# Testing not equals with different specificity
is($v1->equals($v3), FALSE, "Testing equals with different specificities");

# Testing correct equals
is($v1->equals($v1), TRUE, "Testing equals");

## Testing matchSpecific
# Testing with no partner called
is($v1->matchSpecific(), FALSE, "Testing match specific with no arguments");
# Testing with different specific
is($v1->matchSpecific($v3), FALSE, "Testing match specific diff specific");
# Testing with same specific
is($v1->matchSpecific($v2), TRUE, "Testing match specific with same specific");


## Testing more specific
my $v4 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16', moreSpecific => 1);

# Testing with no arguments
is($v4->moreSpecific(), FALSE, "Testing more speicifc with no arguments");
# Testing with bad arguments
is($v4->moreSpecific('bad prefix'), FALSE, "Testing more speicifc with bad prefix");
# Testing more specific
is($v4->moreSpecific('129.82.2.0/24'), TRUE, "Testing more specific with more specific prefix");
# Testing equals
is($v4->moreSpecific('129.82.0.0/16'), TRUE, "Testing more specific with equivillent prefixx");
# Testing overlapping prefix
is($v4->moreSpecific('129.0.0.0/8'), FALSE, "Testing more specific with less specific prefix");
# Testing non overlapping prerfix
is($v4->moreSpecific('109.82.2.0/24'), FALSE, "Testing more specific with non overlapping prefix");


## Testing less specific
is($v4->lessSpecific(), FALSE, "Testing less speicifc with no arguments");
# Testing with bad arguments
is($v4->lessSpecific('bad prefix'), FALSE, "Testing less speicifc with bad prefix");
# Testing more specific
is($v4->lessSpecific('129.82.2.0/24'), FALSE, "Testing less specific with more specific prefix");
# Testing equals
is($v4->lessSpecific('129.82.0.0/16'), TRUE, "Testing less specific with equivillent prefixx");
# Testing overlapping prefix
is($v4->lessSpecific('129.0.0.0/8'), TRUE, "Testing less specific with less specific prefix");
# Testing non overlapping prerfix
is($v4->lessSpecific('109.82.2.0/24'), FALSE, "Testing less specific with non overlapping prefix");

## Testing can aggregate with
my $v5 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16', moreSpecific => 1);
my $v6 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.82.0.0/16', moreSpecific => 1);
my $v7 = new BGPmon::Filter::Prefix(version=> '4', prefix => '9.82.0.0/16', moreSpecific => 1);
my $v8 = new BGPmon::Filter::Prefix(version=> '4', prefix => '129.83.0.0/16', moreSpecific => 1);
# Testing with no arguments
is($v5->canAggregateWith(), FALSE, "Tseting can aggregate with no arguments");
# Testing with equivilent prefix
is($v5->canAggregateWith($v6), FALSE, "Tseting can aggregate with equal prefix");
# Testing with non overlapping prefix
is($v5->canAggregateWith($v7), FALSE, "Tseting can aggregate with non overlapping prefix");
# Testing with sister prefix
is($v5->canAggregateWith($v8), TRUE, "Tseting can aggregate with sister prefix");

## testing getAggregate
# Testing with no arguments
is($v5->getAggregate(), undef, "Tseting get aggregate with no arguments");
# Testing with equivilent prefix
is($v5->getAggregate($v6), undef, "Tseting get aggregate with equal prefix");
# Testing with non overlapping prefix
is($v5->getAggregate($v7), undef, "Tseting get aggregate with non overlapping prefix");
# Testing with sister prefix
is($v5->getAggregate($v8), '129.82.0.0/15', "Tseting get aggregate with sister prefix");


## Testing toString
my $v9 = new BGPmon::Filter::Prefix(version => '4', prefix => '9.82.0.0/16', moreSpecific => 1);
my $v10 = new BGPmon::Filter::Prefix(version => '4', prefix => '129.83.0.0/16', moreSpecific => 0);
is($v9->toString(), "9.82.0.0/16 ms", "Testing to string");
is($v10->toString(), "129.83.0.0/16 ls", "Testing to string");

## Testing getPrefix
is($v9->getPrefix(), "9.82.0.0/16", "Testing getPrefix");

## Testing Version
is($v9->getVersion(), "4", "Testing getPrefix");

## testing is ipv6
is($v9->isV6(), FALSE, "Testing isV6");

## Testing is ipv4
is($v9->isV4(), TRUE, "Testing isv4");






done_testing();

