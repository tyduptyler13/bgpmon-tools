use warnings;
use strict;
use Test::More;

use BGPmon::Filter::Address;

use constant TRUE => 1;
use constant FALSE => 0;

require_ok('Net::IP');

my $resp = `pwd`;
my $location = 't/';
if($resp =~ m/bgpmon-tools\/BGPmon-core\/t/){
	$location = '';
}




## Testing making of new address

#Testing no arguments given to new
my $badAddr = new BGPmon::Filter::Address();
is($badAddr, undef, "Testing new with no arguments");

#Testing address not given to new
my $badAddr2 = new BGPmon::Filter::Address(version => 4);
is($badAddr2, undef, "Testing new without address argument");

#Testing version not given to new
my $badAddr3 = new BGPmon::Filter::Address(address => '129.82.2.2');
is($badAddr3, undef, "Testing new without version argument");

#Testing bad version given to new
my $badAddr4 = new BGPmon::Filter::Address(version => 5, address => '129.82.2.2');
is($badAddr4, undef, "Testing new with bad version argument");

#Testing bad address given to new
my $badAddr5 = new BGPmon::Filter::Address(version => 4, address => "fake address");
is($badAddr5, undef, "Testing new with bad address argument");

#Testing mismatched version and address given to new
my $badAddr6 = new BGPmon::Filter::Address(version=> 4, address=>'2001:468:d01:33::80df:330f');
is($badAddr6, undef, "Testing new with IPv6 address and version 4");

#Testing good ipv4 given to new
my $goodAddr = new BGPmon::Filter::Address(version=> 4, address=>'129.82.2.2');
ok(defined($goodAddr), "Testing new with IPv4 address");

#Testing good ipv6 given to new
my $goodAddr2 = new BGPmon::Filter::Address(version=> 6, address=>'2001:468:d01:33::80df:330f');
ok(defined($goodAddr2), "Testing new with IPv6 address");


## Testing matches
# Setting up the IPv4 and IPv6 data structures
my $v4addr = new BGPmon::Filter::Address(version=> 4, address=>'129.82.2.2');
my $v6addr = new BGPmon::Filter::Address(version=> 6, address=>'2001:468:d01:33::80df:330f');
ok(defined($v4addr), "Making IPv4 address");
ok(defined($v6addr), "Making IPv6 address");

# Testing no arguments given to matches
my $badMatch = $v4addr->matches();
is($badMatch, FALSE, "Testing no arguments given to matches");

# Testing bad prefix given to matches
my $badMatch2 = $v4addr->matches("abcabcabc");
is($badMatch2, FALSE, "Testing invalid IP address given to matches");

# Testing non overlapping IP prefix given to matches
my $badMatch3 = $v4addr->matches("10.0.0.0/24");
is($badMatch3, FALSE, "Testing non-overlapping IPv4 prefix given to matches");

# Testing IPv4 prefix overlapping ipv6 address
my $badMatch4 = $v6addr->matches("129.82.0.0/16");
is($badMatch4, FALSE, "Testing IPv4 prefix given to IPv4 address");

# Testing IPv4 overlapping prefix
my $goodMatch = $v4addr->matches("129.82.0.0/16");
is($goodMatch, TRUE, "Testing valid IPv4 address given to matches");

# Testing IPv6 overlapping prefix
my $goodMatch2 = $v6addr->matches("2001::/8");
ok(defined($goodMatch2), "Testing valid IPv6 prefix given to matches");


## Testing isv6
# Setting up the IPv4 and IPv6 data structures
my $v4addr2 = new BGPmon::Filter::Address(version=> 4, address=>'129.82.2.2');
my $v6addr2 = new BGPmon::Filter::Address(version=> 6, address=>'2001:468:d01:33::80df:330f');
ok(defined($v4addr2), "Making IPv4 address");
ok(defined($v6addr2), "Making IPv6 address");

# Testing that IPv4 is not IPv6
ok(!$v4addr2->isV6(), "Testing that IPv4 does not seem like IPv6");

# Testing that IPv6 is IPv6
ok($v6addr2->isV6(), "Testing that IPv6 seems like IPv6");

## Testing is v4
# Testing that IPv4 is not IPv6
ok(!$v6addr2->isV4(), "Testing that IPv4 seems like IPv4");

# Testing that IPv6 is IPv6
ok($v4addr2->isV4(), "Testing that IPv4 does not seem like IPv6");


## Testing getVresion
# Testing v4 version is 4
is($v4addr2->getVersion, '4', 'Testing IPv4 address is version 4');


# Testing v6 version is 6
is($v6addr2->getVersion, '6', 'Testing IPv6 address is version 6');


## Testing toString

# Testing string of IPv4
is($v4addr2->toString(), "129.82.2.2", "Testing to string of IPv4");

# Testing string of IPv6
is($v6addr2->toString(), '2001:468:d01:33::80df:330f', 'Testing to string of IPv6');

## Testing equals

# Testing equal
ok($v4addr2->equals($v4addr2), "Testing equals");
ok($v6addr2->equals($v6addr2), "Testing equals v6");
# Testing no equal
ok(!$v4addr2->equals($v6addr2), "Testing not equals");
ok(!$v6addr2->equals($v4addr2), "Testing not equals v6");




done_testing();

