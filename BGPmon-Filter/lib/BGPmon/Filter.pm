package BGPmon::Filter;
use strict;
use warnings;

use 5.14.0;

our $VERSION = '3.00';

use constant FALSE => 0;
use constant TRUE => 1;
use BGPmon::Message;
use BGPmon::Filter::Prefix;
use BGPmon::Filter::Address;
use Net::IP;
use Regexp::IPv6 qw($IPv6_re);
use List::MoreUtils qw(uniq);
use Data::Dumper;


=head1 NAME

BGPmon::Filter

This module provides information of if a BGP message matches a set of 
IPv4 or IPv6 prefixes, or if it matches a specific autonymous system number.
=cut

=head1 SYNOPSIS

use BGPmon::Filter;
my $filter = new BGPmon::Filter(filename => "config_file.txt");
unless(defined($filter->parse_config_file()){
  exit 1;
}
my $xml_msg = '...

if($filter->filter_xml_msg($xml_msg)){
  print "Matches!\n";
}

=head1 EXPORT

new 


=head1 SUBROUTINES/METHODS




=head2 new

Will initilialize the object an its state variables.  You may pass in
a configuration file if you would like.  It should be line deleniated. The 
following is an example of filtering all IPv4 messages pertaining to prefixes
that would cover 10/8, all IPv6 prefixes that would be covered by 2008::/64, 
and all messages originating from ASN 12145. 

IPv4 10.0.0.0/8 ls
IPv6 2008::\64 ms
asn 12145

E.x. my $filter = new BGPmon::Filter( filename => "configuration_file.txt');

INPUT: (optional) filename
OUTPUT: refrence

=cut


sub new{
  my $class = shift;
  my %args = @_;

  # Getting filename from arguments if it exists
  my $filename = undef;
  if(exists($args{filename})){
    $filename = $args{filename};
  }


  my $self = bless {
    prefixFilename => $filename,
    v4prefixes => {},
    v4count => 0,
    v6prefixes => [],
    asNumbers => {},
    asnCount => 0,
    v4addresses => [],
    v6addresses => [],
  }, $class;
  return $self;
}



#comment
#Will check to see if the string passed in is of proper IPv6 format.
#cut
sub _is_IPv6{
  my $self = shift;
  my $str = shift;

  return undef unless defined $str;

  my $ip_something = new Net::IP $str;
  return FALSE unless defined $ip_something;

  return TRUE if $ip_something->version eq '6';

  return FALSE;
}




sub _ipv4_chkip{
  my $self = shift;
  my $addrToCheck = shift;
  return undef if not defined $addrToCheck;

  # convering to Net::IP object
  my $ip_something = new Net::IP $addrToCheck;
  return undef unless defined $ip_something;


  # Making sure that it's v4
  return undef if ($ip_something->version() ne '4');

  return $ip_something;
}





=head2 parse_config_file

Will parse the wanted IPv4 and IPv6 prefixes from a configuration file as well
as any autonymous system numbers.  This will also aggregate addresses
where possible unless the mask lengths become less than a /8, and this will
setup a mult-layer hash lookup system for faster retrieval

Input: None

Output: 0 if there is no error
        -1 if couldn't open file
        1+ for line number where error occured
        -2 for internal error

=cut
sub parse_config_file{
  my $self = shift;

  
  return -1 if not defined $self->{prefixFilename};
  my $prefixFilename = $self->{prefixFilename};

  my $file;
  my $lineNum = 0;
  if(!open($file, $prefixFilename)){
    return -1;
  }

  # Holding all items we find in file to add to hashes later
  my @v4prefixes = ();
  my @v6prefixes = ();
  my @v4addresses = ();
  my @v6addresses = ();
  my @asns = ();

  while(my $line = <$file>){
    $lineNum ++;
    chomp $line;

    # Remove any trailing white space.
    $line =~ s/^s+//g;

    # If the line starts with a #, skip it.
    next if ($line =~ /^s*#/);

    # Skipping the line if it's blank
    next if ($line eq "");


    # Splitting the line up
    my @lineArray = split ' ', $line;
    my $lineLength = scalar(@lineArray);
    return $lineNum if $lineLength < 1;
    
    # if this line is an AS number
    if($lineArray[0] =~ /[aA][sS]/){
      return $lineNum if $lineLength != 2;
      my $asn = $lineArray[1] + 0;
      return $lineNum if ($asn <= 0 or $asn > 65536);
      push(@asns, $asn);
    }

    # if this line is an IPv4 number
    elsif($lineArray[0] =~ /[iI][pP][vV][4]/){
      
      # Checking on the specifics at the end of the line
      my $haveSpecific = exists($lineArray[2]);
      my $moreSpecific = undef;
      if($haveSpecific){
        return $lineNum unless $lineArray[2] =~ m/[mMlL][sS]/;
        $moreSpecific = $lineArray[2] =~ m/[mM][sS]/;
      }

      #Making sure whatever is next is either a valid prefix or IPv4 address
      my $addr = $lineArray[1];
      my $ipcheck = $self->_ipv4_chkip($addr);
      return $lineNum unless defined($ipcheck);
      
      #Decerning if we have a prefix or IPv4 address
      if($addr =~ /\//){ #prefix
        return $lineNum unless $haveSpecific;
        
        # Adding prefix to the list since it's okay
        my $version = 4;
        my $prefOb = new BGPmon::Filter::Prefix(version => 4, prefix => $addr, moreSpecific =>  $moreSpecific); 
        return $lineNum unless defined $prefOb;
        push(@v4prefixes, $prefOb);
      }
      else{#IPv4 address
        return $lineNum if $haveSpecific;
        my $addrOb = new BGPmon::Filter::Address(version => 4, address =>$addr);
        return $lineNum unless defined $addrOb;
        push(@v4addresses,$addrOb);
      }
    }

    # if this line is an IPv6 number
    elsif($lineArray[0] =~ /[iI][pP][vV][6]/){
      
      # Checking on the specifics at the end of the line
      my $haveSpecific = exists($lineArray[2]);
      my $moreSpecific = undef;
      if($haveSpecific){
        return $lineNum unless $lineArray[2] =~ m/[mMlL][sS]/;
        $moreSpecific = $lineArray[2] =~ m/[mM][sS]/;
      }
      
      # Making sure whatever is next is either a valid prefix or IPv6 address
      my $addr = $lineArray[1];
      my $ipcheck = $self->_is_IPv6($addr);
      return $lineNum unless defined($ipcheck);
      
      #Decerning if we have a prefix or IPv6 address
      if($addr =~ /\//){ #prefix
        return $lineNum unless $haveSpecific;
        
        # Adding prefix to the list since it's okay
        my $prefOb = new BGPmon::Filter::Prefix(version => 6, prefix => $addr, moreSpecific => $moreSpecific); 
        return $lineNum unless defined $prefOb;
        push(@v6prefixes, $prefOb);
      }
      else{#IPv4 address
        return $lineNum if $haveSpecific;
        my $addrOb = new BGPmon::Filter::Address(version => 6, address => $addr);
        return $lineNum unless defined $addrOb;
        push(@v6addresses,$addrOb);
      }
    }

    # if we don't know what this line is
    else{
      return $lineNum;
    }
  }

  #closing the file
  close($file);
  
  # Uniquing all items found
  @asns = uniq @asns;
  
  # Aggregating prefixes where possible
  @v4prefixes = $self->_condense_prefs(\@v4prefixes);
  @v6prefixes = $self->_condense_prefs(\@v6prefixes);

  # Adding everything to the appropriate hashes
  foreach(@v4prefixes){
    return -2 if($self->addV4Prefix($_) == 1);
  }
  foreach(@v6prefixes){
    return -2 if($self->addV6Prefix($_) == 1);
  }
  foreach(@v4addresses){
    return -2 if($self->addAddress($_, 4) == 1);
  }
  foreach(@v6addresses){
    return -2 if($self->addAddress($_, 6) == 1);
  }
  foreach(@asns){
    return -2 if($self->addASN($_) == 1);

  }


  return 0;
}

=head2 addV4Prefix

Adds a prefix to the filter.  This should be a BGPmon::Filter::Prefix object.

INPUT: BGPmon::Filter::Prefix object
OUTPUT: 0 if added
        1 on error
        2 if prefix is already in filter

=cut
sub addV4Prefix{
  my $self = shift;
  my $prefix = shift;

  # Getting first octet
  my @allOctets = split(/\./, $prefix->getPrefix());
  my $firstOctet = $allOctets[0];

  # Making sure the first octet exists in the hash
  unless(exists($self->{'v4prefixes'}->{$firstOctet})){
    $self->{'v4prefixes'}->{$firstOctet} = [];
  }

  # Making sure the prefix isn't seen in the hash already
  foreach(@{$self->{'v4prefixes'}->{$firstOctet}}){
    return 2 if $_->equals($prefix);
  }

  # Adding to the hash
  push(@{$self->{'v4prefixes'}->{$firstOctet}}, $prefix);

  # Incrementing count
  $self->{v4count} += 1;
  
  return 0;
}

=head2 addV6Prefix

Adds a prefix to the filter.  This should be a BGPmon::Filter::Prefix object.

INPUT: BGPmon::Filter::Prefix object
OUTPUT: 0 if added
        1 on error
        2 if prefix is already in filter

=cut
sub addV6Prefix{
  my $self = shift;
  my $prefix = shift;

  # Making sure the prefix isn't seen in the hash already
  foreach(@{$self->{'v6prefixes'}}){
    return 2 if $_->equals($prefix);
  }

  # Adding to the hash
  push(@{$self->{'v6prefixes'}}, $_);

  return 0;
}

=head2 addAddress

Adds an address to the filter.  This should be a BGPmon::Filter::Address object.

INPUT: BGPmon::Filter::Address object
OUTPUT: 0 if added
        1 on error
        2 if address is already in filter

=cut
sub addAddress{
  my $self = shift;
  my $address = shift;
  my $version = shift;

  if($version == 4){
    foreach(@{$self->{v4addresses}}){
      if($_->equals($address)){
        return 2;
      }
    }

    push(@{$self->{v4addresses}}, $address);
  }
  elsif($version == 6){
    foreach(@{$self->{v6addresses}}){
      if($_->equals($address)){
        return 2;
      }
    }

    push(@{$self->{v6addresses}}, $address);

  }
  else{
    return 1;
  }
  return 0;
}
=head2 addASN

Adds an ASN to the filter.  This should be a string.

INPUT: ASN represented as a string
OUTPUT: 0 if added

=cut
sub addASN{
  my $self = shift;
  my $asn = shift;

  # Adding to the hash
  $self->{asNumbers}->{$asn} = 1;

  # Incrementing count
  $self->{asnCount} += 1;

  return 0;
}

#
#TODO MOVE THIS TO bgpmon_filter
#sub parse_database_config{
#  my $listName = shift;
#  my $fname = 'parse_database_config';
#  lock($lock);


#getting a list of prefixes
#  my @prefs = BGPmon::CPM::PList::Manager::export2CSV('',$listName);
#  my $size = scalar(@prefs);
#  if($size == 0){
#TODO create an error message that has the list size at zero
#  }

#resetting the module
#  filterReset();

#creating new prefixes
#  foreach(@prefs){
#    my $newPref = $_->prefix();
#    my $moreSpec = $_->watch_more_specifics();
#    if(is_IPv6($newPref)){
#      my $temp = new BGPmon::Filter::Prefix(6, $newPref, $moreSpec);
#      push(@v6prefixes, $temp);
#    }
#    else{
#      my $temp = new BGPmon::Filter::Prefix(4, $newPref, $moreSpec);
#      push(@v4prefixes, $temp);
#    }
#  }
#  return 0;
#}


=head2 get_num_IPv4_prefs

Will count the number of IPv4 prefixes it has parsed from the configuration
file and return the integer

Input: None
Output : Integer
=cut
sub get_num_IPv4_prefs{
  my $self = shift;
  return $self->{v4count};
}

=head2 get_num_IPv6_prefs

Will count the number of IPv6 prefixes it has parsed from the configuration
file and return the integer

Input: None
Output : Integer
=cut
sub get_num_IPv6_prefs{
  my $self = shift;
  return scalar(@{$self->{v6prefixes}});
}


=head2 get_num_ASes

Will count the number of ASes it has parsed from the configuration
file and return the integer

Input: None
Output : Integer
=cut
sub get_num_ASes{
  my $self = shift;
  return scalar(keys $self->{asNumbers});
}



=head2 get_num_ip_addrs

Will count the number of IPv4 and IPv6 addresses it has parsed from the 
configuration file and return the integer

Input: None
Output : Integer
=cut
sub get_num_ip_addrs{
  my $self = shift;
  return scalar(@{$self->{v4addresses}}) + scalar(@{$self->{v6addresses}});
}

=head2 get_total_num_filters

Will tally all filters the module will look for per message and return
the interger

Input: None
Output : Integer

=cut
sub get_total_num_filters{
  my $self = shift;
  my $toReturn = 0;
  $toReturn += $self->get_num_IPv4_prefs();
  $toReturn += $self->get_num_IPv6_prefs();
  $toReturn += $self->get_num_ASes();
  $toReturn += $self->get_num_ip_addrs();
  return $toReturn;
}


#condense__prefs
#
#Will try to aggregate prefixes where possible.  This is used to reduce
#overhead that the filter script may experience later.  Please note that the
#parse_config_file must be ran beforehand.
#
#Input: array of prefixes, version of IP
#
#Output: array of prefixes after condensing
#
#
sub _condense_prefs{
  my $self = shift;
  my $prefRef = shift;
  my @prefixes = @{$prefRef};

  #Will continuously agg. addresses where it can until none are left
  my $found = TRUE;
  while($found){
    $found = FALSE;

    for(my $i = 0; $i < scalar(@prefixes); $i++){
      my $currPref = $prefixes[$i];

      next if $currPref->getMaskLength eq '8';

      for(my $k = $i+1; $k < scalar(@prefixes); $k++){
        my $thisPref = $prefixes[$k];

        # Maknig sure they're focused on the same specificity
        next unless $currPref->matchSpecific($thisPref);

        # Making sure we should even check them based on mask length
        next unless $currPref->getMaskLength() eq $thisPref->getMaskLength();


        # Seeing if we can aggregate them
        if($currPref->canAggregateWith($thisPref)){
          my $newPref = $currPref->getAggregate($thisPref);
          splice @prefixes, $k, 1;
          splice @prefixes, $i, 1;
          my $np = new BGPmon::Filter::Prefix(version => $currPref->getVersion(), 
                   prefix => $newPref, moreSpecific => $currPref->{moreSpecific}); 
          push(@prefixes, $np);
          $found = TRUE;
          last;
        }

        # Seeing if they are both the same
        if($currPref->getPrefix() eq $thisPref->getPrefix()){
          splice @prefixes, $k, 1;
          $found = TRUE;
          last;
        }
      }

      if($found){
        last;
      }
    }
  }
  return @prefixes;
}



=head2 filter_xml_msg

Will grab all prefixes from the WITHDRAW, NLRI, MP_REACH_NLRI, and 
MP_UNREACH_NLRI sections as well as the Origin ASN from the AS_PATH (if one 
exists) and will test these against prefixes, addresses, and ASNs in the filter.

If any one of the addresses, prefixes, or ASNs is found to match with a
filter, the message will be marked as positive.

INPUT: xml message as a string
OUTPUT: 1 if message should be kept
        0 if message did not match any filters


=cut
sub filter_xml_msg{
  my $self = shift;
  my $xmlMsg = shift;

  unless(defined($xmlMsg)){
    return undef;
  }

  # A list of all the prefixes and AS's found during searching.
  my @v4s = ();
  my @v6s = ();
  my @ases = ();

  # The translation of the message
  my $message = new BGPmon::Message(string=>$xmlMsg);
  if(!defined($message)){
    #TODO return new value saying we couldn't parse the message.
    return 1;
  }
  
  #Checking the withdrawn part of the message
  my @withs = $message->get_withdraws();
  foreach(@withs){
    my @parts = split(/\//, $_);
    if($self->_is_IPv6($parts[0])){
      push(@v6s, $_);
    }
    else{
      push(@v4s, $_);
    }
  }
  
  #Checking the address part of MP_UNREACH_NLRI
  my @unreach = $message->get_mp_unreach_nlris();
  foreach(@unreach){
    my @parts = split(/\//, $_);
    if($self->_is_IPv6($parts[0])){
      push(@v6s, $_);
    }
    else{
      push(@v4s, $_);
    }
  }


  #Checking the address parts in NLRI place
  my @nlris = $message->get_nlris();
  foreach(@nlris){
    my @parts = split(/\//, $_);
    if($self->_is_IPv6($parts[0])){
      push(@v6s, $_);
    }
    else{
      push(@v4s, $_);
    }
  }


  #Checking the MP_NLRI part of MP_REACH_NLRI; skipping the Next Hop addresses
  my @reach = $message->get_mp_nlris();
  foreach(@reach){
    my @parts = split(/\//, $_);
    if($self->_is_IPv6($parts[0])){
      push(@v6s, $_);
    }
    else{
      push(@v4s, $_);
    }
  }


  #Checking for the origin in the AS_Path attribute
  my @aspath = $message->get_aspath();
  if(scalar(@aspath) > 0){
    my $originAS = $aspath[-1];
    push(@ases, $originAS) if defined($originAS);
  }

  # Uniquing
  my @v4 = uniq(@v4s);
  my @v6 = uniq(@v6s);
  my @as = uniq(@ases);


  ## Matching against data

  # Checking to see if any of these AS numbers are ones we're looking for.
  foreach(@as){
    return TRUE if exists $self->{asNumbers}->{$_};
  }

  # Checking to see if any of the v4 prefixes are matches.
  foreach(@v4){
    my $ipPrefAddr = $_;
    
    # Getting first octet
    my @allOctets = split(/\./, $_);
    my $firstOctet = "$allOctets[0]";
    next unless exists $self->{v4prefixes}->{$firstOctet};

    my @toCheck = @{$self->{v4prefixes}->{$firstOctet}};
    foreach(@toCheck){
      my $v4Prefix = $_;
      my $temp = $v4Prefix->getPrefix();
      return TRUE if $v4Prefix->matches($ipPrefAddr);
    }
  }

  #Checking to see if any of the v4 addresses are a match
  foreach(@v4){
    my $tempPrefix = $_;
    foreach(@{$self->{v4addresses}}){
      my $addr = $_;
      return TRUE if($addr->matches($tempPrefix));
    }
  }
  

  # Checking to see if any of the v6 addresses are matches.
  foreach (@v6){
    my $ipPrefAddr = $_;
    # Seeing if we need to keep on to the message
    foreach(@{$self->{v6prefixes}}){
      my $v6Prefix = $_;
      return TRUE if($v6Prefix->matches($ipPrefAddr));
    }
  }
  
  #Checking to see if any of the v6 addresses are a match
  foreach(@v6){
    my $tempPrefix = $_;
    foreach(@{$self->{v6addresses}}){
      my $addr = $_;
      return TRUE if($addr->matches($tempPrefix));
    }
  }

  return FALSE;

}


#
#
#Will check to see if the BGPmon message passed to it has maching prefix or AS 
#fields that were given earlier to the module.  
#
#Input:  A BGPmon message in XML format
#
#Output: 1 if there was at least one matching filed.
#0 if no matches were found.
#
sub _matches{
  my $self = shift;

  my $xmlMsg = shift;

  return undef unless defined $xmlMsg;

  parse_xml_msg($xmlMsg);

}





1;
__END__


=head1 AUTHOR

M. Lawrence Weikum C<< <mweikum at rams.colostate.edu> >>

=cut

=head1 BUGS

Please report any bugs or feature requeues to 
C<bgpmon at netsec.colostate.edu> or through the web interface
at L<http://bgpmon.netsec.colostate.edu>.

=cut

=head1 SUPPORT

You can find documentation on this module with the perldoc command.

perldoc BGPmon::Filter

=cut


=head1 LICENSE AND COPYRIGHT

Copyright (c) 2012 Colorado State University

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.\

File: Filter.pm

Authors: M. Lawrence Weikum

Date: 20 May 2014
=cut

