#!/usr/bin/perl
use v5.14.0;
use warnings;
use strict;

use BGPmon::Fetch qw/connect_bgpdata read_xml_message close_connection is_connected/;
#use BGPmon::Translator::XFB2PerlHash::Simple qw/init get_status/;

$| = 1; #turn on print flushing
my $server = '129.82.138.67';
my $port = 50001;
my $outputFile = 'parseErrorOutput.txt';

#my $FILEH;

#open $FILEH, "+>", $outputFile or die "Could not open %outputFile\n";
print "Connectiong to $server:$port\n";
if(connect_bgpdata($server, $port)){
  print  "Failed to connect to $server:$port\n";
  exit 1;
}
print  "Connected to $server:$port\n";





while(1){


  if(!is_connected()){
    print  "Lost connection to BGPmon. Reconnecting...\n";

    while(!is_connected()){
      if(connect_bgpdata($server, $port)){
    	print  "Reconnect failed. Waiting 1 minute.\n";
	sleep(60); #sleeping for a minute until we try to reconnect again.
      }
    }

    print  "Reconnected to BGPmon.\n";


  }

  my $xmlMsg = read_xml_message();
 if(!defined($xmlMsg) or !length($xmlMsg)){
   next;
 }

# print "$xmlMsg\n";

=comment
  if($xmlMsg =~ m/PARSE/){
	my $time = localtime;
	print  "$time\n$xmlMsg\n\n";
  }
=cut
  if((index($xmlMsg, 'ERR') != -1) || (index($xmlMsg, 'UNKN') != -1)){
	my $time = localtime;
	print "$time\n$xmlMsg\n\n";
  }

}





