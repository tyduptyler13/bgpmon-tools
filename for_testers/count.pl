#!/usr/bin/perl

use strict;
use warnings;

use BGPmon::Configure;
use BGPmon::Fetch qw/connect_bgpdata read_xml_message close_connection is_connected/;
use threads;
use threads::shared;




#main part




my @params = (
    {
    Name  => BGPmon::Configure::CONFIG_FILE_PARAMETER_NAME,
    Type  => BGPmon::Configure::FILE,
    Default => undef,
    Description => "This is the configuration file name.",
    },
    {
    Name => "server",
    Type => BGPmon::Configure::STRING,
    Default => 'bgpdata3.netsec.colostate.edu',
    Description => "This is the BGPmon server address",
    },
    {
    Name => "port",
    Type => BGPmon::Configure::PORT,
    Default => 50001,
    Description => "This is the BGPmon server port",
    });

#Checking that everything parsed correctly
if(BGPmon::Configure::configure(@params) ) {
  my $code = BGPmon::Configure::get_error_code("configure");
  my $msg = BGPmon::Configure::get_error_message("configure");
  print "$code: $msg\n";
  exit 1;
}



my $server : shared = BGPmon::Configure::parameter_value("server");
my $port : shared = BGPmon::Configure::parameter_value("port");


print "Server: $server, Port: $port\n";


my $numMsgs : shared;
$numMsgs = 0;




my $readThread = threads->create('read');
my $countThread = threads->create('count');




$readThread->join();
$countThread->join();






#counting thread
sub count{

  while(1){
    sleep 1;

    my $msgCount = 0;
    {
      lock($numMsgs);
      $msgCount = $numMsgs;
      $numMsgs = 0;
    }

    if($msgCount == -1){
      last;
    }
    else{

      my @list = (my $sec,my $min,my $hour,my $day, my $mon, my $year_1900, my $wday, my $yday, my $isdst)=localtime;


      print "$hour:$min:$sec saw $msgCount messages\n";
    }
  }
}





#reading thread
sub read{
#log_info("Connecting to source: $server:$port");
  print "Connecting to BGPmon $server:$port\n";# if $debug;
  my $retVal = connect_bgpdata($server, $port);
  if($retVal != 0){
    print "Couldn't connect to the BGPmon server.  Aborting.\n";
#log_err("Coudln't connect to BGPmon server.");
    exit 1;
  }
  print "Connected to BGPmon server!\n";# if $debug;
#log_info("Connected to BGPmon server.");



  while(1){
    my $xmlMsg = undef;

    if(!is_connected()){
      print "Lost connection to BGPmon. Stopping.\n";# if $debug;
#log_err("Lost connection to BGPmon.  Stopping.");
    { #flagging other thread to sop
      lock($numMsgs);
      $numMsgs = -1;
#      print "Read: numMsgs: $numMsgs\n";
    }
      last;
    }

    $xmlMsg = read_xml_message();


# Check if we received an XML message
    if(!defined $xmlMsg or !length($xmlMsg)){
#log_err("Error reading XML messgae from BGPmon");
      print "Error reading XML messgae from BGPmon\n";
      next;
    }


# Adding message to message queue

  }





}








