#!/usr/bin/perl

use warnings;
use strict;

use BGPmon::Fetch qw/connect_bgpdata read_xml_message close_connection is_connected/;
use BGPmon::Translator::XFB2PerlHash::Simple qw/init get_status/;

$| = 1; #turn on print flushing
my $server = '129.82.138.67';
my $port = 50002;
my $outputFile = 'statusOutput2.txt';


print "Connectiong to $server:$port\n";
if(connect_bgpdata($server, $port)){
  print "Failed to connect to $server:$port\n";
  exit 1;
}
print "Connected to $server:$port\n";



open FILEH, '>', $outputFile or die "Could not open %outputFile\n";



while(is_connected()){

  my $xmlMsg = read_xml_message();

  if(!defined($xmlMsg) or !length($xmlMsg)){
    next;
  }


  my $retval = init($xmlMsg);

  my $status = get_status();

  if(!defined($status)){
    next;
  }

  my $time = localtime;

  print FILEH "Status Message: $status at $time\n$xmlMsg\n\n";
}



