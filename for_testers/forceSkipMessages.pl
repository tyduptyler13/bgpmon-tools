#!/usr/bin/perl

use 5.14.0;
use warnings;
use strict;

use BGPmon::Fetch qw/connect_bgpdata read_xml_message close_connection is_connected/;
use BGPmon::Translator::XFB2PerlHash::Simple qw/init get_status/;

$| = 1; #turn on print flushing
my $server = '129.82.138.27'; #robin
my $port = 50001;
my $sleeptime = 300; #5 minutes

say "Connectiong to $server:$port";
if(connect_bgpdata($server, $port)){
  say "Failed to connect to $server:$port";
  exit 1;
}
say "Connected to $server:$port";


while(is_connected()){

  say "Sleeping for $sleeptime seconds";
  sleep($sleeptime);
  say "Awake";

  #Reading 10 messages
  #for(my $i = 0; $i < 100; $i += 1){
  while(1){
    my $xmlMsg = read_xml_message();

    if(!defined($xmlMsg) or !length($xmlMsg)){
      say "finished";
      exit(0);
    }



#    my $time = localtime;
#    say "Status Message: $status at $time\n$xmlMsg";
    say $xmlMsg if $xmlMsg =~ /STATUS/;
  }
}



