#!/usr/bin/perl

use warnings;
use strict;

use BGPmon::Fetch qw/connect_bgpdata read_xml_message close_connection is_connected/;
use BGPmon::Validate qw/init validate get_error_msg get_error_code/;

$| = 1; #turns on print flushing

my $server = '129.82.138.67';
my $port = 50001;
my $outputFile = 'validationErrorOutput.txt';


print "Connectiong to $server:$port\n";
if(connect_bgpdata($server, $port)){
  print "Failed to connect to $server:$port\n";
  exit 1;
}
print "Connected to $server:$port\n";



open FILEH, '>', $outputFile or die "Could not open %outputFile\n";
print "Opened $outputFile\n";

if(init('xsd/bgp_monitor_2_00.xsd')){
  my $errorMsg = get_error_msg('init');
  print "Error initializing validation msg: $errorMsg\n";
  exit 1;
}

while(is_connected()){

  my $xmlMsg = read_xml_message();

  if(!defined($xmlMsg) or !length($xmlMsg)){
    next;
  }

  if(validate($xmlMsg)){
    my $time = localtime;
    my $error = get_error_msg('validate');
    print FILEH "Validation Error : $error\n$xmlMsg\n\n";
  }

}



