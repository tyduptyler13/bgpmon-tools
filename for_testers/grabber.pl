#!/usr/bin/perl

use v5.16.0;
use BGPmon::Fetch qw(connect_bgpdata read_xml_message close_connection is_connected);
use Data::Dumper;
use BGPmon::Translator::XFB2PerlHash;



my $server = "129.82.138.16"; #bgpdata3
my $port = 50001;

my $item = $ARGV[-1];
say "Searching for $item";



# Connecting to BGPmon
print "Connecting to BGPmon\n";
my $retVal = connect_bgpdata($server, $port);
if($retVal != 0){
  print "Couldn't connect to the BGPmon server.  Aborting.\n";
  exit 1;
}

print "connected to bgpmon\n";
print "searching \n";
while(is_connected()){
  my $xmlMsg = read_xml_message();
  if(!defined($xmlMsg)){
    sleep 1;
    next;
  }

  #put the part of the xml you want in here and you shall receive.
  #if($xmlMsg =~ /TABLE_START/){
  if($xmlMsg =~ /$item/){
    print "$xmlMsg\n";
    close_connection();
    break;
  }
}

