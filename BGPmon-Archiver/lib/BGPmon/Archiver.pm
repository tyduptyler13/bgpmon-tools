package BGPmon::Archiver;

use strict;
use warnings;
use BGPmon::Log;
use BGPmon::Fetch;
use BGPmon::Message;

#---- Default settings ----
use constant DEFAULT_UPDATE_PORT => 50001;
use constant DEFAULT_RIB_PORT => 50002;
use constant DEFAULT_SERVER => 'livebgp.netsec.colostate.edu';
use constant DEFAULT_LOG_LEVEL => BGPmon::Log::LOG_WARNING;
use constant DEFAULT_ROLL_INTERVAL => 900;


our $VERSION = '3.10';

=head1 NAME

BGPmon::Archiver

This module contains the main archiver functionality.

=head2 SYNOPSIS

use BGPmon::Archiver;
my $archiver = BGPmon::Archiver->new();
$archiver->activate();

=head2 new
Create a new BGPmon::Archiver instance.
Input: Configuration parameters
Output: BGPmon::Archiver object

=cut
sub new {
    my $class = shift;
    my %args = @_;
    my ($server, $port);
    my $roll_int;
    my $log_file;
    my $use_stdout_for_status;
    my $log_level;
    my $archive_bgpdump;
    my $use_syslog;

    return undef unless(defined(%args));

    # --- Set config. parameters. ---
    # Server configuration parameters.
    if (defined($args{server})) {
        $server = $args{server};
    }
    if (defined($args{port})) {
        $port = $args{port};
    }

    # Logging configuration.
    if (defined($args{use_syslog})) {
        $use_syslog = $args{use_syslog};
    } else {
        $use_syslog = 1;
    }
    if (defined($args{log_level})) {
        $log_level = $args{log_level};
    } else {
        $log_level = BGPmon::Log::WARN;
    }
    if (defined($args{log_file})) {
        $log_file = $args{log_file};
    } else {
        $log_file = *STDOUT;
    }
    if (defined($args{status_file})) {
        $status_file = $args{status_file};
    } else {
        $status_file = *STDOUT;
    }

    # Archiver parameters.
    if (defined($args{roll_interval})) {
        $roll_int = $args{roll_interval};
    } else {
        $roll_int = 900;
    }
    if (defined($args{archive_bgpdump})) {
        $archive_bgpdump = $args{archive_bgpdump};
    } else {
        $archive_bgpdump = 0;
    }

    # Create class and return the hash.
    my $self = bless {
        server => $server,
        port => $port,
        roll_interval => $roll_int,
        log_level => $log_level,
        log_file => $log_file,
        archive_bgpdump => $archive_bgpdump,
        status_file => $status_file,
        _retry_interval => 1,
        _prog_name => $0,
        use_syslog => $use_syslog,
    }, $class;

    return $self;
}

=head2 activate
Activate the archiver. Sets up the log file, status file and connects
to the BGPmon server instance.

Inputs: None
Outputs: None on success, error code on failure.
=cut

sub activate {
    my $self = shift;

    # Initialize the log
    my $ret = BGPmon::Log::log_init(use_syslog => $self->{use_syslog},
        log_level => $self->{log_level},
        log_file => $self->{log_file},
        prog_name => $self->{_prog_name});
    if ($ret != 0) { # Log initialization failed.
        return BGPmon::Log::get_error_code('log_init');
    }

    # Initialize the status file
    unless (open($self->{_status_fh}, ">>$self->{status_file}")) {
        $self->{_status_fh} = *STDOUT;
        BGPmon::Log::log_warn
            ("Could not open status file for writing. Using STDOUT.");
    }

    # Set time to an early date so first message will
    # trigger file creation.
    my $epoch = time();
    $self->{_start_time} = $epoch - 2 * $self->{roll_interval};
    $self->{_start_time} -= ($self->{_start_time} % $self->{roll_interval});
    $self->{_end_time} = $self->{_start_time} + 2 * $self->{roll_interval};

    # Connect to bgpmon instance.
    BGPmon::Log::log_info
        ("Connecting to $self->{server} at port $self->{port}");
    $self->{_fetch_obj} = BGPmon::Fetch->new(server => $self->{server},
        port => $self->{port});
    # If unsuccessful, return undef.
    unless(defined($self->{_fetch_obj})) {
        BGPmon::Log::log_error("Could not connect to
            $self->{server} at port $self->{port}");
        return undef;
    }
}
1;
