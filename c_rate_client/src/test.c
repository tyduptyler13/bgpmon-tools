/*
** client.c -- a stream socket client demo
*/
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "expandable_buffer.h"

#define PORT "50001"
#define XML_BUFFER_LENGTH      1024000ULL

void *read_buffer(void *arg);
int establish_connection(const char* hostname,const char* port,int *sockfd);
void *get_in_addr(struct sockaddr *sa);
int prime_buffer(buffer *bf, int sockfd);

// global variables
buffer bf;

/*****************************************************************************
read_buffer
return values: 0, success
*****************************************************************************/
void* 
read_buffer(void *arg){

  char xml_message[XML_BUFFER_LENGTH];
  fprintf(stderr,"Starting Reading Thread\n");

  while(1){
    if(!read_xml_message(&bf,xml_message,XML_BUFFER_LENGTH)){
      //__sync_fetch_and_add( &message_amount, 1 );
    }
  }
  return NULL;
}

int main(int argc, char *argv[])
{
  pthread_t threadID;
  int  numbytes,sockfd;
  char *hostname;

  //process command line arguments
  if (argc != 2) {
      fprintf(stderr,"usage: client hostname\n");
      return 1;
  }
  hostname = argv[1];

  // connect to the host and port
  if(establish_connection(hostname,PORT,&sockfd)){
    fprintf(stderr,"Unable to establish connection\n");
    return 1;
  }

  // prime the data structures
  //data_amount = 0;
  if(prime_buffer(&bf,sockfd)){
    fprintf(stderr,"Unable to prime the buffer\n");
    exit(1);
  }


  if(pthread_create(&threadID, NULL, read_buffer, NULL)){
    fprintf(stderr,"Failed to create Mrt Control thread \n");
    return 1;
  }

  sleep(2);
  pthread_join(threadID, NULL);
}

/*****************************************************************************
prime_buffer
return values: 0, success
*****************************************************************************/
int
prime_buffer(buffer *bf, int sockfd){

  char tiny[5];
  // initialize the buffer
  init_buffer(bf);

  // read <xml> from the socket
  int num = recv(sockfd, tiny, 5, 0);
  if(num != 5){
    fprintf(stderr,"Failure trying to read <xml>\n");
    return 1;
  }

  char *space;
  uint32_t size;
  if(get_buffer_write_pos(bf,&space,&size)){
    fprintf(stderr,"Failure to get space for writing\n");
    return 1;
  }
  num = recv(sockfd, space, size, 0);
  return record_buffer_write(bf,num);
}

/*****************************************************************************
establish connection
return values: 0, success
*****************************************************************************/
int
establish_connection(const char* hostname,const char * port,int *sockfd){

  struct addrinfo hints, *servinfo, *p;
  int rv;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(hostname, port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return -1;
  }

  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((*sockfd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }

    if (connect(*sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(*sockfd);
      perror("client: connect");
      continue;
    }
    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to connect\n");
    return -1;
  }

  inet_ntop(p->ai_family,get_in_addr((struct sockaddr *)p->ai_addr),s,sizeof s);
  printf("client: connecting to %s\n", s);

  freeaddrinfo(servinfo); // all done with this structure
  return 0;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
