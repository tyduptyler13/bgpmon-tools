/*
** client.c -- a stream socket client demo
*/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <pthread.h>
#include <arpa/inet.h>

#include "expandable_buffer.h"

#define BILLION 1E9
#define PORT "50001" // the port client will be connecting to 
#define INTERVAL 5
#define XML_BUFFER_LENGTH  1024000ULL

void *read_buffer(void *arg);
int establish_connection(const char* hostname,const char* port,int *sockfd);
int prime_buffer(buffer *bf,int sockfd);
void catch_alarm (int sig);
void *get_in_addr(struct sockaddr *sa);

// global variable for accumulated time and data
volatile sig_atomic_t data_amount;
volatile sig_atomic_t message_amount;
volatile sig_atomic_t seconds=0;
struct timespec intervalStart, intervalEnd;
buffer bf;

/*****************************************************************************
read_buffer
return values: 0, success
*****************************************************************************/
void* 
read_buffer(void *arg){

  int error;
  char xml_message[XML_BUFFER_LENGTH];
  long slowdown = *(long*)arg;
  fprintf(stderr,"Starting Reading Thread with slowdown of %ld\n",slowdown);

  while(1){
    if(!(error = read_xml_message(&bf,xml_message,XML_BUFFER_LENGTH))){
      __sync_fetch_and_add( &message_amount, 1 );
      /*fprintf(stderr, "GOT string %s\n\n",xml_message);
      fprintf(stderr, "buffer start position is %lu\n",bf.start_pos);
      fprintf(stderr, "buffer end position is %lu\n",bf.end_pos);
      fprintf(stderr, "buffer size is %lu\n",bf.size);
      fprintf(stderr, "strlen is %lu\n",strlen(xml_message));*/
      if(slowdown > 0){
        struct timespec timeout;
        timeout.tv_sec = 0;
        timeout.tv_nsec = slowdown;
        while (nanosleep(&timeout, &timeout) && errno == EINTR);
        if(errno == EINVAL){
          fprintf(stderr,"The value of the timeout was invalid\n");
        }
      }
    }else{
      if(error == 3){
        fprintf(stderr,"XML buffer is too small\n");
      }
      if(error == 2){
        fprintf(stderr,"unknown error\n");
      }
    }
  }
  return NULL;
}

int main(int argc, char *argv[])
{
  int  numbytes,sockfd;  
  char *hostname;
  long slowdown;

  //process command line arguments
  if (argc < 2) {
      fprintf(stderr,"usage: client hostname [rate (msg/sec)]\n");
      return 1;
  }
  hostname = argv[1];

  if(argc == 3){
    slowdown = atol(argv[2]);
    if(slowdown < 1){
      fprintf(stderr,"usage: client hostname [rate]\n");
      fprintf(stderr,"slowdown must be an integer greater than 0\n");
      return 1;
    }  
    if(slowdown == 1){
      slowdown = 999999999;
    }else{
      slowdown =  1000000000/slowdown;
    }
  }else{
    slowdown = 0;
  }

  // connect to the host and port
  if(establish_connection(hostname,PORT,&sockfd)){
    fprintf(stderr,"Unable to establish connection\n");
    return 1;
  }
 
  // prime the data structures
  data_amount = 0;
  if(prime_buffer(&bf,sockfd)){
    fprintf(stderr,"Unable to prime the buffer\n");
    return 1;
  }

  // spawn the thread that will read the buffer
  pthread_t threadID;
  if(pthread_create(&threadID, NULL, read_buffer, &slowdown)){
    fprintf(stderr,"Failed to create Mrt Control thread \n");
    close(sockfd);
    return 1;
  }

  // read the buffer
  // set up the timer
  signal (SIGALRM, catch_alarm);
  alarm(INTERVAL);
  clock_gettime(CLOCK_REALTIME, &intervalStart);


  char *position;
  uint32_t space; 
  if(get_buffer_write_pos(&bf,&position,&space)){
    fprintf(stderr,"Unable to read from the buffer\n");
  }

  // wrapping is only possible after this point
  while((numbytes = recv(sockfd, position, space, 0)) > -1) {
     __sync_fetch_and_add( &data_amount, numbytes );
    if(record_buffer_write(&bf,numbytes)){
      fprintf(stderr,"buffer write failed\n");
    }
    if(get_buffer_write_pos(&bf,&position,&space)){
      //fprintf(stderr,"Unable to write to the buffer\n");
    }
  }
  perror("recv");
  close(sockfd);
  return 0;
}



/*****************************************************************************
prime_buffer
return values: 0, success
*****************************************************************************/
int
prime_buffer(buffer *bf, int sockfd){

  char tiny[5];
  // initialize the buffer
  init_buffer(bf);

  // read <xml> from the socket
  int num = recv(sockfd, tiny, 5, 0);
  if(num != 5){
    fprintf(stderr,"Failure trying to read <xml>\n");
    return 1;
  }

  char *space;
  uint32_t size;
  if(get_buffer_write_pos(bf,&space,&size)){
    fprintf(stderr,"Failure to get space for writing\n");
    return 1;
  }
  num = recv(sockfd, space, size, 0);
  return record_buffer_write(bf,num);
}

/*****************************************************************************
establish connection
return values: 0, success
*****************************************************************************/
int
establish_connection(const char* hostname,const char * port,int *sockfd){

  struct addrinfo hints, *servinfo, *p;
  int rv;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(hostname, port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return -1;
  }

  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((*sockfd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }

    if (connect(*sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(*sockfd);
      perror("client: connect");
      continue;
    }
    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to connect\n");
    return -1;
  }

  inet_ntop(p->ai_family,get_in_addr((struct sockaddr *)p->ai_addr),s,sizeof s);
  printf("client: connecting to %s\n", s);

  freeaddrinfo(servinfo); // all done with this structure
  return 0;
}

/* The signal handler just clears the flag and re-enables itself. */
void
catch_alarm (int sig) {
  seconds += INTERVAL;
  clock_gettime(CLOCK_REALTIME, &intervalEnd);
  fprintf(stdout,"%u %d %d\n",seconds,data_amount,message_amount);
  fflush(stdout);
  //double accum = (intervalEnd.tv_sec - intervalStart.tv_sec )
               //+ (intervalEnd.tv_nsec - intervalStart.tv_nsec )
               /// BILLION;
  //printf( "%lf\n", accum );
  data_amount = 0;
  message_amount = 0;
  signal (SIGALRM, catch_alarm);
  alarm(INTERVAL);
  clock_gettime(CLOCK_REALTIME, &intervalStart);
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
