/*
** test the buffer
*/
#include <stdio.h>
#include <string.h>
#include "expandable_buffer.h"

#define XML_BUFFER_LENGTH 1024

int 
main(int argc, char *argv[]){

  buffer bf;
  char *space;
  uint32_t size;
  static char *xml_message;

  xml_message = malloc(XML_BUFFER_LENGTH);
  if(xml_message == NULL){
    fprintf(stderr,"FAILED: malloc failed\n");
  }

  init_buffer(&bf);
  bf.size = 35;
  bf.buffer[bf.size] = '\0';
  int err = read_xml_message(&bf,xml_message,XML_BUFFER_LENGTH);
  if(err != 1){
    fprintf(stderr,"FAILED: buffer should be empty\n");
    return 1;
  }
  // try to write to the buffer (a complete xml message)
  if(get_buffer_write_pos(&bf,&space,&size)){
    fprintf(stderr,"Failure to get space for writing\n");
    return 1;
  }
  if(size < 16){
    fprintf(stderr,"FAILED: there isn't room for the test string\n");
    return 1;
  }
  memcpy(space,"<test>xml</test>",16);
  if(record_buffer_write(&bf,16)){
    fprintf(stderr,"FAILED: overwrote buffer\n");
    return 1;
  }
  // try to write to the buffer (a complete xml message)
  if(get_buffer_write_pos(&bf,&space,&size)){
    fprintf(stderr,"Failure to get space for writing\n");
    return 1;
  }
  if(size < 17){
    fprintf(stderr,"FAILED: there isn't room for the test string\n");
    return 1;
  }
  memcpy(space,"<test>xml2</test>",17);
  if(record_buffer_write(&bf,17)){
    fprintf(stderr,"FAILED: overwrote buffer\n");
    return 1;
  }
  err = read_xml_message(&bf,xml_message,XML_BUFFER_LENGTH);
  if(err != 0){
    fprintf(stderr,"FAILED: buffer should have one valid message\n");
    return 1;
  }
  fprintf(stderr,"message is %s\n",xml_message);
  err = read_xml_message(&bf,xml_message,XML_BUFFER_LENGTH);
  if(err != 0){
    fprintf(stderr,"FAILED: buffer should have one valid message\n");
    return 1;
  }
  fprintf(stderr,"message is %s\n",xml_message);

  // try to write to the buffer (a complete xml message)
  if(get_buffer_write_pos(&bf,&space,&size)){
    fprintf(stderr,"Failure to get space for writing\n");
    return 1;
  }
  if(size != 2){
    fprintf(stderr,"FAILED: size is wrong expected 2 got %lu\n",size);
    return 1;
  }
  memcpy(space,"<t",2);
  if(record_buffer_write(&bf,2)){
    fprintf(stderr,"FAILED: overwrote buffer\n");
    return 1;
  }
  if(get_buffer_write_pos(&bf,&space,&size)){
    fprintf(stderr,"Failure to get space for writing\n");
    return 1;
  }
  if(size <15){
    fprintf(stderr,"FAILED: size is wrong expected 31 got %lu\n",size);
    fprintf(stderr,"FAILED: buffer %s\n",bf.buffer);
    fprintf(stderr,"FAILED: buffer %llu %llu\n",bf.start_pos,bf.end_pos);
    return 1;
  }
  memcpy(space,"est>xml3</test>",15);
  if(record_buffer_write(&bf,15)){
    fprintf(stderr,"FAILED: overwrote buffer\n");
    return 1;
  }
  err = read_xml_message(&bf,xml_message,XML_BUFFER_LENGTH);
  if(err != 0){
    fprintf(stderr,"FAILED: buffer should have one valid message\n");
    return 1;
  }
  fprintf(stderr,"message is %s\n",xml_message);

  fprintf(stderr,"SUCCESS: testing complete.\n");
  return 0;
  

}

