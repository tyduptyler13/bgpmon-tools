#ifndef EXPANDABLEBACKLOG_H
#define EXPANDABLEBACKLOG_H

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <pthread.h>

#include <arpa/inet.h>

#define BUFFER_SIZE 1048576
//#define XML_BF_LEN  1024000ULL


struct backlog_struct{
  char        buffer[BUFFER_SIZE];
  uint32_t    size;           
  uint32_t    start_pos;
  uint32_t    end_pos;
  uint32_t    scan_pos;
  uint32_t    start_wrap;
  uint32_t    end_wrap;
  pthread_mutex_t wrap_lock;   
};
typedef struct backlog_struct buffer;


int init_buffer(buffer* b);
int get_buffer_write_pos(buffer* b,char** pos, uint32_t* space);
int record_buffer_write(buffer* b,uint32_t length);
int read_xml_message(buffer* b,char* dest, uint32_t max_length);

#endif
