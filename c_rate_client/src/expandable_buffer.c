#include "expandable_buffer.h"

/*****************************************************************************
init_buffer
initialize all of the struct variables
note: space is assumed to be allocated by caller
input: buffer pointer
return values: 0, success
               1, failure
*****************************************************************************/
int 
init_buffer(buffer* b){
  b->size = BUFFER_SIZE;
  b->start_pos = 0;
  b->scan_pos = 0;
  b->end_pos = 0;
  b->start_wrap = 0;
  b->end_wrap = 0;
  pthread_mutex_init(&b->wrap_lock, NULL);
  return 0;
}

/*****************************************************************************
get_buffer_write_pos
get a pointer to the space where writing can take place, along with the 
amount of data that can be written into it
return values: 0, success
               1, buffer is full
*****************************************************************************/
int
get_buffer_write_pos(buffer* b,char** pos, uint32_t* space){
  pthread_mutex_lock(&b->wrap_lock);
  *pos = &b->buffer[b->end_pos];
  // calculate the space
  if(b->start_wrap ^ b->end_wrap){
    // we are in a wrapped state
    *space = (b->start_pos-1) - b->end_pos;
  }else{
    // not in a wrapped state
    *space = b->size - b->end_pos;
  }
  if(*space == 0){
    pthread_mutex_unlock(&b->wrap_lock);
    return 1;
  }
  pthread_mutex_unlock(&b->wrap_lock);
  return 0;
}

/*****************************************************************************
record_buffer_write
get a pointer to the space where writing can take place, along with the 
amount of data that can be written into it
return values: 0, success
               1, buffer is full
*****************************************************************************/
int
record_buffer_write(buffer* b,uint32_t length){
  pthread_mutex_lock(&b->wrap_lock);
  b->end_pos += length;
  if(b->end_pos == b->size){
    b->end_pos = 0;
    __sync_fetch_and_xor( &b->end_wrap, 1 );
  }else if(b->end_pos > b->size){
    pthread_mutex_unlock(&b->wrap_lock);
    return 1;
  }
  pthread_mutex_unlock(&b->wrap_lock);
  return 0;
}

/*****************************************************************************
read_xml_message
read a complete xml message from the buffer
input: buffer pointer, pointer to space for message, max length of data
return values: 0, success
               1, buffer is empty 
               2, unknown
               3, destination buffer is too small
*****************************************************************************/
int 
read_xml_message(buffer* b,char* dest, uint32_t max_length){

  // our positions in the buffer
  uint32_t scan_pos = b->start_pos;
  int scan_wrap = b->start_wrap;

  // have we emptied the buffer?
  int empty = 0;
  if(b->start_pos == b->end_pos){
    return 1;
  }
  // position in the destination buffer
  uint32_t dest_idx = 0;

  // keep track of tag depth and message depth
  int tag_depth = 0;
  int msg_depth = 0;

  // as it scan through the data it needs to maintain 3 variables
  char* previous = NULL;
  char* current = &b->buffer[scan_pos];
  char* next = NULL;
  pthread_mutex_lock(&b->wrap_lock);
  if(b->end_wrap ^ scan_wrap){
    // we are in a wrapped state
    if((scan_pos+1)<b->size){
      next = &b->buffer[scan_pos+1];
    }else{
      if(b->end_pos != 0){
        next = &b->buffer[0];
      }else{
        // the buffer is going to be empty on the next step
        // and in this case that means that we should stop
        pthread_mutex_unlock(&b->wrap_lock);
        return 1;
      }
    }
  }else{
    if((scan_pos+1)<b->end_pos){
      next = &b->buffer[scan_pos+1];
    }else{
      // the buffer is going to be empty on the next step
      // and in this case that means that we should stop
      pthread_mutex_unlock(&b->wrap_lock);
      return 1;
    }
  }
  pthread_mutex_unlock(&b->wrap_lock);

  // if we aren't starting at a < character the XML is corrupt
  if(*current != '<'){
    return 4;
  }
  if(dest_idx < max_length){
    dest[dest_idx] = *current;
    dest_idx++;
  }else{
    return 3;
  }
  

  // increment our state
  tag_depth++;
  msg_depth++;  

  // the basic idea here is to take one step forward in the buffer
  // this step may actually be back to the beginning of the buffer
  // or it may not be possible
  pthread_mutex_lock(&b->wrap_lock);
  while((tag_depth != 0 || msg_depth != 0 ) && !empty){
    // take one step further in the buffer
    scan_pos++;
    previous = current;
    current = next;

    if(current == NULL){
      pthread_mutex_unlock(&b->wrap_lock);
      return 1;
    }
 
    if(b->end_wrap ^ scan_wrap){
      // we are in a wrapped state
      if(scan_pos == b->size){
        scan_pos = 0;
        scan_wrap = scan_wrap ^ 1;
        if(b->end_pos > 1){
          next = &b->buffer[1];
        }else{
          next = NULL;
        }
      }else if((scan_pos+1)<b->size){
        next = &b->buffer[scan_pos+1];
      }else{
        if(b->end_pos != 0){
          next = &b->buffer[0];
        }else{
          next = NULL;
        }
      }
    }else{
      // not in a wrapped position
      if((scan_pos+1)<b->end_pos){
        next = &b->buffer[scan_pos+1];
      }else{
        next = NULL;
      }
    }

    // check the value of current
    if(*current == '<'){
      tag_depth++;
      msg_depth++;
    }else if(*current == '>'){
      tag_depth--;
    }else if(*current == '/'){
      if(*previous == '<'){
        msg_depth -=2;
      }else if(next != NULL &&*next == '>'){
        msg_depth -=1;
      }
    }

    if(dest_idx < max_length){
      dest[dest_idx] = *current;
      dest_idx++;
    }else{
      pthread_mutex_unlock(&b->wrap_lock);
      return 3;
    }

    // check for an emtpy buffer
    if(scan_pos == b->end_pos){
      empty = 1;
    }
  }
  pthread_mutex_unlock(&b->wrap_lock);

  // if a complete message was found
  // update the state of the buffer
  if(tag_depth == 0 && msg_depth == 0){
    pthread_mutex_lock(&b->wrap_lock);
    if(scan_pos > b->start_pos){
      if( (scan_pos+1) < b->size ){
        b->start_pos = scan_pos+1;
      }else{
        b->start_pos = scan_pos+1;
        __sync_fetch_and_xor( &b->start_wrap, 1);
      }
    }else{
      b->start_pos = scan_pos+1;
      __sync_fetch_and_xor( &b->start_wrap, 1);
    }
    pthread_mutex_unlock(&b->wrap_lock);
    dest[dest_idx] = '\0';
    return 0;
  }else if(empty){
    return 1;
  }
  return 2;
}

